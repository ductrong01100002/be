package com.example.capstone_g25_be.controller;

import com.example.capstone_g25_be.entities.people;
import com.example.capstone_g25_be.repository.PeopleRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/people")
public class PeopleController {
    private final PeopleRepository peopleRepository;

    public PeopleController(PeopleRepository peopleRepository) {
        this.peopleRepository = peopleRepository;
    }

    @GetMapping("/get-people")
    public ResponseEntity<List<people>> getAll(){
        List<people> peopleList = peopleRepository.findAll();
        System.out.println(peopleList.size());
        return ResponseEntity.ok().body(peopleList);

    }
}
