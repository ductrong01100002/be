package com.example.capstone_g25_be.repository;

import com.example.capstone_g25_be.entities.people;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PeopleRepository extends JpaRepository<people,Long> {


}
