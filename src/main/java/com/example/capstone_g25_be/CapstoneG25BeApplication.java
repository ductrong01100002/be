package com.example.capstone_g25_be;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class CapstoneG25BeApplication {

    public static void main(String[] args) {
        SpringApplication.run(CapstoneG25BeApplication.class, args);
    }

}
