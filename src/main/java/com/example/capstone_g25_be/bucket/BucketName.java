package com.example.capstone_g25_be.bucket;

public enum BucketName {

	PROFILE_IMAGE("amigoscode-image-upload-222");

	private final String bucketName;

	BucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	public String getBucketName() {
		return bucketName;
	}

}
