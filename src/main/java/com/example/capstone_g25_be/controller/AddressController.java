package com.example.capstone_g25_be.controller;

import com.example.capstone_g25_be.model.response.MessageResponse;
import com.example.capstone_g25_be.utils.Constant;
import com.example.capstone_g25_be.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.capstone_g25_be.entities.Address;
import com.example.capstone_g25_be.entities.District;
import com.example.capstone_g25_be.entities.Province;
import com.example.capstone_g25_be.entities.Ward;
import com.example.capstone_g25_be.model.request.AddressRequest;
import com.example.capstone_g25_be.model.request.ManufacturerRequest;
import com.example.capstone_g25_be.repository.AddressRepository;
import com.example.capstone_g25_be.repository.DistrictRepository;
import com.example.capstone_g25_be.repository.ProvinceRepository;
import com.example.capstone_g25_be.repository.WardRepository;
import com.example.capstone_g25_be.services.AddressService;
import com.example.capstone_g25_be.utils.AddressConvertUtil;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/address")
public class AddressController {
	
	@Autowired
	AddressService addressService;
	
	@PostMapping("/add")
	public ResponseEntity<?> addAddress(@RequestBody AddressRequest addressRequest) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return addressService.addAddress(addressRequest);
	}

}
