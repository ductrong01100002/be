package com.example.capstone_g25_be.controller;

import com.example.capstone_g25_be.entities.*;
import com.example.capstone_g25_be.model.request.LoginRequest;
import com.example.capstone_g25_be.model.request.SignupRequest;
import com.example.capstone_g25_be.model.request.UpdateUserRequest;
import com.example.capstone_g25_be.model.response.JwtResponse;
import com.example.capstone_g25_be.model.response.MessageResponse;
import com.example.capstone_g25_be.repository.*;
import com.example.capstone_g25_be.security.jwt.JwtUtils;
import com.example.capstone_g25_be.security.services.UserDetailsImpl;
import com.example.capstone_g25_be.services.ProductService;
import com.example.capstone_g25_be.services.UserService;
import com.example.capstone_g25_be.utils.Constant;
import com.example.capstone_g25_be.utils.EmailSender;
import com.example.capstone_g25_be.utils.Generate;
import com.example.capstone_g25_be.utils.GeneratePass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    private  AuthenticationManager authenticationManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EmailSender emailSender;

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private ProvinceRepository provinceRepository;

    @Autowired
    private DistrictRepository districtRepository;

    @Autowired
    private WardRepository wardRepository;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        Optional<User> user = userRepository.findByUserName(loginRequest.getUsername());
        if (!user.isPresent()) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Tên đăng nhập hoặc mật khẩu không chính xác !",500));
        }
        Boolean isActive = user.get().getActive();
        if(!isActive){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng !",500));
        }
        String password = user.get().getPassWord();
        boolean checkOldPassword = encoder.matches(loginRequest.getPassword(),password);
        if(!checkOldPassword){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Tên đăng nhập hoặc mật khẩu không chính xác !",500));
        }
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getFullName(),
                userDetails.getImageUrl(),
                roles));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        if (userRepository.existsByUserName(signUpRequest.getUsername().trim())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Mã nhân viên đã tồn tại!"));
        }
        if (userRepository.existsByEmail(signUpRequest.getEmail().trim())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Email đã tồn tại !"));
        }
        if (userRepository.existsByIdentityCard(signUpRequest.getIdentityCard().trim())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Số CCCD/CMND đã tồn tại !"));
        }
        if (userRepository.existsByPhone(signUpRequest.getPhone().trim())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Sô điện thoại đã tồn tại !"));
        }
        String password = String.valueOf(GeneratePass.randomPass(8));
        emailSender.sendMailSingUp(signUpRequest.getEmail().trim(), password,signUpRequest.getFullName(),signUpRequest.getUsername());
        System.out.println(password);
        // Create new user's account
        User user = new User(signUpRequest.getUsername().trim(),
                encoder.encode(password),signUpRequest.getGender(),
                signUpRequest.getDateOfBirth(),signUpRequest.getEmail().trim(),signUpRequest.getIdentityCard().trim(),
                signUpRequest.getPhone().trim(),signUpRequest.getFullName().trim());

        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        Province province = provinceRepository.getById(signUpRequest.getProvinceId());
        District district = districtRepository.getById(signUpRequest.getDistrictId());
        Ward ward = wardRepository.getById(signUpRequest.getWardId());
        Address address = new Address();
        address.setProvince(province);
        address.setDistrict(district);
        address.setWard(ward);
        address.setDetail(signUpRequest.getAddressDetail().trim());
        addressRepository.save(address);
        if (strRoles == null) {
            Role userRole = roleRepository.findByRoleName(ERole.ROLE_SELLER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "ROLE_OWNER":
                        Role adminRole = roleRepository.findByRoleName(ERole.ROLE_OWNER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);
                        break;

                    case "ROLE_STOREKEEPER":
                        Role userROLE = roleRepository.findByRoleName(ERole.ROLE_STOREKEEPER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userROLE);
                        break;
                    default:
                        Role userRole = roleRepository.findByRoleName(ERole.ROLE_SELLER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                }
            });
        }

        user.setRoles(roles);
        user.setAddress(address);
        user.setActive(Constant.ACTIVE);
        user.setDeleteAt(false);
        userRepository.save(user);

        return ResponseEntity.ok(new MessageResponse("success"));
    }



}
