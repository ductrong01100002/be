package com.example.capstone_g25_be.controller;

import com.example.capstone_g25_be.model.response.MessageResponse;
import com.example.capstone_g25_be.utils.Constant;
import com.example.capstone_g25_be.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.capstone_g25_be.model.request.CategoryRequest;
import com.example.capstone_g25_be.services.CategoryService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/category")
public class CategoryController {
	
	private final int defaultPage = 1;
	private final int defaultSize = 10;
	
	@Autowired
	CategoryService categoryService;

	@GetMapping
    public ResponseEntity<?> listCategory(
    		@RequestParam(required = false) Integer pageIndex,
			@RequestParam(required = false) Integer pageSize, 
			@RequestParam(required = false) String categoryName,
			@RequestParam(required = false) String categoryDescription) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		pageIndex = pageIndex == null ? defaultPage : pageIndex;
		pageSize = pageSize == null ? defaultSize : pageSize;

		return categoryService.findAll(pageIndex, pageSize, categoryName, categoryDescription);
    }
	
	@PostMapping("/add")
	public ResponseEntity<?> addCategory(@RequestBody CategoryRequest categoryRequest){
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return categoryService.addCategory(categoryRequest);
	}
	
	@PutMapping("/update")
	public ResponseEntity<?> updateCategory(@RequestBody CategoryRequest categoryRequest){
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return categoryService.updateCategory(categoryRequest);
	}
	
	@GetMapping("/{categoryId}")
    public ResponseEntity<?> categoryDetail(
    		@PathVariable Long categoryId,
    		@RequestParam(required = false) Integer pageIndex,
			@RequestParam(required = false) Integer pageSize) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		pageIndex = pageIndex == null ? defaultPage : pageIndex;
		pageSize = pageSize == null ? defaultSize : pageSize;
		return categoryService.findById(categoryId, pageIndex, pageSize);
    }
	
	@DeleteMapping("/delete/{categoryId}")
	public ResponseEntity<?> deleteCategory(@PathVariable Long categoryId){
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return categoryService.deleteCategory(categoryId);
	}
	
	@GetMapping("/notPaging")
	public ResponseEntity<?> listCategoryNotPaging() {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return categoryService.findAllNotPaging();
	}

}
