package com.example.capstone_g25_be.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.capstone_g25_be.model.response.MessageResponse;
import com.example.capstone_g25_be.services.DashBoardService;
import com.example.capstone_g25_be.utils.Constant;
import com.example.capstone_g25_be.utils.Utils;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/dashBoard")
public class DashBoardController {

	@Autowired
	DashBoardService dashBoardService;

	@GetMapping()
	@Scheduled(cron = "0 59 23 ? * *")
//	@Scheduled(cron = "0/15 * * * * ?")
	public ResponseEntity<?> autoSaveDashBoardData() {
		dashBoardService.dashBoardOwner();
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping("/list")
	public ResponseEntity<?> listDashBoardData() {
		boolean isActive = Utils.checkIsActive();
		if (!isActive) {
			return ResponseEntity.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return dashBoardService.dashBoardOwnerlist();
	}

}
