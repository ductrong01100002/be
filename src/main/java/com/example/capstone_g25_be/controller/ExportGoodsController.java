package com.example.capstone_g25_be.controller;

import com.example.capstone_g25_be.entities.ServiceResult;
import com.example.capstone_g25_be.model.request.ExportGoodsRequest;
import com.example.capstone_g25_be.model.request.UpdateExportOrder;
import com.example.capstone_g25_be.model.request.UpdateImportGoodsRequest;
import com.example.capstone_g25_be.model.response.MessageResponse;
import com.example.capstone_g25_be.services.ExportGoodsService;
import com.example.capstone_g25_be.utils.Constant;
import com.example.capstone_g25_be.utils.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.text.ParseException;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/export-order")
@Slf4j
public class ExportGoodsController {
    private final static int DEFAULT_PAGE = 0;
    private final static int DEFAULT_SIZE = 10;
    private final ExportGoodsService exportGoodsService;

    public ExportGoodsController(ExportGoodsService exportGoodsService) {
        this.exportGoodsService = exportGoodsService;
    }

    /**
     * Get list export order
     */
    @GetMapping("/list")
    public ResponseEntity<ServiceResult<Map<String,Object>>> getListExportOrder(@RequestParam(required = false) Long userId,
                                                                       @RequestParam(required = false) String billReferenceNumber,
                                                                       String startDate,
                                                                       String endDate,
                                                                       @RequestParam(required = false) Integer pageIndex,
                                                                       @RequestParam(required = false) Integer pageSize,
                                                                       @RequestParam(required = false) Integer status) throws ParseException {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            ServiceResult<Map<String, Object>> mapServiceResult = new ServiceResult<>();
            mapServiceResult.setMessage("Tài khoản của bạn đã bị tạm dừng!");
            mapServiceResult.setStatus1(Constant.NOT_Allow);
            return ResponseEntity.ok(mapServiceResult);
        }
        pageIndex = pageIndex == null ? DEFAULT_PAGE : pageIndex;
        pageSize = pageSize == null ? DEFAULT_SIZE : pageSize;
        Date date1 = null;
        Date date2 = null;
        String s1 = "";
        String s2 = "";

        String billReferenceNumber1;
        if(billReferenceNumber==null || billReferenceNumber.equals("")){
            billReferenceNumber1 = "";
        } else{
            billReferenceNumber1 = billReferenceNumber.trim();
        }

        if(s1.equals(startDate) & s2.equals(endDate) ){
            date1 = null;
            date2 =null ;
        } else if(null==startDate & null ==endDate ){
            date1 = null;
            date2 =null ;
        }
        else if(s1.equals(startDate)){
            date2 = new SimpleDateFormat("dd-MM-yyyy").parse(endDate);
        }
        else if(s2.equals(endDate)){
            date1 = new SimpleDateFormat("dd-MM-yyyy").parse(startDate);
        }else{
            date1 = new SimpleDateFormat("dd-MM-yyyy").parse(startDate);
            date2 = new SimpleDateFormat("dd-MM-yyyy").parse(endDate);
        }

        ServiceResult<Map<String,Object>> mapServiceResult =  exportGoodsService.getListExportOrder(userId,billReferenceNumber1,date1,date2,status,pageIndex,pageSize);
        return ResponseEntity.ok(mapServiceResult);
    }

    /**
     * API 1 : Get infor Export order
     */
    @GetMapping("/detail/{orderId}")
    public ResponseEntity<ServiceResult<Map<String,Object>>> getExportOrderDetail(@PathVariable Long orderId) {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            ServiceResult<Map<String, Object>> mapServiceResult = new ServiceResult<>();
            mapServiceResult.setMessage("Tài khoản của bạn đã bị tạm dừng!");
            mapServiceResult.setStatus1(Constant.NOT_Allow);
            return ResponseEntity.ok(mapServiceResult);
        }
        ServiceResult<Map<String,Object>> mapServiceResult =  exportGoodsService.getExportOrderDetail(orderId);
        return ResponseEntity.ok(mapServiceResult);
    }

    /**
     * API 2 : Get list product in Detail Export order
     */
    @GetMapping("/list-product-detail")
    public ResponseEntity<ServiceResult<Map<String,Object>>> getListProductOfExportGoods(@RequestParam(required = false) Long orderId,
                                                                                         @RequestParam(required = false) Integer pageIndex,
                                                                                         @RequestParam(required = false) Integer pageSize) {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            ServiceResult<Map<String, Object>> mapServiceResult = new ServiceResult<>();
            mapServiceResult.setMessage("Tài khoản của bạn đã bị tạm dừng!");
            mapServiceResult.setStatus1(Constant.NOT_Allow);
            return ResponseEntity.ok(mapServiceResult);
        }
        ServiceResult<Map<String,Object>> mapServiceResult =  exportGoodsService.getlistProductOfExportGoods(orderId,pageIndex,pageSize);
        return ResponseEntity.ok(mapServiceResult);
    }

    /**
     * Get list product Dropdown list
     */
    @GetMapping("/list-product")
    public ResponseEntity<Map<String,Object>> getListProductByImportDate() {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            Map<String, Object> mapServiceResult = new HashMap<>();
            mapServiceResult.put("message","Tài khoản của bạn đã bị tạm dừng!");
            mapServiceResult.put("status",Constant.NOT_Allow);
            return ResponseEntity.ok(mapServiceResult);
        }
        return ResponseEntity.ok(exportGoodsService.getListProductByImportDate());
    }

    /**
     * Get product Selected in Dropdown list
     */
    @GetMapping("/list-quantity-instock")
    public ResponseEntity<Map<String,Object>> getProductInstock(@RequestParam(required = false) Long productId,
                                                                @RequestParam(required = false) Integer pageIndex,
                                                                @RequestParam(required = false) Integer pageSize) {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            Map<String, Object> mapServiceResult = new HashMap<>();
            mapServiceResult.put("message","Tài khoản của bạn đã bị tạm dừng!");
            mapServiceResult.put("status",Constant.NOT_Allow);
            return ResponseEntity.ok(mapServiceResult);
        }
        pageIndex = pageIndex == null ? DEFAULT_PAGE : pageIndex;
        pageSize = pageSize == null ? DEFAULT_SIZE : pageSize;
        return ResponseEntity.ok(exportGoodsService.getProductInstock(productId,pageIndex,pageSize));
    }

    @PostMapping("/create")
    public ResponseEntity<?> importGoods(@RequestBody ExportGoodsRequest exportGoodsRequest) {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
        }
        return exportGoodsService.save(exportGoodsRequest);
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateImportGoods(@RequestBody UpdateExportOrder UpdateExportOrder) {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
        }
        return exportGoodsService.update(UpdateExportOrder);
    }


    @GetMapping("/confirm/{orderId}/{confirmUserId}")
    public ResponseEntity<?> confirmExportGoods(@PathVariable Long orderId,@PathVariable Long confirmUserId) {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
        }
        try {
            return exportGoodsService.confirm(orderId,confirmUserId);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/cancel/{orderId}")
    public ResponseEntity<?> cancelExportGoods(@PathVariable Long orderId) {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
        }
        try {
            return exportGoodsService.cancel(orderId);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
