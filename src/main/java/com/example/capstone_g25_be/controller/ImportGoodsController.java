package com.example.capstone_g25_be.controller;


import com.example.capstone_g25_be.entities.ServiceResult;
import com.example.capstone_g25_be.model.request.ImportGoodsRequest;
import com.example.capstone_g25_be.model.request.UpdateImportGoodsRequest;
import com.example.capstone_g25_be.model.response.CreatedByDto;
import com.example.capstone_g25_be.model.response.MessageResponse;
import com.example.capstone_g25_be.repository.ImportOrderRepository;
import com.example.capstone_g25_be.services.ImportGoodsService;
import com.example.capstone_g25_be.utils.Constant;
import com.example.capstone_g25_be.utils.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/import-order")
@Slf4j
public class ImportGoodsController {
    private final static int DEFAULT_PAGE = 0;
    private final static int DEFAULT_SIZE = 10;
    private final ImportGoodsService importGoodsService;

    @Autowired
    private ImportOrderRepository importOrderRepository;

    public ImportGoodsController(ImportGoodsService importGoodsService) {
        this.importGoodsService = importGoodsService;
    }

    @PostMapping("/create")
    public ResponseEntity<?> importGoods(@RequestBody ImportGoodsRequest importGoodsRequest) {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
        }
        return importGoodsService.save(importGoodsRequest);
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateImportGoods(@RequestBody UpdateImportGoodsRequest updateImportGoodsRequest) {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
        }
        return importGoodsService.update(updateImportGoodsRequest);
    }

    @GetMapping("/list")
    public ResponseEntity<ServiceResult<Map<String, Object>>> listImportGoods(Long userId,
                                                                              @RequestParam(required = false) String billReferenceNumber,
                                                                              @RequestParam(required = false) String startDate,
                                                                              @RequestParam(required = false) String endDate,
                                                                              @RequestParam(required = false) Integer pageIndex,
                                                                              @RequestParam(required = false) Integer pageSize,
                                                                              @RequestParam(required = false) Integer status) throws ParseException {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            ServiceResult<Map<String, Object>> mapServiceResult = new ServiceResult<>();
            mapServiceResult.setMessage("Tài khoản của bạn đã bị tạm dừng!");
            mapServiceResult.setStatus1(Constant.NOT_Allow);
            return ResponseEntity.ok(mapServiceResult);
        }
        pageIndex = pageIndex == null ? DEFAULT_PAGE : pageIndex;
        pageSize = pageSize == null ? DEFAULT_SIZE : pageSize;
        Date date1 = null;
        Date date2 = null;
        String s1 = "";
        String s2 = "";
        String billReferenceNumber1;
        if (billReferenceNumber == null || billReferenceNumber.equals("")) {
            billReferenceNumber1 = "";
        } else {
            billReferenceNumber1 = billReferenceNumber.trim();
        }

        if (s1.equals(startDate) & s2.equals(endDate)) {
            date1 = null;
            date2 = null;
        } else if (null == startDate & null == endDate) {
            date1 = null;
            date2 = null;
        } else if (s1.equals(startDate)) {
            date2 = new SimpleDateFormat("dd-MM-yyyy").parse(endDate);
        } else if (s2.equals(endDate)) {
            date1 = new SimpleDateFormat("dd-MM-yyyy").parse(startDate);
        } else {
            date1 = new SimpleDateFormat("dd-MM-yyyy").parse(startDate);
            date2 = new SimpleDateFormat("dd-MM-yyyy").parse(endDate);
        }

        try {
            ServiceResult<Map<String, Object>> mapServiceResult = importGoodsService.getListImportGoods(userId, billReferenceNumber1, date1, date2, status, pageIndex, pageSize);
            return ResponseEntity.ok(mapServiceResult);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping("/detail/{orderId}")
    public ResponseEntity<ServiceResult<Map<String, Object>>> getInforDetailImportGoods(@PathVariable Long orderId) {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            ServiceResult<Map<String, Object>> mapServiceResult = new ServiceResult<>();
            mapServiceResult.setMessage("Tài khoản của bạn đã bị tạm dừng!");
            mapServiceResult.setStatus1(Constant.NOT_Allow);
            return ResponseEntity.ok(mapServiceResult);
        }
        ServiceResult<Map<String, Object>> mapServiceResult = importGoodsService.getInforDetailImportGoods(orderId);
        return ResponseEntity.ok(mapServiceResult);
    }

    @GetMapping("/list-product")
    public ResponseEntity<ServiceResult<Map<String, Object>>> getListProductOfImportGoods(@RequestParam(required = false) Long orderId,
                                                                                          @RequestParam(required = false) Integer pageIndex,
                                                                                          @RequestParam(required = false) Integer pageSize) {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            ServiceResult<Map<String, Object>> mapServiceResult = new ServiceResult<>();
            mapServiceResult.setMessage("Tài khoản của bạn đã bị tạm dừng!");
            mapServiceResult.setStatus1(Constant.NOT_Allow);
            return ResponseEntity.ok(mapServiceResult);
        }
        ServiceResult<Map<String, Object>> mapServiceResult = importGoodsService.getlistProductOfImportGoods(orderId, pageIndex, pageSize);
        return ResponseEntity.ok(mapServiceResult);
    }

    @GetMapping("/confirm/{orderId}/{confirmUserId}")
    public ResponseEntity<?> confirmImportGoods(@PathVariable Long orderId, @PathVariable Long confirmUserId) {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
        }
        try {
            return importGoodsService.confirm(orderId, confirmUserId);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/cancel/{orderId}")
    public ResponseEntity<?> cancelImportGoods(@PathVariable Long orderId) {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
        }
        try {
            return  importGoodsService.cancel(orderId);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/list-create-by")
    public ResponseEntity<?> getListCreateBy() {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
        }
        List<Map<String, Object>> map;
        List<CreatedByDto> createdByDtos = null;
        try {
            map = importOrderRepository.getCreateBy();
            createdByDtos = CreatedByDto.createSuccessData(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(createdByDtos, HttpStatus.OK);
    }
}
