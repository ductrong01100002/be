package com.example.capstone_g25_be.controller;

import com.example.capstone_g25_be.model.response.MessageResponse;
import com.example.capstone_g25_be.utils.Constant;
import com.example.capstone_g25_be.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.capstone_g25_be.model.request.InventoryCheckingHistoryRequest;
import com.example.capstone_g25_be.services.InventoryCheckingHistoryService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/inventoryCheckingHistory")
public class InventoryCheckingHistoryController {
	
	private final int defaultPage = 1;
	private final int defaultSize = 10;
	
	@Autowired
	InventoryCheckingHistoryService inventoryCheckingHistoryService;
	
	@PostMapping(path = "/add")
	public ResponseEntity<?> addInventoryCheckingHistory(
			@RequestBody InventoryCheckingHistoryRequest inventoryCheckingHistoryRequest){
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return inventoryCheckingHistoryService.addInventoryCheckingHistory(inventoryCheckingHistoryRequest);
	}
	
	@GetMapping
    public ResponseEntity<?> listInventoryCheckingHistory(
    		@RequestParam(required = false) Integer pageIndex,
			@RequestParam(required = false) Integer pageSize,
			@RequestParam(required = false) Long wareHouseId,
			@RequestParam(required = false) Long userId,
			@RequestParam(required = false) String startDate,
			@RequestParam(required = false) String endDate,
			@RequestParam(required = false) String orderBy,
			@RequestParam(required = true) String order) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		pageIndex = pageIndex == null ? defaultPage : pageIndex;
		pageSize = pageSize == null ? defaultSize : pageSize;
		
		return inventoryCheckingHistoryService.findAll(pageIndex, pageSize, wareHouseId, userId,
				startDate, endDate, orderBy, order);
    }
	
	@GetMapping("/detail/{inventoryCheckingHistoryId}")
    public ResponseEntity<?> getInventoryCheckingHistoryDetail(
    		@PathVariable Long inventoryCheckingHistoryId) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return inventoryCheckingHistoryService.findByIdAndFindListDetail(inventoryCheckingHistoryId);
    }

}
