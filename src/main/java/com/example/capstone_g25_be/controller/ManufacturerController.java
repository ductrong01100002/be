package com.example.capstone_g25_be.controller;

import com.example.capstone_g25_be.model.response.MessageResponse;
import com.example.capstone_g25_be.utils.Constant;
import com.example.capstone_g25_be.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.capstone_g25_be.model.request.ManufacturerRequest;
import com.example.capstone_g25_be.services.ManufacturerService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/manufacturer")
public class ManufacturerController {

	private final int defaultPage = 1;
	private final int defaultSize = 10;

	@Autowired
	ManufacturerService manufacturerService;

	@GetMapping
	public ResponseEntity<?> listManufacturer(@RequestParam(required = false) Integer pageIndex,
			@RequestParam(required = false) Integer pageSize, @RequestParam(required = false) String manufactorName,
			@RequestParam(required = false) String manufactorEmail,
			@RequestParam(required = false) Integer manufactorPhone) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		pageIndex = pageIndex == null ? defaultPage : pageIndex;
		pageSize = pageSize == null ? defaultSize : pageSize;

		return manufacturerService.findAll(pageIndex, pageSize, manufactorName, manufactorEmail);
	}

	@PostMapping("/add")
	public ResponseEntity<?> addManufacturer(@RequestBody ManufacturerRequest manufacturerRequest) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return manufacturerService.addManufacturer(manufacturerRequest);
	}
	
	@GetMapping("/{manufactorId}")
    public ResponseEntity<?> manufactorDetail(@PathVariable Long manufactorId,
    		@RequestParam(required = false) Integer pageIndex,
			@RequestParam(required = false) Integer pageSize) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		pageIndex = pageIndex == null ? defaultPage : pageIndex;
		pageSize = pageSize == null ? defaultSize : pageSize;
		return manufacturerService.findById(manufactorId, pageIndex, pageSize);
    }
	
	@GetMapping("/notPaging")
	public ResponseEntity<?> listManufacturerNotPaging() {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return manufacturerService.findAllNotPaging();
	}

}
