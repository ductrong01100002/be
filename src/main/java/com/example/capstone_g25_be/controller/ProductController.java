package com.example.capstone_g25_be.controller;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.example.capstone_g25_be.model.response.MessageResponse;
import com.example.capstone_g25_be.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.capstone_g25_be.model.request.ProductRequest;
import com.example.capstone_g25_be.services.ProductService;
import com.example.capstone_g25_be.utils.Constant;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/product")
public class ProductController {
	
	private final int defaultPage = 1;
	private final int defaultSize = 10;
	
	@Autowired
	private ProductService productService;
	
	@GetMapping
    public ResponseEntity<?> listProducts(
    		@RequestParam(required = false) Integer pageIndex,
			@RequestParam(required = false) Integer pageSize,
			@RequestParam(required = false) String productName,
			@RequestParam(required = false) String productCode,
			@RequestParam(required = false) Long manufactorId,
			@RequestParam(required = false) Long categoryId) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		pageIndex = pageIndex == null ? defaultPage : pageIndex;
		pageSize = pageSize == null ? defaultSize : pageSize;
		
		return productService.findAll(pageIndex, pageSize, productName,
				productCode, categoryId, manufactorId);
    }
	
	@GetMapping("/{productId}")
    public ResponseEntity<?> productDetail(
    		@PathVariable Long productId,
    		@RequestParam(required = false) Integer pageIndex,
			@RequestParam(required = false) Integer pageSize) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		pageIndex = pageIndex == null ? defaultPage : pageIndex;
		pageSize = pageSize == null ? defaultSize : pageSize;
		return productService.findById(productId, pageIndex, pageSize);
    }
	
	@PostMapping(path = "/add")
	public ResponseEntity<?> addProduct(@RequestBody ProductRequest productRequest){
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return productService.addProduct(productRequest);
	}
	
	@PutMapping(path = "/update")
	public ResponseEntity<?> updateProduct(@RequestBody ProductRequest productRequest){
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return productService.updateProduct(productRequest);
	}
	
	@PostMapping(path = "/add/image",
			consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> addImageNewProduct(@RequestParam("file") MultipartFile file){
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return productService.addImageProduct(file);
	}
	
	@PutMapping(
			path = "update/image/{productId}",
			consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE
			)
	public ResponseEntity<?> updateImageProduct(
			@PathVariable("productId") Long productId,
			@RequestParam("file") MultipartFile file) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return productService.updateImageProduct(productId, file);
	}
	
	@GetMapping("/download/image/{productId}")
	public byte[] downloadUserProfileImage(
			@PathVariable("productId") Long productId) {
		return productService.downloadProductImage(productId);
	}

	// Import excel

	/**
	 * download shift template
	 * @param response
	 * @return
	 */
	@GetMapping(value = "/download-template", produces = MediaType.ALL_VALUE)
	public ResponseEntity<Object> downloadTemplate(HttpServletResponse response) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		try {
			String path = getClass().getClassLoader().getResource(Constant.TEMPLATE_FOLDER).getPath();
			File file = new File(path + Constant.STR_SLASH + Constant.IMPORT_PRODUCT_TEMPLATE);
			byte[] byteArray = Files.readAllBytes(file.getAbsoluteFile().toPath());
			return ResponseEntity.ok()
					.contentType(MediaType.APPLICATION_OCTET_STREAM)
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + Constant.IMPORT_PRODUCT_TEMPLATE)
					.body(byteArray);
		} catch (Exception e) {
			Map<String,Object> res = new HashMap<>();
			res.put("code",Constant.SUCCESS);
			return ResponseEntity.status(HttpStatus.OK).body(res);
		}
	}
	@PostMapping("/import-product")
	public ResponseEntity<Object> importShift(@RequestParam(name = "file") MultipartFile file) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		try {
			return ResponseEntity.status(HttpStatus.OK).body(productService.importProduct(file));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
	
	@GetMapping("/wareHouse/{wareHouseId}")
    public ResponseEntity<?> getProductByWareHouse(
    		@PathVariable Long wareHouseId) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return productService.findProductByWareHouseId(wareHouseId);
    }
	
	@GetMapping("/consignment/{productId}")
    public ResponseEntity<?> getProductAndListConsignment(
    		@PathVariable Long productId,
    		@RequestParam Long warehouseId) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return productService.findByIdAndFindListConsignment(productId, warehouseId);
    }
	
	@GetMapping("/manufacturer/{manufacturerId}")
    public ResponseEntity<?> getProductBymanufacturer(
    		@PathVariable Long manufacturerId) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return productService.findProductBymanufacturerId(manufacturerId);
    }
	
}
