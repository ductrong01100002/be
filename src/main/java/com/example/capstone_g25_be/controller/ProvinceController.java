package com.example.capstone_g25_be.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.capstone_g25_be.model.response.MessageResponse;
import com.example.capstone_g25_be.services.ProvinceService;
import com.example.capstone_g25_be.utils.Constant;
import com.example.capstone_g25_be.utils.Utils;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/province")
public class ProvinceController {

	private final int defaultPage = 1;
	private final int defaultSize = 10;

	@Autowired
	ProvinceService provinceService;

	@GetMapping
	public ResponseEntity<?> listProvince() {
		boolean isActive = Utils.checkIsActive();
		if (!isActive) {
			return ResponseEntity.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return provinceService.findAll();
	}

}
