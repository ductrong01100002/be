package com.example.capstone_g25_be.controller;


import com.example.capstone_g25_be.entities.ServiceResult;
import com.example.capstone_g25_be.model.request.ExportGoodsRequest;
import com.example.capstone_g25_be.model.request.ReturnOrderRequest;
import com.example.capstone_g25_be.model.response.MessageResponse;
import com.example.capstone_g25_be.services.ReturnOrderService;
import com.example.capstone_g25_be.utils.Constant;
import com.example.capstone_g25_be.utils.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/return-order")
@Slf4j
public class ReturnOrderController {
    private final static int DEFAULT_PAGE = 0;
    private final static int DEFAULT_SIZE = 10;

    @Autowired
    private ReturnOrderService returnOrderService;

    /**
     * Get list return order
     */
    @GetMapping("/list")
    public ResponseEntity<Object> getListReturnOrder(@RequestParam(required = false) Long userId,
                                                                                @RequestParam(required = false) String billReferenceNumber,
                                                                                String startDate,
                                                                                String endDate,
                                                                                @RequestParam(required = false) Integer pageIndex,
                                                                                @RequestParam(required = false) Integer pageSize) throws ParseException {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
        }
        pageIndex = pageIndex == null ? DEFAULT_PAGE : pageIndex;
        pageSize = pageSize == null ? DEFAULT_SIZE : pageSize;
        Date date1;
        Date date2;
        String billReferenceNumber1;

        if(billReferenceNumber==null || billReferenceNumber.equals("")){
            billReferenceNumber1 = "";
        } else{
            billReferenceNumber1 = billReferenceNumber.trim();
        }
        if(startDate==null || startDate.equals("")||endDate==null|| endDate.equals("")){
            date1 = null;
            date2 =null ;
        } else{
            date1 = new SimpleDateFormat("dd-MM-yyyy").parse(startDate);
            date2 = new SimpleDateFormat("dd-MM-yyyy").parse(endDate);
        }

        ServiceResult<Map<String,Object>> mapServiceResult =  returnOrderService.getListReturnOrder(userId,billReferenceNumber1,date1,date2,pageIndex,pageSize);
        return ResponseEntity.ok(mapServiceResult);
    }

    /**
     * API 2 : Get list product in Detail Export order
     */
    @GetMapping("/detail")
    public ResponseEntity<ServiceResult<Map<String,Object>>> getDetailReturnOrder(@RequestParam(required = false) Long orderId,
                                                                                         @RequestParam(required = false) Integer pageIndex,
                                                                                         @RequestParam(required = false) Integer pageSize) {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            ServiceResult<Map<String, Object>> mapServiceResult = new ServiceResult<>();
            mapServiceResult.setMessage("Tài khoản của bạn đã bị tạm dừng!");
            mapServiceResult.setStatus1(Constant.NOT_Allow);
            return ResponseEntity.ok(mapServiceResult);
        }
        ServiceResult<Map<String,Object>> mapServiceResult =  returnOrderService.getDetailReturnOrder(orderId,pageIndex,pageSize);
        return ResponseEntity.ok(mapServiceResult);
    }

    @PostMapping("/return/{orderId}")
    public ResponseEntity<?> returnOrder(@PathVariable Long orderId,@RequestBody ReturnOrderRequest returnOrderRequest) {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
        }
        try {
            return returnOrderService.returnOrder(orderId,returnOrderRequest);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
