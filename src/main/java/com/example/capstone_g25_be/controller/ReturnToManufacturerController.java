package com.example.capstone_g25_be.controller;

import com.example.capstone_g25_be.model.response.MessageResponse;
import com.example.capstone_g25_be.utils.Constant;
import com.example.capstone_g25_be.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.capstone_g25_be.model.request.ReturnToManufacturerRequest;
import com.example.capstone_g25_be.services.ReturnToManufacturerService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/returnToManufacturer")
public class ReturnToManufacturerController {
	
	private final int defaultPage = 1;
	private final int defaultSize = 10;
	
	@Autowired
	ReturnToManufacturerService returnToManufacturerService;
	
	@PostMapping(path = "/add")
	public ResponseEntity<?> addReturnToManufacturer(
			@RequestBody ReturnToManufacturerRequest returnToManufacturerRequest){
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return returnToManufacturerService.addReturnToManufacturer(returnToManufacturerRequest);
	}
	
	@GetMapping
    public ResponseEntity<?> listReturnToManufacturer(
    		@RequestParam(required = false) Integer pageIndex,
			@RequestParam(required = false) Integer pageSize,
			@RequestParam(required = false) Long userId,
			@RequestParam(required = false) Long statusId,
			@RequestParam(required = false) Long manufacturerId,
			@RequestParam(required = false) String startDate,
			@RequestParam(required = false) String endDate) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		pageIndex = pageIndex == null ? defaultPage : pageIndex;
		pageSize = pageSize == null ? defaultSize : pageSize;
		
		return returnToManufacturerService.findAll(pageIndex, pageSize, manufacturerId, userId, statusId,
				startDate, endDate);
    }
	
	@GetMapping("/detail/{returnToManufacturerId}")
    public ResponseEntity<?> getReturnToManufacturerDetail(
    		@PathVariable Long returnToManufacturerId) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return returnToManufacturerService.findByIdAndFindListDetail(returnToManufacturerId);
    }
	
	@PutMapping("/update/{returnToManufacturerId}")
    public ResponseEntity<?> updateReturnToManufacturerDetail(
    		@PathVariable Long returnToManufacturerId,
    		@RequestBody ReturnToManufacturerRequest returnToManufacturerRequest) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return returnToManufacturerService.updateReturnToManufacturer(returnToManufacturerId, returnToManufacturerRequest);
    }
	
	@GetMapping("/confirm/{returnToManufacturerId}")
    public ResponseEntity<?> confirmReturnToManufacturer(
    		@PathVariable Long returnToManufacturerId,
    		@RequestParam(required = true) Long userConfirmedId) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return returnToManufacturerService.confirmReturnToManufacturer(returnToManufacturerId, userConfirmedId);
    }
	
	@DeleteMapping("/delete/{returnToManufacturerId}")
    public ResponseEntity<?> cancelReturnToManufacturer(
    		@PathVariable Long returnToManufacturerId,
    		@RequestParam(required = true) Long userDeleteId) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return returnToManufacturerService.deleteReturnToManufacturer(returnToManufacturerId, userDeleteId);
    }

}
