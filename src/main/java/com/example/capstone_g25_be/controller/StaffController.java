package com.example.capstone_g25_be.controller;

import com.example.capstone_g25_be.entities.Role;
import com.example.capstone_g25_be.entities.ServiceResult;
import com.example.capstone_g25_be.entities.User;
import com.example.capstone_g25_be.model.request.NewPassword;
import com.example.capstone_g25_be.model.request.ResetPasswordRequest;
import com.example.capstone_g25_be.model.request.UpdateStaffRequest;
import com.example.capstone_g25_be.model.response.MessageResponse;
import com.example.capstone_g25_be.model.response.ProfileResponse;
import com.example.capstone_g25_be.model.response.UserResponse;
import com.example.capstone_g25_be.repository.RoleRepository;
import com.example.capstone_g25_be.repository.StaffRepository;
import com.example.capstone_g25_be.repository.UserRepository;
import com.example.capstone_g25_be.services.StaffService;
import com.example.capstone_g25_be.services.UserService;
import com.example.capstone_g25_be.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.*;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/staff")
public class StaffController {

    @Autowired
    private StaffService staffService;

    @Autowired
    private StaffRepository staffRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private EmailSender emailSender;

    @GetMapping("/get-role")
    public ResponseEntity<?> getRole() {
        return new ResponseEntity<>(roleRepository.getRole(),HttpStatus.OK);
    }

    @GetMapping("/list")
    public ResponseEntity<?> getListStaff(String keyWords,String sortColumn, @RequestParam(required = false) Integer pageIndex,
                                          @RequestParam(required = false) Integer pageSize) {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
        }
        Map<String, Object> map = new HashMap<>();
        try {
            map = staffService.getListStaff(keyWords.trim(),sortColumn, pageIndex, pageSize);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<?> getDetailStaff(@PathVariable Long id) {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
        }
        Map<String, Object> map = new HashMap<>();
        try {
            map = staffService.getDetailStaff(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @GetMapping("/detail-profile/{id}")
    public ResponseEntity<?> getDetailProfile(@PathVariable Long id) {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
        }
        Map<String, Object> map = new HashMap<>();
        try {
            User user = userRepository.findById(id).orElseThrow(() -> new RuntimeException("Mã nhân viên không tồn tại !"));;
            Map<String, Object> output = staffRepository.getDetailProfile(id);
            map.put("data", ProfileResponse.createSuccessData(user,output));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @PostMapping("/update")
    public ResponseEntity<?> updateStaff(@Valid @RequestBody UpdateStaffRequest staffRequest) {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
        }
        try {
            staffService.updateStaff(staffRequest);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/update-status/{id}")
    public ResponseEntity<?> setActiveONotActiveStaff(@PathVariable Long id, @RequestParam(required = false) Boolean isActive) {
        boolean isActive1 = Utils.checkIsActive();
        if(!isActive1){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
        }
        User user;
        Map<String, Object> map = new HashMap<>();
        try {
            user = userRepository.getById(id);
            user.setActive(isActive);
            userRepository.save(user);
            map.put("message", "Cập nhật trạng thái thành công !");
            map.put("code", Constant.SUCCESS);
        } catch (Exception e) {
            map.put("message", "Cập nhật trạng thái thất bại !");
            map.put("code", Constant.FAIL);
            e.printStackTrace();
        }
        return new ResponseEntity<>(map, HttpStatus.OK);
    }
    //owner reset pass for user
    @PostMapping("/reset-password")
    public ResponseEntity<?> resetPassword(@RequestBody ResetPasswordRequest resetPasswordRequest) {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
        }
        Map<String, Object> map = new HashMap<>();
        try {
            Optional<User> user = userRepository.findById(resetPasswordRequest.getId());
            if(!user.isPresent()){
                map.put("message", "Đặt lại mật khẩu thất bại !");
                map.put("code", Constant.FAIL);
            }
            String email = null;
            boolean result = false;
            String password = String.valueOf(GeneratePass.randomPass(8));
            //th truyền email = ""
            if(resetPasswordRequest.getEmail()==null ||resetPasswordRequest.getEmail().equals("")){
                user.get().setPassWord(encoder.encode(password));
                email = userRepository.findEmailByUserName(user.get().getUserName());
                result = emailSender.sendMailChangePassword(email, password,user.get().getFullName());
            }else{
                //th truyền email
                user.get().setPassWord(encoder.encode(password));
                result = emailSender.sendMailChangePassword(resetPasswordRequest.getEmail(), password,user.get().getFullName());
            }

            if (result) {
                map.put("message", "Đặt lại mật khẩu thành công !");
                map.put("code", Constant.SUCCESS);
                userRepository.save(user.get());
            } else {
                map.put("message", "Đặt lại mật khẩu thất bại !");
                map.put("code", Constant.FAIL);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @GetMapping("/update-role/{id}")
    public ResponseEntity<?> setRole(@PathVariable Long id,
                                     @RequestParam(required = false) Long roleId) {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
        }
        Map<String, Object> map = new HashMap<>();
        try {
            Role role = roleRepository.findById(roleId).orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            Optional<User> user = userRepository.findById(id);
            Set<Role> roles = new HashSet<>();
            roles.add(role);
            user.get().setRoles(roles);
            userRepository.save(user.get());
            map.put("message", "Cập nhật chức vụ thành công !");
            map.put("code", Constant.SUCCESS);
        } catch (Exception e) {
            map.put("message", "Cập nhật chức vụ thất bại !");
            map.put("code", Constant.FAIL);
            e.printStackTrace();
        }
        return new ResponseEntity<>(map,HttpStatus.OK);
    }

    //API image
    @PostMapping(path = "/add/image",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addImageNewUser(@RequestParam("file") MultipartFile file){
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
        }
        return userService.addImageUser(file);
    }

    @PutMapping(
            path = "update/image/{userId}",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> uploadUserProfileImage(
            @PathVariable("userId") Long userId,
            @RequestParam("file") MultipartFile file) {
        boolean isActive = Utils.checkIsActive();
        if(!isActive){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
        }
        return userService.updateImageUser(userId, file);
    }

    @GetMapping("/download/image/{userId}")
    public byte[] downloadUserProfileImage(
            @PathVariable("userId") Long userId) {
        return userService.downloadUserImage(userId);
    }


}
