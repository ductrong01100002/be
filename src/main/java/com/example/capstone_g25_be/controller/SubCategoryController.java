package com.example.capstone_g25_be.controller;

import com.example.capstone_g25_be.model.response.MessageResponse;
import com.example.capstone_g25_be.utils.Constant;
import com.example.capstone_g25_be.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.capstone_g25_be.model.request.SubCategoryRequest;
import com.example.capstone_g25_be.services.SubCategoryService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/subCategory")
public class SubCategoryController {
	
	private final int defaultPage = 1;
	private final int defaultSize = 10;
	
	@Autowired
	SubCategoryService subCategoryService;
	
	@GetMapping
    public ResponseEntity<?> getSubCategoryByCategoryId(
    		@RequestParam(required = false) Long categoryId) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return subCategoryService.findSubCategoryByCategoryId(categoryId);
    }
	
	@PostMapping("/add")
	public ResponseEntity<?> addSubCategory(@RequestBody SubCategoryRequest subCategoryRequest){
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return subCategoryService.addSubCategory(subCategoryRequest);
	}
	
	@PutMapping("/update")
	public ResponseEntity<?> updateSubCategory(@RequestBody SubCategoryRequest subCategoryRequest){
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return subCategoryService.updateSubCategory(subCategoryRequest);
	}

}
