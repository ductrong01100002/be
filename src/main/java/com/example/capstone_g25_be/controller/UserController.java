package com.example.capstone_g25_be.controller;

import com.example.capstone_g25_be.entities.ServiceResult;
import com.example.capstone_g25_be.model.request.CheckOTPForm;
import com.example.capstone_g25_be.model.request.ForgotPasswordForm;
import com.example.capstone_g25_be.model.request.NewPassword;
import com.example.capstone_g25_be.model.request.SettingPassword;
import com.example.capstone_g25_be.model.response.MessageResponse;
import com.example.capstone_g25_be.utils.Constant;
import com.example.capstone_g25_be.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.capstone_g25_be.services.UserService;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/user")
public class UserController {
	
	private final int defaultPage = 1;
	private final int defaultSize = 10;
	
	@Autowired
	UserService userService;
	
	@GetMapping
    public ResponseEntity<?> listProducts(
    		@RequestParam(required = false) Integer pageIndex,
			@RequestParam(required = false) Integer pageSize,
			@RequestParam(required = false) String userName) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		pageIndex = pageIndex == null ? defaultPage : pageIndex;
		pageSize = pageSize == null ? defaultSize : pageSize;
		
		return userService.findAll(pageIndex, pageSize, userName);
    }

	@GetMapping("/list")
	public ResponseEntity<?> listUser(
			@RequestParam(required = false) Integer pageIndex,
			@RequestParam(required = false) Integer pageSize,
			@RequestParam(required = false) String searchData) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		pageIndex = pageIndex == null ? defaultPage : pageIndex;
		pageSize = pageSize == null ? defaultSize : pageSize;

		return userService.getListUser(pageIndex, pageSize, searchData);
	}

	@PostMapping("/forgot_password")
	ServiceResult<Boolean> forgotPassword(@RequestBody ForgotPasswordForm forgotPasswordForm) {
		return userService.resetPassword(forgotPasswordForm.getUserName());
	}

	@PostMapping("/check_otp")
	ResponseEntity<?> checkOTP(@RequestBody CheckOTPForm checkOTPForm) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return userService.checkOTPandResetPass(checkOTPForm.getOtp(), checkOTPForm.getUserName());
	}
	//password ở ngoai man hình chính
	@PostMapping("/create-password")
	ResponseEntity<?> createNewPassword(@RequestBody SettingPassword settingPassword) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return userService.createPasswword(settingPassword.getUserName(),settingPassword.getNewPassword());
	}

	//password ở  trong của owner
	@PostMapping("/new-password")
	ResponseEntity<?> checkOTP(@RequestBody NewPassword newPassword) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return userService.settingPasswword(newPassword.getOldPassword(),newPassword.getNewPassword(), newPassword.getUserName());
	}


}
