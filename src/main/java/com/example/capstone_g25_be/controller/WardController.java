package com.example.capstone_g25_be.controller;

import com.example.capstone_g25_be.model.response.MessageResponse;
import com.example.capstone_g25_be.utils.Constant;
import com.example.capstone_g25_be.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.capstone_g25_be.services.DistrictService;
import com.example.capstone_g25_be.services.WardService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/ward")
public class WardController {
	
	private final int defaultPage = 1;
	private final int defaultSize = 10;
	
	@Autowired
	WardService wardService;
	
	@GetMapping("/{districtId}")
    public ResponseEntity<?> ListWard(
    		@PathVariable Long districtId,
    		@RequestParam(required = false) Integer pageIndex,
			@RequestParam(required = false) Integer pageSize) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		pageIndex = pageIndex == null ? defaultPage : pageIndex;
		pageSize = pageSize == null ? defaultSize : pageSize;
		return wardService.findByDistrictId(districtId);
    }

}
