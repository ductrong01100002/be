package com.example.capstone_g25_be.controller;

import com.example.capstone_g25_be.model.response.MessageResponse;
import com.example.capstone_g25_be.utils.Constant;
import com.example.capstone_g25_be.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.capstone_g25_be.model.request.WareHouseRequest;
import com.example.capstone_g25_be.repository.WarehouseRepository;
import com.example.capstone_g25_be.services.WareHouseService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/warehouse")
public class WareHouseController {
	
	@Autowired
	WarehouseRepository wareHouseRepository;
	
	@Autowired
	WareHouseService wareHouseService;

	private final int defaultPage = 1;
	private final int defaultSize = 10;
	
	@GetMapping
    public ResponseEntity<?> listWareHouse(
    		@RequestParam(required = false) Integer pageIndex,
			@RequestParam(required = false) Integer pageSize, 
			@RequestParam(required = false) String wareHouseName) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		pageIndex = pageIndex == null ? defaultPage : pageIndex;
		pageSize = pageSize == null ? defaultSize : pageSize;
		
		return wareHouseService.findAll(pageIndex, pageSize, wareHouseName);
    }
	
	@GetMapping("/{wareHouseId}")
    public ResponseEntity<?> manufactorDetail(@PathVariable Long wareHouseId) {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return wareHouseService.findWareHouseById(wareHouseId);
    }
	
	@PostMapping("/add")
	public ResponseEntity<?> addWareHouse(@RequestBody WareHouseRequest wareHouseRequest){
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		
		return wareHouseService.addWareHouse(wareHouseRequest);
	}
	
	@PutMapping("/update")
	public ResponseEntity<?> updateWareHouse(@RequestBody WareHouseRequest wareHouseRequest){
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return wareHouseService.updateWareHouse(wareHouseRequest);
	}
	
	@DeleteMapping("/delete/{wareHouseId}")
	public ResponseEntity<?> deleteWareHouse(@PathVariable Long wareHouseId){
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return wareHouseService.deleteWareHouse(wareHouseId);
	}
	
	@GetMapping("/notPaging")
	public ResponseEntity<?> listWareHouseNotPaging() {
		boolean isActive = Utils.checkIsActive();
		if(!isActive){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Tài khoản của bạn đã bị tạm dừng!", Constant.NOT_Allow));
		}
		return wareHouseService.findAllNotPaging();
	}
}
