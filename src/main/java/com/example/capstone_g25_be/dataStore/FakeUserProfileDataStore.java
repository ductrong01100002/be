package com.example.capstone_g25_be.dataStore;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Repository;

import com.example.capstone_g25_be.profile.UserProfile;


@Repository
public class FakeUserProfileDataStore {
	
	private static final List<UserProfile> USER_PROFILES = new ArrayList<>();
	
	static {
		USER_PROFILES.add(new UserProfile(UUID.fromString("b5ae9b09-aa90-403d-a0f6-96884b17ebc7"), "chinh", null));
		USER_PROFILES.add(new UserProfile(UUID.fromString("82f2cd6a-2af3-4d18-a6c3-f8efb06160de"), "trong", null));
	}
	
	public List<UserProfile> getUserProfiles(){
		return USER_PROFILES;
	}
	

}
