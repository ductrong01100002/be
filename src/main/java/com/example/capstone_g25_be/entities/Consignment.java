package com.example.capstone_g25_be.entities;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Consignment")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Consignment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private LocalDateTime expirationDate;
	
	private LocalDateTime importDate;
	
	private Double unitPrice;
	
	private Integer quantity;
	
	private Boolean deletedAt;
	
	@OneToMany(mappedBy = "consignment", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<OrderDetail> orderDetails;
	
	@OneToMany(mappedBy = "consignment", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<InventoryCheckingHistoryDetail> inventoryCheckingHistoryDetails;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    private Product product;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "warehouse_id", referencedColumnName = "id")
    private Warehouse warehouse;

	public Consignment(Long id, LocalDateTime expirationDate, LocalDateTime importDate, Double unitPrice, Integer quantity) {
		this.id = id;
		this.expirationDate = expirationDate;
		this.importDate = importDate;
		this.unitPrice = unitPrice;
		this.quantity = quantity;
	}

	public Consignment(Long id, LocalDateTime expirationDate, LocalDateTime importDate, Double unitPrice, Integer quantity, Product product) {
		this.id = id;
		this.expirationDate = expirationDate;
		this.importDate = importDate;
		this.unitPrice = unitPrice;
		this.quantity = quantity;
		this.product = product;
	}
}
