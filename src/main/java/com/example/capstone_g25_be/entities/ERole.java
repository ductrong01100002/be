package com.example.capstone_g25_be.entities;


public enum ERole {
    ROLE_OWNER,
    ROLE_SELLER,
    ROLE_STOREKEEPER
}
