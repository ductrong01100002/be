package com.example.capstone_g25_be.entities;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "InventoryCheckingHistory")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InventoryCheckingHistory {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private LocalDateTime createDate; 
	
	private Boolean deletedAt;
	
	private double totalDifferentAmout;
	
	@OneToMany(mappedBy = "inventoryCheckingHistory", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<InventoryCheckingHistoryDetail> inventoryCheckingHistoryDetails;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "warehouse_id", referencedColumnName = "id")
    private Warehouse warehouse;

}
