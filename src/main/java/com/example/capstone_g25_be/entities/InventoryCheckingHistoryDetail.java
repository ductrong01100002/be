package com.example.capstone_g25_be.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "InventoryCheckingHistoryDetail")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InventoryCheckingHistoryDetail {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Integer instockQuantity;
	
	private Integer realityQuantity;
	
	private double differentAmout;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "consignment_id", referencedColumnName = "id")
    private Consignment consignment;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "inventoryCheckingHistory_id", referencedColumnName = "id")
    private InventoryCheckingHistory inventoryCheckingHistory;

}
