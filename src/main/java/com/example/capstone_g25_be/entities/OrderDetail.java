package com.example.capstone_g25_be.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "OrderDetail")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetail {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Double unitPrice;
	
	private Integer quantity;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "order_id", referencedColumnName = "id")
    private Order order;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "consignment_id", referencedColumnName = "id")
    private Consignment consignment;

	public OrderDetail(Long id, Double unitPrice, Integer quantity) {
		this.id = id;
		this.unitPrice = unitPrice;
		this.quantity = quantity;
	}

}
