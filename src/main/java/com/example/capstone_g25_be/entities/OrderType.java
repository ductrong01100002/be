package com.example.capstone_g25_be.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "OrderType")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderType {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String name;
	
	private String description;
	
	private Boolean deletedAt;
	
	@OneToMany(mappedBy = "orderType", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Order> orders;

	public OrderType(Long id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}
}
