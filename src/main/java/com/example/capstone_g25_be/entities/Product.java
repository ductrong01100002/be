package com.example.capstone_g25_be.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Product")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String name;

	private String productCode;

	private String unitMeasure;

	private String wrapUnitMeasure;

	private Integer numberOfWrapUnitMeasure;

	private String color;
	
	private String image;
	
	private String description;

	private Boolean deletedAt;

	private double unitPrice;
	
	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Consignment> consignments;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    private Category category;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "subCategory_id", referencedColumnName = "id")
    private SubCategory subCategory;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "manufacturer_id", referencedColumnName = "id")
    private Manufacturer manufacturer;

	public Product(Long id, String name, String productCode, String unitMeasure, String wrapUnitMeasure, Integer numberOfWrapUnitMeasure, String color, String image, String description, double unitPrice) {
		this.id = id;
		this.name = name;
		this.productCode = productCode;
		this.unitMeasure = unitMeasure;
		this.wrapUnitMeasure = wrapUnitMeasure;
		this.numberOfWrapUnitMeasure = numberOfWrapUnitMeasure;
		this.color = color;
		this.image = image;
		this.description = description;
		this.unitPrice = unitPrice;
	}
}
