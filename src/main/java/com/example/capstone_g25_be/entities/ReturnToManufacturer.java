package com.example.capstone_g25_be.entities;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "ReturnToManufacturer")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReturnToManufacturer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private LocalDateTime createDate;
	
	private LocalDateTime confirmedDate;
	
	private LocalDateTime returnDate; 
	
	private LocalDateTime expectedReturnDate;
	
	private Long userConfirmedId;
	
	private Boolean deletedAt;
	
	private double totalAmout;
	
	private String description;
	
	@OneToMany(mappedBy = "returnToManufacturer", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<ReturnToManufacturerDetail> returnToManufacturerDetails;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_create_id", referencedColumnName = "id")
    private User user;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "warehouse_id", referencedColumnName = "id")
    private Warehouse warehouse;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "manufacturer_id", referencedColumnName = "id")
    private Manufacturer manufacturer;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "status_id", referencedColumnName = "id")
    private Status status;

}
