package com.example.capstone_g25_be.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "ReturnToManufacturerDetail")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReturnToManufacturerDetail {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String productName;

	private Integer quantity;
	
	private String unitMeasure;

	private double unitPrice;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "returnToManufacturer_id", referencedColumnName = "id")
	private ReturnToManufacturer returnToManufacturer;

}
