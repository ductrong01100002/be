package com.example.capstone_g25_be.entities;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "user",
uniqueConstraints = {
        @UniqueConstraint(columnNames = {"username", "email"})
}
)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(name = "username")
    private String userName;

    @NotBlank
    @Column(name = "password")
    private String passWord;
    
    private Boolean gender;

    @JsonFormat(pattern="dd/MM/YYYY")
    private LocalDate dateOfBirth;
    
    private String email;
    
    private String identityCard;
    
    private String phone;

    private String fullName;

    private String otp;

    private Boolean active;
    
    private Boolean deleteAt;

    private LocalDateTime resetDate;

    private String image;
    
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Order> orders;
    
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<InventoryCheckingHistory> inventoryCheckingHistories;
    
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<ReturnToManufacturer> returnToManufacturers;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    private Address address;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable( name = "user_roles",
                joinColumns = @JoinColumn(name ="user_id"),
                inverseJoinColumns = @JoinColumn(name ="role_id"))
    private Set<Role> roles = new HashSet<>();

    public User(String userName, String passWord, Boolean gender, LocalDate dateOfBirth, String email, String identityCard, String phone,String fullName) {
        this.userName = userName;
        this.passWord = passWord;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.email = email;
        this.identityCard = identityCard;
        this.phone = phone;
        this.fullName = fullName;
    }

    public User(Long id, String userName, String passWord, Boolean gender, LocalDate dateOfBirth, String email, String identityCard, String phone, String fullName) {
        this.id = id;
        this.userName = userName;
        this.passWord = passWord;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.email = email;
        this.identityCard = identityCard;
        this.phone = phone;
        this.fullName = fullName;
    }
}
