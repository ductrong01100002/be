package com.example.capstone_g25_be.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddressRequest {
	
	private Long id;
	
	private String addressDetail;
	
	private Long provinceId;
	
	private Long districtId;
	
	private Long wardId;

}
