package com.example.capstone_g25_be.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
public class ConsignmentExport {
    private Long id;
    private int quantity;
    private double unitPrice;
}
