package com.example.capstone_g25_be.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
@Getter
@Setter
@AllArgsConstructor
public class ConsignmentRequest {
        private Long consignmentId;
        private Long productId;
        private LocalDateTime expirationDate;
        private LocalDateTime importDate;
        private double unitPrice;
        private int quantity;
}
