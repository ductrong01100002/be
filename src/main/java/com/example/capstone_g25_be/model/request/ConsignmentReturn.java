package com.example.capstone_g25_be.model.request;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class ConsignmentReturn {
        private Long id;
        private Long productId;
        private int quantity;
        private double unitPrice;
}
