package com.example.capstone_g25_be.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;
@Getter
@Setter
@AllArgsConstructor
public class ExportGoodsRequest {
    private String billReferenceNumber;
    private LocalDateTime createdDate;
    private String description;
    private Long userId;
    List<ConsignmentExport> consignmentExports;
}
