package com.example.capstone_g25_be.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InventoryCheckingHistoryDetailRequest {

	private Long consignmentId;

	private Integer instockQuantity;

	private Integer realityQuantity;

	private double differentAmout;

}
