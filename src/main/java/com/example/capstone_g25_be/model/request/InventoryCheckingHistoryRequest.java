package com.example.capstone_g25_be.model.request;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InventoryCheckingHistoryRequest {
	
	private Long userId;
	
	private Long wareHouseId;
	
	private double totalDifferentAmout;
	
	private List<InventoryCheckingHistoryDetailRequest> listCheckingHistoryDetailRequest;

}
