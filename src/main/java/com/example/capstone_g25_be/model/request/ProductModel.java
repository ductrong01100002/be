package com.example.capstone_g25_be.model.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductModel {
    private Long productId;
    private String productName;
    private String productCode;
    private String unitMeasure;
}
