package com.example.capstone_g25_be.model.request;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Value;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductRequest {

	private Long id;
	
	@Length(max = 5, message = "Lỗi name")
	private String name;

	private String categoryName;
	
	@Length(max = 5, message = "Lỗi productCode")
	private String productCode;

	private String unitMeasure;
	
	@Range(min = 0, max = 5, message = "lỗi unitPrice")
	private double unitPrice;

	private String wrapUnitMeasure;

	private Integer numberOfWrapUnitMeasure;

	private String color;

	private String description;
	
	private String imageName;

	private String manufactorName;

	private Long categoryId;

	private Long manufactorId;
	
	private Long subCategoryId;
}
