package com.example.capstone_g25_be.model.request;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;
@Getter
@Setter
public class ReturnOrderRequest {
    private String billReferenceNumber;
    private String description;
    private Long userId;
    List<ConsignmentReturn> consignmentReturns;
}
