package com.example.capstone_g25_be.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReturnToManufacturerDetailRequest {
	
	private Long id;
	
	private String productName;

	private Integer quantity;
	
	private String unitMeasure;

	private double unitPrice;

}
