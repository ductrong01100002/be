package com.example.capstone_g25_be.model.request;

import java.time.LocalDateTime;
import java.util.List;

import com.example.capstone_g25_be.entities.Status;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReturnToManufacturerRequest {

	private LocalDateTime createDate;

	private LocalDateTime confirmedDate;

	private LocalDateTime returnDate;

	private LocalDateTime expectedReturnDate;
	
	private String description;

	private Long userConfirmedId;

	private double totalAmout;

	private Long userCreateId;

	private Long warehouseId;

	private Long manufacturerId;

	private Status statusId;
	
	private List<ReturnToManufacturerDetailRequest> listReturnToManufacturerDetailRequest;

}
