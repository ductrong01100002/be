package com.example.capstone_g25_be.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SignupRequest {
  @NotBlank
  @Size(min = 3, max = 20)
  private String username;

  private String fullName;

  private String identityCard;

  private LocalDate dateOfBirth;

  private Boolean gender;

  private String phone;

  private String email;

  private Set<String> role;

  private Long provinceId;

  private Long districtId;

  private Long wardId;

  private String addressDetail;

}
