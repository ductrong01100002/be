package com.example.capstone_g25_be.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class UpdateExportOrder {
        private Long orderId;
        private String billReferenceNumber;
        private String description;
        private Long userId;
        List<ConsignmentExport> consignmentExports;
}
