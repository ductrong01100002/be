package com.example.capstone_g25_be.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;
@Getter
@Setter
@AllArgsConstructor
public class UpdateImportGoodsRequest {
        private Long orderId;
        private String billReferenceNumber;
        private LocalDateTime createdDate;
        private LocalDateTime confirmDate;
        private String description;
        private Long userId;
        private Long manufactorId;
        private Long wareHouseId;
        List<ConsignmentRequest> consignmentRequests;
}
