package com.example.capstone_g25_be.model.request;

import lombok.Getter;
import lombok.Setter;
import java.time.LocalDate;

@Getter
@Setter
public class UpdateStaffRequest {

    private Long id;

    private String fullName;

    private String identityCard;

    private LocalDate dateOfBirth;

    private Boolean gender;

    private String phone;

    private String email;

    private Long addressId;

    private Long provinceId;

    private Long districtId;

    private Long wardId;

    private String detailAddress;
}
