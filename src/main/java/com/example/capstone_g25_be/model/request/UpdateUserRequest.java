package com.example.capstone_g25_be.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UpdateUserRequest {

        private Long id;
        @NotBlank
        @Size(min = 3, max = 20)
        private String fullName;

        private Long identityCard;

        private LocalDateTime dateOfBirth;

        private Integer gender;

        private String phone;

        private String email;

        private Long addressId;
}
