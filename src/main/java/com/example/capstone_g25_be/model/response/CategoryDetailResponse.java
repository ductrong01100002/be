package com.example.capstone_g25_be.model.response;

import com.example.capstone_g25_be.entities.Category;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryDetailResponse {

	private Long id;

	private String name;

	private String description;
	
	public static CategoryDetailResponse createSuccessData(Category category) {
		CategoryDetailResponse categoryDetailResponse = new CategoryDetailResponse();
		categoryDetailResponse.setId(category.getId());
		categoryDetailResponse.setName(category.getName());
		categoryDetailResponse.setDescription(category.getDescription());
		return categoryDetailResponse;
	}

}
