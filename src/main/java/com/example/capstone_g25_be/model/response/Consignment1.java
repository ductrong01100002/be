package com.example.capstone_g25_be.model.response;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

public interface Consignment1 {

	Long getId();

	LocalDateTime getExpirationDate();

	LocalDateTime getImportDate();

	int getQuantity();

}
