package com.example.capstone_g25_be.model.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigInteger;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ConsignmentList {

        private BigInteger id;

        private BigInteger warehouseId;

        private String warehouseName;

        private String importDate;

        private String expirationDate;

        private Integer quantity;

        private Integer quantityInstock;

        private Integer quantityReturn;
}
