package com.example.capstone_g25_be.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreatedByDto {
    private BigInteger id;
    private String name;
    public static List<CreatedByDto> createSuccessData(List<Map<String,Object>> mapList) {
        List<CreatedByDto> dtoList = new ArrayList<>();
        for (Map<String,Object> p : mapList) {
            CreatedByDto createdByDto = new CreatedByDto();
            createdByDto.setId((BigInteger)p.get(("id")));
            createdByDto.setName(p.get("name").toString()+" "+"("+p.get("userName").toString()+")");
            dtoList.add(createdByDto);
        }
        return dtoList;
    }
}
