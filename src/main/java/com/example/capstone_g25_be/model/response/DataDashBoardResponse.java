package com.example.capstone_g25_be.model.response;

import java.time.LocalDate;
import java.util.List;

import com.example.capstone_g25_be.entities.InventoryAmout;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataDashBoardResponse {

	private Integer numberUser;

	private Integer numberProduct;

	private Integer numberImportOrderNotConfirmed;

	private Integer numberExportOrderNotConfirmed;

	private Integer numberImportOrderConfirmed;
	
	private double latestInventoryAmount;
	
	private LocalDate lastDay;

	private List<ProductDashBoard> chart;

	public static DataDashBoardResponse createSuccessData(List<InventoryAmout> listInventoryAmout, Integer numberUser,
			Integer numberProduct, Integer numberImportOrderNotConfirmed, Integer numberExportOrderNotConfirmed,
			Integer numberImportOrderConfirmed, InventoryAmout inventoryAmout, LocalDate lastDay, List<String> returnDate) {
		DataDashBoardResponse response = new DataDashBoardResponse();
		response.setNumberUser(numberUser);
		response.setNumberProduct(numberProduct);
		response.setNumberImportOrderNotConfirmed(numberImportOrderNotConfirmed);
		response.setNumberExportOrderNotConfirmed(numberExportOrderNotConfirmed);
		response.setNumberImportOrderConfirmed(numberImportOrderConfirmed);
		response.setLatestInventoryAmount(inventoryAmout.getAmout() / 1000000);
		response.setLastDay(lastDay);
		response.setChart(ProductDashBoard.createSuccessData(listInventoryAmout, returnDate));
		return response;
	}

}
