package com.example.capstone_g25_be.model.response;

import com.example.capstone_g25_be.entities.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DetailExportReponse {

    private BigInteger productId;

    private String productCode;

    private String productName;

    private String unitMeasure;

    private double unitPrice;

    private int quantity;

    private double totalPrice;

    private Integer numberOfWrapUnitMeasure;

    private String wrapUnitMeasure;

    List<ConsignmentList> consignmentList;

    public static List<DetailExportReponse> createSuccessData(List<Map<String, Object>> productList,
                                                              List<Map<String, Object>> consignmentList) {
        List<DetailExportReponse> reponse = new ArrayList<>();
        for (Map<String, Object> p : productList) {
            DetailExportReponse detailExportReponse = new DetailExportReponse();
            detailExportReponse.setProductId((BigInteger) p.get("productId"));
            detailExportReponse.setProductCode(p.get("productCode").toString());
            detailExportReponse.setProductName(p.get("productName").toString());
            detailExportReponse.setUnitMeasure(p.get("unitMeasure").toString());
            detailExportReponse.setUnitPrice((Double) p.get("unitPrice"));
            detailExportReponse.setQuantity(((BigDecimal) p.get("quantity")).intValue());
            detailExportReponse.setTotalPrice((Double) p.get("totalPrice"));
            detailExportReponse.setNumberOfWrapUnitMeasure(p.get("numberOfWrapUnitMeasure") == null ? null : (Integer) p.get("numberOfWrapUnitMeasure"));
            detailExportReponse.setWrapUnitMeasure(p.get("wrapUnitMeasure") == null ? null : p.get("wrapUnitMeasure").toString());
            List<ConsignmentList> consignmentLists = new ArrayList<>();
            for (Map<String, Object> c : consignmentList) {
                if (p.get("productId").equals(c.get("productId"))) {
                    ConsignmentList consignment = new ConsignmentList();
                    consignment.setId((BigInteger) c.get("consignmentId"));
                    consignment.setWarehouseId((BigInteger) c.get("warehouseId"));
                    String warehouseName;
                    if (c.get("warehouseName") == null) {
                        warehouseName = "";
                    } else {
                        warehouseName = c.get("warehouseName").toString();
                    }
                    consignment.setWarehouseName(warehouseName);
                    consignment.setImportDate(c.get("importDate")==null?null:c.get("importDate").toString());
                    String exDate;
                    if (c.get("expirationDate") == null) {
                        exDate = "";
                    } else {
                        exDate = c.get("expirationDate").toString();
                    }
                    consignment.setExpirationDate(exDate);
                    consignment.setQuantity((Integer) c.get("quantity"));
                    consignment.setQuantityInstock((Integer) c.get("quantityInstock"));
                    consignment.setQuantityReturn(0);
                    consignmentLists.add(consignment);
                    detailExportReponse.setConsignmentList(consignmentLists);
                }
            }
            reponse.add(detailExportReponse);
        }
        return reponse;
    }
}
