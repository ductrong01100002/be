package com.example.capstone_g25_be.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DetailReturnResponse {

        private BigInteger productId;

        private String productCode;

        private String productName;

        private String unitMeasure;

        private double unitPrice;

        private int quantity;

        private double totalPrice;

        private Integer numberOfWrapUnitMeasure;

        private String wrapUnitMeasure;

        List<ConsignmentReturnList> consignmentReturnLists;
        public static List<DetailReturnResponse> createSuccessData(List<Map<String,Object>> productList,
                                                                                                             List<Map<String,Object>> consignmentList) {
            List<DetailReturnResponse> reponse = new ArrayList<>();
            for (Map<String,Object> p:productList) {
                DetailReturnResponse detailReturnResponse = new DetailReturnResponse();
                detailReturnResponse.setProductId((BigInteger) p.get("productId"));
                detailReturnResponse.setProductCode(p.get("productCode").toString());
                detailReturnResponse.setProductName(p.get("productName").toString());
                detailReturnResponse.setUnitMeasure(p.get("unitMeasure").toString());
                detailReturnResponse.setUnitPrice((Double) p.get("unitPrice"));
                detailReturnResponse.setQuantity(((BigDecimal) p.get("quantity")).intValue());
                detailReturnResponse.setTotalPrice((Double) p.get("totalPrice"));
                detailReturnResponse.setNumberOfWrapUnitMeasure(p.get("numberOfWrapUnitMeasure")==null?null:(Integer) p.get("numberOfWrapUnitMeasure"));
                detailReturnResponse.setWrapUnitMeasure(p.get("wrapUnitMeasure")==null?null:p.get("wrapUnitMeasure").toString());
                List<ConsignmentReturnList> consignmentLists = new ArrayList<>();
                for (Map<String,Object> c:consignmentList) {
                    if (p.get("productId").equals(c.get("productId"))) {
                        ConsignmentReturnList consignment = new ConsignmentReturnList();
                        consignment.setId((BigInteger) c.get("consignmentId"));
                        consignment.setWarehouseId((BigInteger) c.get("warehouseId"));
                        String warehouseName ;
                        if(c.get("warehouseName")==null){
                            warehouseName = "";
                        }else{
                            warehouseName = c.get("warehouseName").toString();
                        }
                        consignment.setWarehouseName(warehouseName);
                        consignment.setImportDate(c.get("importDate").toString());
                        String exDate ;
                        if (c.get("expirationDate")==null){
                            exDate = "";
                        }else{
                            exDate = c.get("expirationDate").toString();
                        }
                        consignment.setExpirationDate(exDate);
                        consignment.setQuantityReturn((Integer) c.get("quantity"));
                        consignmentLists.add(consignment);
                        detailReturnResponse.setConsignmentReturnLists(consignmentLists);
                    }
                }
                reponse.add(detailReturnResponse);
            }
            return reponse;
        }
}
