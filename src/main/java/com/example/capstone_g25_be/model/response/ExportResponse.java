package com.example.capstone_g25_be.model.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigInteger;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ExportResponse {

    private BigInteger productId;

    private String productCode;

    private String productName;

    private String unitMeasure;

    private double unitPrice;

    private int quantity;

    private Integer numberOfWrapUnitMeasure;

    private String wrapUnitMeasure;

    List<ProductConsignment> consignmentList;
}
