package com.example.capstone_g25_be.model.response;

import java.time.LocalDateTime;
import java.util.List;

import com.example.capstone_g25_be.entities.Consignment;
import com.example.capstone_g25_be.entities.InventoryCheckingHistory;
import com.example.capstone_g25_be.entities.InventoryCheckingHistoryDetail;
import com.example.capstone_g25_be.entities.Product;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InventoryCheckingHistoryDetailResponse {
	
	private Long id;

	private String userName;
	
	private String fullName;

	private String wareHouseName;

	private LocalDateTime createDate;
	
	private double totalDifferentAmout;

	private List<ListProductForCheckingHistoryResponse> listProduct;
	
	public static InventoryCheckingHistoryDetailResponse createSuccessData(
			InventoryCheckingHistory inventoryCheckingHistory, 
			List<InventoryCheckingHistoryDetail> listDetail, 
			List<Product> listProduct, List<Consignment> listConsignment) {
		InventoryCheckingHistoryDetailResponse response = new InventoryCheckingHistoryDetailResponse();
		response.setId(inventoryCheckingHistory.getId());
		response.setUserName(inventoryCheckingHistory.getUser().getUserName());
		response.setFullName(inventoryCheckingHistory.getUser().getFullName());
		response.setWareHouseName(inventoryCheckingHistory.getWarehouse().getName());
		response.setCreateDate(inventoryCheckingHistory.getCreateDate());
		response.setTotalDifferentAmout(inventoryCheckingHistory.getTotalDifferentAmout());
		response.setListProduct(ListProductForCheckingHistoryResponse.
				createSuccessData(listProduct, listConsignment, listDetail));
		return response;
	}

}
