package com.example.capstone_g25_be.model.response;

import lombok.Data;

import java.util.List;

@Data
public class JwtResponse {
  private String token;
  private String type = "Bearer";
  private Long id;
  private String username;
  private String fullName;
  private String imageUrl;
  private List<String> roles;

//  public JwtResponse(String accessToken, Long id, String username,List<String> roles) {
//    this.token = accessToken;
//    this.id = id;
//    this.username = username;
//    this.roles = roles;
//  }


  public JwtResponse(String token, Long id, String username, String fullName, String imageUrl, List<String> roles) {
    this.token = token;
    this.id = id;
    this.username = username;
    this.fullName = fullName;
    this.imageUrl = imageUrl;
    this.roles = roles;
  }

  public String getAccessToken() {
    return token;
  }

  public void setAccessToken(String accessToken) {
    this.token = accessToken;
  }

  public String getTokenType() {
    return type;
  }

  public void setTokenType(String tokenType) {
    this.type = tokenType;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public List<String> getRoles() {
    return roles;
  }
}
