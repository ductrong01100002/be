package com.example.capstone_g25_be.model.response;

import java.util.ArrayList;
import java.util.List;

import com.example.capstone_g25_be.entities.Category;
import com.example.capstone_g25_be.entities.Product;
import com.example.capstone_g25_be.entities.SubCategory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ListCategoryResponse {

	private Long id;

	private String name;

	private String description;
	
	private List<ListSubCategoryResponse> subCategory;

	public static List<ListCategoryResponse> createSuccessData(List<Category> listCategory,
			List<SubCategory> listSubCategory) {
		List<ListCategoryResponse> listResponse = new ArrayList<>();
		for (Category category : listCategory) {
			List<SubCategory> list = new ArrayList<>();
			for (SubCategory subCategory : listSubCategory) {
				if(subCategory.getCategory().getId() == category.getId()) {
					list.add(subCategory);
				}
			}
			ListCategoryResponse listCategoryResponse = new ListCategoryResponse();
			listCategoryResponse.setId(category.getId());
			listCategoryResponse.setName(category.getName());
			listCategoryResponse.setDescription(category.getDescription());
			listCategoryResponse.setSubCategory(ListSubCategoryResponse.createSuccessData(list));
			listResponse.add(listCategoryResponse);
		}
		return listResponse;
	}

}
