package com.example.capstone_g25_be.model.response;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.example.capstone_g25_be.entities.Consignment;
import com.example.capstone_g25_be.entities.InventoryCheckingHistoryDetail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListConsignmentForCheckingDetailResponse {

	private Long id;

	private LocalDateTime expirationDate;

	private LocalDateTime importDate;

	private Double unitPrice;

	private Integer instockQuantity;

	private Integer realityQuantity;

	private double differentAmout;

	public static List<ListConsignmentForCheckingDetailResponse> createSuccessData(Long productId,
			List<Consignment> listConsignment, List<InventoryCheckingHistoryDetail> listCheckingDetail) {
		List<ListConsignmentForCheckingDetailResponse> listProductForCheckingHistoryResponse = new ArrayList<>();
		for (Consignment consignment : listConsignment) {
			for (InventoryCheckingHistoryDetail inventoryCheckingHistoryDetail : listCheckingDetail) {
				if (consignment.getProduct().getId() == productId &&
						inventoryCheckingHistoryDetail.getConsignment().getId() == consignment.getId()) {
					ListConsignmentForCheckingDetailResponse response = new ListConsignmentForCheckingDetailResponse();
					response.setId(consignment.getId());
					response.setExpirationDate(consignment.getExpirationDate());
					response.setImportDate(consignment.getImportDate());
					response.setUnitPrice(consignment.getUnitPrice());
					response.setInstockQuantity(inventoryCheckingHistoryDetail.getInstockQuantity());
					response.setRealityQuantity(inventoryCheckingHistoryDetail.getRealityQuantity());
					response.setDifferentAmout(inventoryCheckingHistoryDetail.getDifferentAmout());
					listProductForCheckingHistoryResponse.add(response);
				}
			}
		}
		return listProductForCheckingHistoryResponse;
	}

}
