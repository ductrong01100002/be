package com.example.capstone_g25_be.model.response;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.example.capstone_g25_be.entities.Consignment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListConsignmentResponse {

	private Long id;

	private LocalDateTime expirationDate;

	private LocalDateTime importDate;

	private Double unitPrice;

	private Integer quantity;
	
	private String realityQuantity;
	
	private Long wareHouseId;

	private String wareHouseName;

	private String addressWareHouse;

	public static List<ListConsignmentResponse> createSuccessData(List<Consignment> listConsignment) {
		List<ListConsignmentResponse> listResponse = new ArrayList<>();
		for (Consignment consignment : listConsignment) {
			ListConsignmentResponse listSubProductResponse = new ListConsignmentResponse();
			listSubProductResponse.setId(consignment.getId());
			listSubProductResponse.setExpirationDate(consignment.getExpirationDate());
			listSubProductResponse.setImportDate(consignment.getImportDate());
			listSubProductResponse.setUnitPrice(consignment.getUnitPrice());
			listSubProductResponse.setQuantity(consignment.getQuantity());
			listSubProductResponse.setRealityQuantity("");
			listSubProductResponse.setWareHouseId(consignment.getWarehouse().getId());
			listSubProductResponse.setWareHouseName(consignment.getWarehouse().getName());
			listSubProductResponse.setAddressWareHouse(consignment.getWarehouse().getAddress().getDetail() + " - "
					+ consignment.getWarehouse().getAddress().getWard().getName() + " - "
					+ consignment.getWarehouse().getAddress().getDistrict().getName() + " - "
					+ consignment.getWarehouse().getAddress().getProvince().getName());
			listResponse.add(listSubProductResponse);
		}
		return listResponse;
	}

}
