package com.example.capstone_g25_be.model.response;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.example.capstone_g25_be.entities.Consignment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListConsignmentResponse1 {
	
	private Long id;

	private LocalDateTime expirationDate;

	private LocalDateTime importDate;
	
	private int quantity;


	public static List<ListConsignmentResponse1> createSuccessData(List<Consignment1> listConsignment) {
		List<ListConsignmentResponse1> listResponse = new ArrayList<>();
		for (Consignment1 consignment : listConsignment) {
			ListConsignmentResponse1 listSubProductResponse = new ListConsignmentResponse1();
			listSubProductResponse.setId(consignment.getId());
			listSubProductResponse.setExpirationDate(consignment.getExpirationDate());
			listSubProductResponse.setImportDate(consignment.getImportDate());
			listSubProductResponse.setQuantity(consignment.getQuantity());
			listResponse.add(listSubProductResponse);
		}
		return listResponse;
	}

}
