package com.example.capstone_g25_be.model.response;

import java.util.ArrayList;
import java.util.List;

import com.example.capstone_g25_be.entities.District;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ListDistrictResponse {

	private Long id;

	private String name;

	private Long provinceId;

	public static List<ListDistrictResponse> createSuccessData(List<District> listDistrict) {
		List<ListDistrictResponse> listResponse = new ArrayList<>();
		for (District district : listDistrict) {
			ListDistrictResponse listDistrictResponse = new ListDistrictResponse();
			listDistrictResponse.setId(district.getId());
			listDistrictResponse.setName(district.getName());
			listDistrictResponse.setProvinceId(district.getProvince().getId());
			listResponse.add(listDistrictResponse);
		}
		return listResponse;
	}

}
