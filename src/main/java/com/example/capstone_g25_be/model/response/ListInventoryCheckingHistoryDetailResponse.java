//package com.example.capstone_g25_be.model.response;
//
//import java.time.LocalDateTime;
//import java.util.ArrayList;
//import java.util.List;
//
//import com.example.capstone_g25_be.entities.InventoryCheckingHistory;
//import com.example.capstone_g25_be.entities.InventoryCheckingHistoryDetail;
//
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//@Data
//@NoArgsConstructor
//@AllArgsConstructor
//public class ListInventoryCheckingHistoryDetailResponse {
//
//	private String productName;
//
//	private String productCode;
//
//	private String unitMeasure;
//
//	private String wrapUnitMeasure;
//
//	private Integer numberOfWrapUnitMeasure;
//
//	private double unitPrice;
//
//	private LocalDateTime expirationDate;
//
//	private LocalDateTime importDate;
//
//	public static List<ListInventoryCheckingHistoryDetailResponse> createSuccessData(
//			List<InventoryCheckingHistoryDetail> listInventoryCheckingHistory) {
//		List<ListInventoryCheckingHistoryDetailResponse> response = new ArrayList<>();
//		for (InventoryCheckingHistoryDetail inventoryCheckingHistoryDetail : listInventoryCheckingHistory) {
//			ListInventoryCheckingHistoryDetailResponse data = new ListInventoryCheckingHistoryDetailResponse();
//			data.setProductName(inventoryCheckingHistoryDetail.getConsignment().getProduct().getName());
//			data.setProductCode(inventoryCheckingHistoryDetail.getConsignment().getProduct().getProductCode());
//			data.setUnitMeasure(inventoryCheckingHistoryDetail.getConsignment().getProduct().getUnitMeasure());
//			data.setWrapUnitMeasure(inventoryCheckingHistoryDetail.getConsignment().getProduct().getWrapUnitMeasure());
//			data.setNumberOfWrapUnitMeasure(
//					inventoryCheckingHistoryDetail.getConsignment().getProduct().getNumberOfWrapUnitMeasure());
//			data.setUnitPrice(inventoryCheckingHistoryDetail.getConsignment().getProduct().getUnitPrice());
//			data.setExpirationDate(inventoryCheckingHistoryDetail.getConsignment().getExpirationDate());
//			data.setImportDate(inventoryCheckingHistoryDetail.getConsignment().getImportDate());
//			response.add(data);
//		}
//		
//		return response;
//	}
//}
