package com.example.capstone_g25_be.model.response;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.example.capstone_g25_be.entities.InventoryCheckingHistory;
import com.example.capstone_g25_be.entities.InventoryCheckingHistoryDetail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ListInventoryCheckingHistoryResponse {

	private Long id;

	private LocalDateTime createDate;

	private Long userId;

	private Long wareHouseId;

	private String userName;

	private String fullName;

	private String wareHouseName;

	private double differentAmout;

	public static List<ListInventoryCheckingHistoryResponse> createSuccessData(
			List<InventoryCheckingHistory> listInventoryCheckingHistory) {
		List<ListInventoryCheckingHistoryResponse> response = new ArrayList<>();
		for (InventoryCheckingHistory inventoryCheckingHistory : listInventoryCheckingHistory) {
			ListInventoryCheckingHistoryResponse listInventoryCheckingHistoryResponse = new ListInventoryCheckingHistoryResponse();
			listInventoryCheckingHistoryResponse.setId(inventoryCheckingHistory.getId());
			listInventoryCheckingHistoryResponse.setCreateDate(inventoryCheckingHistory.getCreateDate());
			listInventoryCheckingHistoryResponse.setUserId(inventoryCheckingHistory.getUser().getId());
			listInventoryCheckingHistoryResponse.setWareHouseId(inventoryCheckingHistory.getWarehouse().getId());
			listInventoryCheckingHistoryResponse.setUserName(inventoryCheckingHistory.getUser().getUserName());
			listInventoryCheckingHistoryResponse.setFullName(inventoryCheckingHistory.getUser().getFullName());
			listInventoryCheckingHistoryResponse.setWareHouseName(inventoryCheckingHistory.getWarehouse().getName());
			listInventoryCheckingHistoryResponse.setDifferentAmout(inventoryCheckingHistory.getTotalDifferentAmout());
			response.add(listInventoryCheckingHistoryResponse);
		}

		return response;
	}

}
