package com.example.capstone_g25_be.model.response;

import java.util.ArrayList;
import java.util.List;

import com.example.capstone_g25_be.entities.Manufacturer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ListManufacturerResponse {
	
	private Long id;
	
	private String name;

	private String email;

	private String phone;
	
	private String manufactureAddress;

	public static List<ListManufacturerResponse> createSuccessData(List<Manufacturer> listManufactor) {
		List<ListManufacturerResponse> listResponse = new ArrayList<>();
		for (Manufacturer manufactor : listManufactor) {
			ListManufacturerResponse listCategoryResponse = new ListManufacturerResponse();
			listCategoryResponse.setId(manufactor.getId());
			listCategoryResponse.setName(manufactor.getName());
			listCategoryResponse.setEmail(manufactor.getEmail());
			listCategoryResponse.setPhone(manufactor.getPhone());
			listCategoryResponse.setManufactureAddress(manufactor.getAddress().getDetail() + " - "
					+ manufactor.getAddress().getWard().getName() + " - " 
					+ manufactor.getAddress().getDistrict().getName() + " - " 
					+ manufactor.getAddress().getProvince().getName());
			listResponse.add(listCategoryResponse);
		}
		return listResponse;
	}
}
