package com.example.capstone_g25_be.model.response;

import java.util.ArrayList;
import java.util.List;

import com.example.capstone_g25_be.entities.Product;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListProductByWareHouse {
	
	private Long productId;

    private String productName;

    public static List<ListProductByWareHouse> createSuccessData(List<Product> listProduct) {
		List<ListProductByWareHouse> listResponse = new ArrayList<>();
		for (Product product : listProduct) {
			ListProductByWareHouse listProductByWareHouse = new ListProductByWareHouse();
			listProductByWareHouse.setProductId(product.getId());
			listProductByWareHouse.setProductName(product.getName());;
			listResponse.add(listProductByWareHouse);
		}
		return listResponse;
	}

}
