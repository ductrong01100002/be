package com.example.capstone_g25_be.model.response;

import java.util.ArrayList;
import java.util.List;

import com.example.capstone_g25_be.entities.Consignment;
import com.example.capstone_g25_be.entities.InventoryCheckingHistoryDetail;
import com.example.capstone_g25_be.entities.Product;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListProductForCheckingHistoryResponse {
	
	private Long id;

	private String name;

	private String productCode;

	private String unitMeasure;

	private String wrapUnitMeasure;

	private Integer numberOfWrapUnitMeasure;

	private double unitPrice;
	
	private List<ListConsignmentForCheckingDetailResponse> listConsignment;
	
	public static List<ListProductForCheckingHistoryResponse> createSuccessData(List<Product> listProduct,
			List<Consignment> listConsignment, List<InventoryCheckingHistoryDetail> listCheckingDetail) {
		List<ListProductForCheckingHistoryResponse> listProductForCheckingHistoryResponse = new ArrayList<>();
		for (Product product : listProduct) {
			ListProductForCheckingHistoryResponse response = new ListProductForCheckingHistoryResponse();
			response.setId(product.getId());
			response.setName(product.getName());
			response.setProductCode(product.getProductCode());
			response.setUnitMeasure(product.getUnitMeasure());
			response.setWrapUnitMeasure(product.getWrapUnitMeasure());
			response.setNumberOfWrapUnitMeasure(product.getNumberOfWrapUnitMeasure());
			response.setUnitPrice(product.getUnitPrice());
			response.setListConsignment(ListConsignmentForCheckingDetailResponse.
					createSuccessData(product.getId(), listConsignment, listCheckingDetail));
			listProductForCheckingHistoryResponse.add(response);
		}
		return listProductForCheckingHistoryResponse;
	}

}
