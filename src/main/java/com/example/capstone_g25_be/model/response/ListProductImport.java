package com.example.capstone_g25_be.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListProductImport {

    private Long id;

    private Integer newQuantity;

    private Integer oldQuantity;
}
