package com.example.capstone_g25_be.model.response;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.capstone_g25_be.entities.Product;
import com.example.capstone_g25_be.profile.UserProfileService;
import com.example.capstone_g25_be.repository.ConsignmentRepository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListProductResponse {

	private Long id;

	private String name;

	private String productCode;

	private String unitMeasure;

	private String wrapUnitMeasure;

	private Integer numberOfWrapUnitMeasure;

	private String color;

	private String description;

	private String categoryName;

	private String manufactorName;

	private Integer quantity;

	private Long categoryId;

	private Long manufactorId;

	private String image;

	public static List<ListProductResponse> createSuccessData(List<Product> listProduct,
			List<ListProductVo> listProductVo) {
		List<ListProductResponse> listResponse = new ArrayList<>();
		for (Product product : listProduct) {
			for (ListProductVo productVo : listProductVo) {
				if (product.getId().equals(productVo.getId())) {
					ListProductResponse listProductResponse = new ListProductResponse();
					listProductResponse.setId(product.getId());
					listProductResponse.setName(product.getName());
					listProductResponse.setProductCode(product.getProductCode());
					listProductResponse.setUnitMeasure(product.getUnitMeasure());
					listProductResponse.setWrapUnitMeasure(product.getWrapUnitMeasure());
					listProductResponse.setNumberOfWrapUnitMeasure(product.getNumberOfWrapUnitMeasure());
					listProductResponse.setColor(product.getColor());
					listProductResponse.setCategoryName(product.getCategory().getName());
					listProductResponse.setDescription(product.getDescription());
					listProductResponse.setManufactorName(product.getManufacturer().getName());
					listProductResponse.setQuantity(productVo.getQuantity());
					listProductResponse.setCategoryId(product.getCategory().getId());
					listProductResponse.setManufactorId(product.getManufacturer().getId());
					if (product.getImage() != null) {
						listProductResponse.setImage("api/product/download/image/" + product.getId());
					}
					listResponse.add(listProductResponse);
				}
			}
		}
		return listResponse;
	}

	public static List<ListProductResponse> createSuccessData2(List<Product> listProduct) {
		List<ListProductResponse> listResponse = new ArrayList<>();
		for (Product product : listProduct) {
			ListProductResponse listProductResponse = new ListProductResponse();
			listProductResponse.setId(product.getId());
			listProductResponse.setName(product.getName());
			listProductResponse.setProductCode(product.getProductCode());
			listProductResponse.setUnitMeasure(product.getUnitMeasure());
			listProductResponse.setWrapUnitMeasure(product.getWrapUnitMeasure());
			listProductResponse.setNumberOfWrapUnitMeasure(product.getNumberOfWrapUnitMeasure());
			listProductResponse.setColor(product.getColor());
			listProductResponse.setCategoryName(product.getCategory().getName());
			listProductResponse.setDescription(product.getDescription());
			listProductResponse.setManufactorName(product.getManufacturer().getName());
			listProductResponse.setCategoryId(product.getCategory().getId());
			listProductResponse.setManufactorId(product.getManufacturer().getId());
			if (product.getImage() != null) {
				listProductResponse.setImage("api/product/download/image/" + product.getId());
			}
			listResponse.add(listProductResponse);
		}
		return listResponse;
	}
}
