package com.example.capstone_g25_be.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListProductVo {
	
	private Long id;
	
	private Integer quantity;

}
