package com.example.capstone_g25_be.model.response;

import java.util.ArrayList;
import java.util.List;

import com.example.capstone_g25_be.entities.Category;
import com.example.capstone_g25_be.entities.Province;
import com.example.capstone_g25_be.entities.SubCategory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ListProvinceResponse {
	
	private Long id;

	private String name;

	private Long districtId;
	
	private String districtName;

	public static List<ListProvinceResponse> createSuccessData(List<Province> listProvince) {
		List<ListProvinceResponse> listResponse = new ArrayList<>();
		for (Province province : listProvince) {
			ListProvinceResponse listCategoryResponse = new ListProvinceResponse();
			listCategoryResponse.setId(province.getId());
			listCategoryResponse.setName(province.getName());
			listResponse.add(listCategoryResponse);
		}
		return listResponse;
	}

}
