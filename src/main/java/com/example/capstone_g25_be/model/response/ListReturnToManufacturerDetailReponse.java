package com.example.capstone_g25_be.model.response;

import java.util.ArrayList;
import java.util.List;

import com.example.capstone_g25_be.entities.ReturnToManufacturer;
import com.example.capstone_g25_be.entities.ReturnToManufacturerDetail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ListReturnToManufacturerDetailReponse {
	
	private Long id;

	private String productName;

	private Integer quantity;
	
	private String unitMeasure;

	private double unitPrice;
	
	public static List<ListReturnToManufacturerDetailReponse> createSuccessData(
			List<ReturnToManufacturerDetail> listReturnToManufacturerDetail) {
		List<ListReturnToManufacturerDetailReponse> response = new ArrayList<>();
		for (ReturnToManufacturerDetail r : listReturnToManufacturerDetail) {
			ListReturnToManufacturerDetailReponse returnToManufacturer = new ListReturnToManufacturerDetailReponse();
			returnToManufacturer.setId(r.getId());
			returnToManufacturer.setProductName(r.getProductName());
			returnToManufacturer.setQuantity(r.getQuantity());
			returnToManufacturer.setUnitMeasure(r.getUnitMeasure());
			returnToManufacturer.setUnitPrice(r.getUnitPrice());
			response.add(returnToManufacturer);
		}
		return response;
	}

}
