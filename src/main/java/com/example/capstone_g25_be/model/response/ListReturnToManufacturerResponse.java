package com.example.capstone_g25_be.model.response;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.example.capstone_g25_be.entities.ReturnToManufacturer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ListReturnToManufacturerResponse {

	private Long id;

	private LocalDateTime createDate;

	private LocalDateTime expectedReturnDate;

	private Long userCreateId;
	
	private Long manufacturerId;
	
	private Long StatusId;

	private String userCreateName;

	private String userCreateFullName;

	private String manufacturerName;
	
	private String statusName;

	private double totalAmout;
	
	private String description;

	public static List<ListReturnToManufacturerResponse> createSuccessData(
			List<ReturnToManufacturer> listReturnToManufacturer) {
		List<ListReturnToManufacturerResponse> response = new ArrayList<>();
		for (ReturnToManufacturer r : listReturnToManufacturer) {
			ListReturnToManufacturerResponse returnToManufacturer = new ListReturnToManufacturerResponse();
			returnToManufacturer.setId(r.getId());
			returnToManufacturer.setCreateDate(r.getCreateDate());
			returnToManufacturer.setExpectedReturnDate(r.getExpectedReturnDate());
			returnToManufacturer.setUserCreateId(r.getUser().getId());
			returnToManufacturer.setManufacturerId(r.getManufacturer().getId());
			returnToManufacturer.setStatusId(r.getStatus().getId());
			returnToManufacturer.setUserCreateName(r.getUser().getUserName());
			returnToManufacturer.setUserCreateFullName(r.getUser().getFullName());
			returnToManufacturer.setManufacturerName(r.getManufacturer().getName());
			returnToManufacturer.setStatusName(r.getStatus().getName());
			returnToManufacturer.setTotalAmout(r.getTotalAmout());
			returnToManufacturer.setDescription(r.getDescription());
			response.add(returnToManufacturer);
		}

		return response;
	}

}
