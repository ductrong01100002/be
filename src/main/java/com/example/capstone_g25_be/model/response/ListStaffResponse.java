package com.example.capstone_g25_be.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListStaffResponse {
    private BigInteger totalRecord;
    private BigInteger id;
    private String imageUrl;
    private String userName;
    private String fullName;
    private String phone;
    private BigInteger roleId;
    private String roleName;
    private boolean isActive;
    public static List<ListStaffResponse> createSuccessData(List<Map<String,Object>> map){
        List<ListStaffResponse> listStaffResponses = new ArrayList<>();
        for (Map<String,Object> detail : map) {
            ListStaffResponse response = new ListStaffResponse();
            response.setTotalRecord((BigInteger)detail.get("totalRecord"));
            response.setId((BigInteger)detail.get("id"));
            response.setImageUrl(detail.get("imageUrl")==null?"":"api/staff/download/image/" + detail.get("id"));
            response.setUserName(detail.get("userName").toString());
            response.setFullName(detail.get("fullName").toString());
            response.setPhone(detail.get("phone").toString());
            response.setRoleId((BigInteger)detail.get("roleId"));
            response.setRoleName(detail.get("roleName").toString());
            response.setActive((Boolean) detail.get("isActive"));
            listStaffResponses.add(response);
        }
        return listStaffResponses;
    }
}
