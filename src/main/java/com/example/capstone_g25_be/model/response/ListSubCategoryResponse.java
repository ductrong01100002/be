package com.example.capstone_g25_be.model.response;

import java.util.ArrayList;
import java.util.List;

import com.example.capstone_g25_be.entities.SubCategory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListSubCategoryResponse {

	private Long id;

	private String name;

	private String description;
	
	private String categoryName;
	
	private Long categoryId;
	
	public static List<ListSubCategoryResponse> createSuccessData(List<SubCategory> listSubCategory) {
		List<ListSubCategoryResponse> listResponse = new ArrayList<>();
		for (SubCategory subCategory : listSubCategory) {
			ListSubCategoryResponse listSubCategoryResponse = new ListSubCategoryResponse();
			listSubCategoryResponse.setId(subCategory.getId());
			listSubCategoryResponse.setName(subCategory.getName());
			listSubCategoryResponse.setDescription(subCategory.getDescription());
			listSubCategoryResponse.setCategoryName(subCategory.getCategory().getName());
			listSubCategoryResponse.setCategoryId(subCategory.getCategory().getId());
			listResponse.add(listSubCategoryResponse);
		}
		return listResponse;
	}

}
