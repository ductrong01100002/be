package com.example.capstone_g25_be.model.response;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.example.capstone_g25_be.entities.User;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListUserResponse {
	
	private Long id;

    private String userName;

    private String passWord;
    
    private Boolean gender;

    private LocalDate dateOfBirth;

    private String email;

    private String identityCard;
    
    private String phone;

    private String resetPasswordByToken;

    public static List<ListUserResponse> createSuccessData(List<User> listUser) {
		List<ListUserResponse> listResponse = new ArrayList<>();
		for (User user : listUser) {
			ListUserResponse listUserResponse = new ListUserResponse();
			listUserResponse.setId(user.getId());
			listUserResponse.setUserName(user.getUserName());
			listUserResponse.setPassWord(user.getPassWord());
			listUserResponse.setGender(user.getGender());
			listUserResponse.setDateOfBirth(user.getDateOfBirth());
			listUserResponse.setEmail(user.getEmail());
			listUserResponse.setIdentityCard(user.getIdentityCard());
			listUserResponse.setPhone(user.getPhone());
			listUserResponse.setResetPasswordByToken(user.getOtp());
			listResponse.add(listUserResponse);
		}
		return listResponse;
	}
}
