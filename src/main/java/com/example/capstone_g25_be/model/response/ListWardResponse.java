package com.example.capstone_g25_be.model.response;

import java.util.ArrayList;
import java.util.List;

import com.example.capstone_g25_be.entities.District;
import com.example.capstone_g25_be.entities.Ward;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ListWardResponse {

	private Long id;

	private String name;

	private Long districtId;

	public static List<ListWardResponse> createSuccessData(List<Ward> listWard) {
		List<ListWardResponse> listResponse = new ArrayList<>();
		for (Ward ward : listWard) {
			ListWardResponse listWardResponse = new ListWardResponse();
			listWardResponse.setId(ward.getId());
			listWardResponse.setName(ward.getName());
			listWardResponse.setDistrictId(ward.getDistrict().getId());
			listResponse.add(listWardResponse);
		}
		return listResponse;
	}

}
