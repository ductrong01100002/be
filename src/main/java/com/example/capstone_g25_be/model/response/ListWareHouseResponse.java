package com.example.capstone_g25_be.model.response;

import java.util.ArrayList;
import java.util.List;

import com.example.capstone_g25_be.entities.Category;
import com.example.capstone_g25_be.entities.SubCategory;
import com.example.capstone_g25_be.entities.Warehouse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ListWareHouseResponse {

	private Long id;

	private String name;
	
	private String AddressWareHouse;
	
	public static List<ListWareHouseResponse> createSuccessData(List<Warehouse> listWareHouse) {
		List<ListWareHouseResponse> listResponse = new ArrayList<>();
		for (Warehouse warehouse : listWareHouse) {
			ListWareHouseResponse listWareHouseResponse = new ListWareHouseResponse();
			listWareHouseResponse.setId(warehouse.getId());
			listWareHouseResponse.setName(warehouse.getName());
			listWareHouseResponse.setAddressWareHouse(warehouse.getAddress().getDetail() + " - "
					+ warehouse.getAddress().getWard().getName() + " - " 
					+ warehouse.getAddress().getDistrict().getName() + " - " 
					+ warehouse.getAddress().getProvince().getName());
			listResponse.add(listWareHouseResponse);
		}
		return listResponse;
	}

}
