package com.example.capstone_g25_be.model.response;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.capstone_g25_be.entities.Manufacturer;
import com.example.capstone_g25_be.entities.Product;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ManufacturerDetailReponse {

	private Long id;

	private String name;

	private String email;

	private String phone;

	private String addressManufactor;
	
	private String addressDetail;
	
	private String provinceName;
	
	private String districtName;
	
	private String wardName;
	
	private Long provinceId;
	
	private Long districtId;
	
	private Long wardId;
	
	private List<ListProductResponse> listProducts;
	

	public static ManufacturerDetailReponse createSuccessData(Manufacturer manufactor, List<Product> listProduct,
			List<ListProductVo> listProductVo) {
		ManufacturerDetailReponse manufactorDetailReponse = new ManufacturerDetailReponse();
		manufactorDetailReponse.setId(manufactor.getId());
		manufactorDetailReponse.setName(manufactor.getName());
		manufactorDetailReponse.setEmail(manufactor.getEmail());
		manufactorDetailReponse.setPhone(manufactor.getPhone());
		manufactorDetailReponse.setAddressManufactor(manufactor.getAddress().getDetail() + " - "
				+ manufactor.getAddress().getWard().getName() + " - " 
				+ manufactor.getAddress().getDistrict().getName() + " - " 
				+ manufactor.getAddress().getProvince().getName());
		manufactorDetailReponse.setAddressDetail(manufactor.getAddress().getDetail());
		manufactorDetailReponse.setProvinceId(manufactor.getAddress().getProvince().getId());
		manufactorDetailReponse.setDistrictId(manufactor.getAddress().getDistrict().getId());
		manufactorDetailReponse.setWardId(manufactor.getAddress().getWard().getId());
		manufactorDetailReponse.setProvinceName(manufactor.getAddress().getProvince().getName());
		manufactorDetailReponse.setDistrictName(manufactor.getAddress().getDistrict().getName());
		manufactorDetailReponse.setWardName(manufactor.getAddress().getWard().getName());
		manufactorDetailReponse.setListProducts(ListProductResponse.createSuccessData(listProduct, listProductVo));
		return manufactorDetailReponse;
	}

}
