package com.example.capstone_g25_be.model.response;

public interface Product1 {
	
	Long getId();

	String getName();

	String getProductCode();

	String getUnitMeasure();

	String getWrapUnitMeasure();

	Integer getNumberOfWrapUnitMeasure();
	
	double getUnitPrice();

}
