package com.example.capstone_g25_be.model.response;

import java.util.List;

import com.example.capstone_g25_be.entities.Consignment;
import com.example.capstone_g25_be.entities.Product;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductAndListConsignmentResponse {

	private Long id;

	private String name;

	private String productCode;

	private String unitMeasure;

	private String wrapUnitMeasure;

	private Integer numberOfWrapUnitMeasure;

	private double unitPrice;

	private List<ListConsignmentResponse1> listConsignment;

	public static ProductAndListConsignmentResponse createSuccessData(Product1 product, List<Consignment1> list) {
		ProductAndListConsignmentResponse productDetailResponse = new ProductAndListConsignmentResponse();
		productDetailResponse.setId(product.getId());
		productDetailResponse.setName(product.getName());
		productDetailResponse.setProductCode(product.getProductCode());
		productDetailResponse.setUnitMeasure(product.getUnitMeasure());
		productDetailResponse.setWrapUnitMeasure(product.getWrapUnitMeasure());
		productDetailResponse.setNumberOfWrapUnitMeasure(product.getNumberOfWrapUnitMeasure());;
		productDetailResponse.setUnitPrice(product.getUnitPrice());
		productDetailResponse.setListConsignment(ListConsignmentResponse1.createSuccessData(list));
		return productDetailResponse;
	}

}
