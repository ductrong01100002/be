package com.example.capstone_g25_be.model.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigInteger;
import java.time.LocalDate;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProductConsignment {

    private BigInteger id;

    private BigInteger warehouseId;

    private String warehouseName;

    private String importDate;

    private String expirationDate;

    private Integer quantityInstock;

    private Integer quantity;

}
