package com.example.capstone_g25_be.model.response;

import java.util.ArrayList;
import java.util.List;

import com.example.capstone_g25_be.entities.District;
import com.example.capstone_g25_be.entities.InventoryAmout;
import com.example.capstone_g25_be.entities.Product;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDashBoard {
	
	private String saveDate;
	
	private double amout;
	
	public static List<ProductDashBoard> createSuccessData(List<InventoryAmout> listInventoryAmout,
			List<String> returnDate) {
		List<ProductDashBoard> listResponse = new ArrayList<>();
		int element = 0;
		for (InventoryAmout i : listInventoryAmout) {
			ProductDashBoard productDashBoard = new ProductDashBoard();
			productDashBoard.setSaveDate(returnDate.get(element));
			if(i !=null) {
				productDashBoard.setAmout(i.getAmout() / 1000000);
			}
			element++;
			listResponse.add(productDashBoard);
		}
		return listResponse;
	}

}
