package com.example.capstone_g25_be.model.response;

import com.example.capstone_g25_be.entities.Product;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDetailResponse {

	private Long id;

	private String name;

	private String productCode;

	private String unitMeasure;

	private String wrapUnitMeasure;

	private Integer numberOfWrapUnitMeasure;

	private String color;

	private String description;

	private String categoryName;

	private String subCategoryName;

	private String manufactorName;

	private Integer quantity;
	
	private double unitPrice;
	
	private Long categoryId;
	
	private Long subCategoryId;
	
	private Long manufactorId;
	
	private String image;
	
	private String imageName;

	public static ProductDetailResponse createSuccessData(Product product, ListProductVo productVo) {
		ProductDetailResponse productDetailResponse = new ProductDetailResponse();
		productDetailResponse.setId(product.getId());
		productDetailResponse.setName(product.getName());
		productDetailResponse.setProductCode(product.getProductCode());
		productDetailResponse.setUnitMeasure(product.getUnitMeasure());
		productDetailResponse.setWrapUnitMeasure(product.getWrapUnitMeasure());
		productDetailResponse.setNumberOfWrapUnitMeasure(product.getNumberOfWrapUnitMeasure());
		productDetailResponse.setColor(product.getColor());
		productDetailResponse.setCategoryName(product.getCategory().getName());
		productDetailResponse.setDescription(product.getDescription());
		productDetailResponse.setManufactorName(product.getManufacturer().getName());
		productDetailResponse.setQuantity(productVo.getQuantity());
		productDetailResponse.setUnitPrice(product.getUnitPrice());
		productDetailResponse.setCategoryId(product.getCategory().getId());
		if(product.getSubCategory() != null) {
			productDetailResponse.setSubCategoryId(product.getSubCategory().getId());
			productDetailResponse.setSubCategoryName(product.getSubCategory().getName());
		}
		productDetailResponse.setManufactorId(product.getManufacturer().getId());
		if(product.getImage() != null) {
			productDetailResponse.setImage("api/product/download/image/" +  product.getId());
			productDetailResponse.setImageName(product.getImage());
		}
		return productDetailResponse;
	}

}
