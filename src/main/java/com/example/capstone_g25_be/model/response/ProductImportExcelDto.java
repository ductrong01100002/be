package com.example.capstone_g25_be.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductImportExcelDto {
    private String productName;
    private String productCode;
    private String unitMeasure;
    private String color;
    private String description;
    private String wrapUnitMeasure;
    private String numberOfWrapUnitMeasure;
    private String category;
    private String subCategory;
    private String manufacture;
}
