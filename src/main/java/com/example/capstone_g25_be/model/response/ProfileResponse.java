package com.example.capstone_g25_be.model.response;

import com.example.capstone_g25_be.entities.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;
import java.sql.Date;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProfileResponse {
    private String imageUrl;
    private String fullName;
    private String userName;
    private String email;
    private String phone;
    private String identityCard;
    private BigInteger addressId;
    private BigInteger provinceId;
    private String provinceName;
    private BigInteger districtId;
    private String districtName;
    private BigInteger wardId;
    private String wardName;
    private String detailAddress;
    private boolean isActive;
    private boolean gender;
    private Date dateOfBirth;
    public static ProfileResponse createSuccessData(User user, Map<String, Object> objectMap) {
        ProfileResponse userResponse = new ProfileResponse();
        if(user.getImage() != null) {
            userResponse.setImageUrl("api/staff/download/image/" + user.getId());
        }else{
            userResponse.setImageUrl("");
        }
        userResponse.setFullName(objectMap.get("fullName").toString());
        userResponse.setUserName(objectMap.get("userName").toString());
        userResponse.setEmail(objectMap.get("email").toString());
        userResponse.setPhone(objectMap.get("phone").toString());
        userResponse.setIdentityCard(objectMap.get("identityCard").toString());
        userResponse.setAddressId((BigInteger)objectMap.get("addressId"));
        userResponse.setProvinceId((BigInteger)objectMap.get("provinceId"));
        userResponse.setProvinceName(objectMap.get("provinceName").toString());
        userResponse.setDistrictId((BigInteger)objectMap.get("districtId"));
        userResponse.setDistrictName(objectMap.get("districtName").toString());
        userResponse.setWardId((BigInteger)objectMap.get("wardId"));
        userResponse.setWardName(objectMap.get("wardName").toString());
        userResponse.setDetailAddress(objectMap.get("detailAddress").toString());
        userResponse.setActive((Boolean) objectMap.get("isActive"));
        userResponse.setGender((Boolean) objectMap.get("gender"));
        userResponse.setDateOfBirth((java.sql.Date) objectMap.get("dateOfBirth"));
        return userResponse;
    }
}
