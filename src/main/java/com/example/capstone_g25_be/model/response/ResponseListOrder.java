package com.example.capstone_g25_be.model.response;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class ResponseListOrder {
    private Long orderId;
    private LocalDateTime time;
    private String manufactorName;
    private String statusName;

    public ResponseListOrder(Long orderId, LocalDateTime time, String manufactorName, String statusName) {
        this.orderId = orderId;
        this.time = time;
        this.manufactorName = manufactorName;
        this.statusName = statusName;
    }
}
