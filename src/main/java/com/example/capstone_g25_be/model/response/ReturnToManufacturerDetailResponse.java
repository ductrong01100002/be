package com.example.capstone_g25_be.model.response;

import java.time.LocalDateTime;
import java.util.List;

import com.example.capstone_g25_be.entities.ReturnToManufacturer;
import com.example.capstone_g25_be.entities.ReturnToManufacturerDetail;
import com.example.capstone_g25_be.entities.Warehouse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReturnToManufacturerDetailResponse {

	private Long id;
	
	private LocalDateTime createDate;

	private LocalDateTime expectedReturnDate;

	private LocalDateTime confirmedDate;

	private LocalDateTime returnDate;

	private Long userConfirmedId;

	private Long userCreateId;

	private Long manufacturerId;

	private Long wareHouseId;

	private Long StatusId;

	private String userCreateName;

	private String userConfirmedName;

	private String manufacturerName;

	private String statusName;

	private String wareHouseName;

	private String wareHouseAddress;

	private double totalAmout;
	
	private String description;
	
	private String fullNameCreate;
	
	private String fullNameConfirmed;

	private List<ListReturnToManufacturerDetailReponse> listReturnToManufacturerDetail;

	public static ReturnToManufacturerDetailResponse createSuccessData(ReturnToManufacturer returnToManufacturer,
			List<ReturnToManufacturerDetail> listDetail, Warehouse wareHouse, String userConfirmedName, String fullNameConfirmed) {
		ReturnToManufacturerDetailResponse response = new ReturnToManufacturerDetailResponse();
		response.setId(returnToManufacturer.getId());
		response.setCreateDate(returnToManufacturer.getCreateDate());
		response.setExpectedReturnDate(returnToManufacturer.getExpectedReturnDate());
		response.setConfirmedDate(returnToManufacturer.getConfirmedDate());
		response.setReturnDate(returnToManufacturer.getReturnDate());
		response.setUserCreateId(returnToManufacturer.getUser().getId());
		response.setUserConfirmedId(returnToManufacturer.getUserConfirmedId());
		response.setManufacturerId(returnToManufacturer.getManufacturer().getId());
		response.setWareHouseId(returnToManufacturer.getWarehouse().getId());
		response.setStatusId(returnToManufacturer.getStatus().getId());
		response.setUserCreateName(returnToManufacturer.getUser().getUserName());
		response.setUserConfirmedName(userConfirmedName);
		response.setManufacturerName(returnToManufacturer.getManufacturer().getName());
		response.setStatusName(returnToManufacturer.getStatus().getName());
		response.setWareHouseName(returnToManufacturer.getWarehouse().getName());
		response.setWareHouseAddress(wareHouse.getAddress().getDetail() + " - "
				+ wareHouse.getAddress().getWard().getName() + " - " 
				+ wareHouse.getAddress().getDistrict().getName() + " - " 
				+ wareHouse.getAddress().getProvince().getName());
		response.setTotalAmout(returnToManufacturer.getTotalAmout());
		response.setDescription(returnToManufacturer.getDescription());
		response.setFullNameCreate(returnToManufacturer.getUser().getFullName());
		response.setFullNameConfirmed(fullNameConfirmed);
		response.setListReturnToManufacturerDetail(ListReturnToManufacturerDetailReponse.createSuccessData(listDetail));
		return response;
	}

}
