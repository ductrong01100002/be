package com.example.capstone_g25_be.model.response;

import java.util.ArrayList;
import java.util.List;

import com.example.capstone_g25_be.entities.SubCategory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SubCategoryResponse {

	private Long id;

	private String name;

	private String description;
	
	private Long categoryId;
	
	public static List<SubCategoryResponse> createSuccessData(List<SubCategory> listSubCategory) {
		List<SubCategoryResponse> listSubCategoryResponses = new ArrayList<>();
		for (SubCategory subCategory : listSubCategory) {
			SubCategoryResponse subCategoryResponse = new SubCategoryResponse();
			subCategoryResponse.setId(subCategory.getId());
			subCategoryResponse.setName(subCategory.getName());
			subCategoryResponse.setDescription(subCategory.getDescription());
			subCategoryResponse.setCategoryId(subCategory.getCategory().getId());
			listSubCategoryResponses.add(subCategoryResponse);
		}
		return listSubCategoryResponses;
	}

}
