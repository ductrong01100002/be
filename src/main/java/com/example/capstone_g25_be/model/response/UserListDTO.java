package com.example.capstone_g25_be.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserListDTO {

    private BigInteger id;

    private String image;

    private String fullName;

    private String phone;

    private String role;

    private int isActive;

    public static List<UserListDTO> createSuccessData(List<Map<String,Object>> maps) {
        List<UserListDTO> response = new ArrayList<>();
        for (Map<String,Object> p:maps) {
            UserListDTO dto = new UserListDTO();
            dto.setId((BigInteger) p.get("id"));
            String image ;
            if(p.get("image")==null){
                image = "";
            }else{
                image = p.get("image").toString();
            }
            dto.setImage(image);
            dto.setFullName(p.get("fullName").toString());
            dto.setPhone(p.get("phone").toString());
            dto.setRole(p.get("role").toString());
            dto.setIsActive(((int) p.get("isActive")));
            response.add(dto);
        }
        return response;
    }
}
