package com.example.capstone_g25_be.model.response;

import com.example.capstone_g25_be.entities.Manufacturer;
import com.example.capstone_g25_be.entities.Warehouse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WareHouseDetailResponse {
	
	private Long id;

	private String name;

	private String addressDetail;
	
	private Long provinceId;
	
	private Long districtId;
	
	private Long wardId;
	
	public static WareHouseDetailResponse createSuccessData(Warehouse wareHouse) {
		WareHouseDetailResponse wareHouseDetailReponse = new WareHouseDetailResponse();
		wareHouseDetailReponse.setId(wareHouse.getId());
		wareHouseDetailReponse.setName(wareHouse.getName());
		wareHouseDetailReponse.setAddressDetail(wareHouse.getAddress().getDetail());
		wareHouseDetailReponse.setProvinceId(wareHouse.getAddress().getProvince().getId());
		wareHouseDetailReponse.setDistrictId(wareHouse.getAddress().getDistrict().getId());
		wareHouseDetailReponse.setWardId(wareHouse.getAddress().getWard().getId());
		return wareHouseDetailReponse;
	}

}
