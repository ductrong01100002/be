package com.example.capstone_g25_be.profile;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.apache.http.entity.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.capstone_g25_be.bucket.BucketName;
import com.example.capstone_g25_be.entities.Product;
import com.example.capstone_g25_be.fileStore.FileStore;
import com.example.capstone_g25_be.repository.ProductRepository;

@Service
public class UserProfileService {

	private final UserProfileDataAccessService userProfileDataAccessService;
	
	private final FileStore fileStore;
	
	private final ProductRepository productRepository;

	@Autowired
	public UserProfileService(UserProfileDataAccessService userProfileDataAccessService,
			FileStore fileStore, ProductRepository productRepository) {
		this.userProfileDataAccessService = userProfileDataAccessService;
		this.fileStore = fileStore;
		this.productRepository = productRepository;
	}

	List<UserProfile> getUserProfiles() {
		return userProfileDataAccessService.getUserProfiles();
	}

	public void upLoadUserProfileImage(UUID userProfileId, MultipartFile file) {
		if (file.isEmpty()) {
			throw new IllegalStateException("Cannot upload empty file [ " + file.getSize() + " ]");
		}
		if (!Arrays.asList(
				ContentType.IMAGE_JPEG.getMimeType(),
				ContentType.IMAGE_PNG.getMimeType(),
				ContentType.IMAGE_GIF.getMimeType())
				.contains(file.getContentType())) {
			throw new IllegalStateException("File must be an image");
		}
		UserProfile user= getUserProfileOrThrow(userProfileId);
		
		Map<String, String> metadata = new HashMap<>();
		metadata.put("Content-Type", file.getContentType());
		metadata.put("Content-Length", String.valueOf( file.getSize()));
		
		String path = String.format("%s/%s", BucketName.PROFILE_IMAGE.getBucketName(), user.getUserProfileId());
		String fileName = String.format("%s", file.getOriginalFilename());
		
		try {
			fileStore.save(path, fileName, Optional.of(metadata), file.getInputStream());
			user.setUserProfileImageLink(fileName);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}

	}

	public byte[] downloadUserProfileImage(UUID userProfileId) {
		UserProfile user = getUserProfileOrThrow(userProfileId);
		String path = String.format("%s/%s",
				BucketName.PROFILE_IMAGE.getBucketName(),
				user.getUserProfileId(),
				user.getUserProfileImageLink());
		return user.getUserProfileImageLink()
			.map(key -> fileStore.download(path, key))
			.orElse(new byte[0]);
	}
	
	private UserProfile getUserProfileOrThrow(UUID userProfileId) {
		return userProfileDataAccessService.getUserProfiles()
				.stream()
				.filter(userProfile -> userProfile
							.getUserProfileId()
							.equals(userProfileId))
							.findFirst()
							.orElseThrow(
							() -> new IllegalStateException(String.format("User profile %s not found", userProfileId)));
	}
	
	
	public void upLoadUserProfileImage2(Long userProfileId, MultipartFile file) {
		if (file.isEmpty()) {
			throw new IllegalStateException("Cannot upload empty file [ " + file.getSize() + " ]");
		}
		if (!Arrays.asList(
				ContentType.IMAGE_JPEG.getMimeType(),
				ContentType.IMAGE_PNG.getMimeType(),
				ContentType.IMAGE_GIF.getMimeType())
				.contains(file.getContentType())) {
			throw new IllegalStateException("File must be an image");
		}
		Product product = productRepository.findProductById(userProfileId);
		
		Map<String, String> metadata = new HashMap<>();
		metadata.put("Content-Type", file.getContentType());
		metadata.put("Content-Length", String.valueOf( file.getSize()));
		
		String path = String.format("%s/%s/%s", BucketName.PROFILE_IMAGE.getBucketName(),"Product", product.getId());
		String fileName = String.format("%s", file.getOriginalFilename());
		
		try {
			fileStore.save(path, fileName, Optional.of(metadata), file.getInputStream());
			product.setImage(fileName);
			productRepository.save(product);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}

	}

	public byte[] downloadUserProfileImage2(Product product) {
		String path = String.format("%s/%s/%s",
				BucketName.PROFILE_IMAGE.getBucketName(),
				"Product",
				product.getId());
		return fileStore.download(path, product.getImage());
	}
	
	private UserProfile getUserProfileOrThrow2(Long userProfileId) {
		return userProfileDataAccessService.getUserProfiles()
				.stream()
				.filter(userProfile -> userProfile
							.getUserProfileId()
							.equals(userProfileId))
							.findFirst()
							.orElseThrow(
							() -> new IllegalStateException(String.format("User profile %s not found", userProfileId)));
	}

}
