package com.example.capstone_g25_be.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.example.capstone_g25_be.entities.Address;
@Repository
@EnableJpaRepositories
public interface AddressRepository extends JpaRepository<Address, Long>{
	
	@Query("SELECT a FROM Address a Where a.id = ?1 AND a.deletedAt = false" )
	Address findAddressById(Long addressId);

}
