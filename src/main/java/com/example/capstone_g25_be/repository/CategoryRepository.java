package com.example.capstone_g25_be.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.example.capstone_g25_be.entities.Category;

@Repository
@EnableJpaRepositories
public interface CategoryRepository extends JpaRepository<Category, Long>{
	
	@Query(value = "SELECT * FROM category WHERE MATCH(name) "
			+ "AGAINST (?1) AND deleted_at = false", nativeQuery = true )
	Page<Category> findBySearch(String nameSearch, Pageable pageable);
	
	
	@Query("SELECT c FROM Category c Where c.id = ?1 AND c.deletedAt = false" )
	Category findCategoryById(Long categoryId);
	
	Page<Category> findAllByDeletedAt(boolean deletedAt, Pageable pageable);
	
	@Query("SELECT c FROM Category c Where c.name = ?1 AND c.deletedAt = false" )
	Category findCategoryByName(String categoryName);
	
	@Query("SELECT c FROM Category c  Where id <> ?1 and name = ?2 AND deleted_at = false")
	Category findCategoryByIdAndName(Long id, String categoryName);
	
	@Query("SELECT c FROM Category c  Where deleted_at = false")
	List<Category> findAllCategory();
}
