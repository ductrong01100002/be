package com.example.capstone_g25_be.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.example.capstone_g25_be.entities.Consignment;
import com.example.capstone_g25_be.model.response.Consignment1;

@Repository
@EnableJpaRepositories
public interface ConsignmentRepository extends JpaRepository<Consignment, Long> {

	@Query("SELECT sum(quantity) " + "FROM Consignment c " + "where product_id = ?1"
			+ " AND importDate is not null AND deletedAt = false ")
	Integer countQuantity(Long productId);

	@Query("SELECT c FROM Consignment c "
			+ "Where c.product.id = ?1 AND importDate is not null AND c.quantity > 0  AND c.deletedAt = false" )
	Page<Consignment> findAllConsignment(Long productId,Pageable pageable);

	@Query("SELECT c FROM Consignment c Where c.id = ?1 " )
	Consignment findConsignmentById(Long orderId);

	@Query("SELECT c from Consignment c where id = ?1 ")
	Consignment countQuantityWithConsignmentNotImport(Long consignnentId);
	
	@Query("SELECT c.product.id FROM Consignment c " + "where warehouse_id = ?1"
			+ " AND importDate is not null AND deletedAt = false AND quantity > 0")
	List<Long> findAllConsignmentByWareHouseId(Long wareHouseId);
	
	@Query("SELECT c FROM Consignment c "
			+ "Where c.product.id = ?1 AND importDate is not null  AND c.deletedAt = false AND quantity > 0" )
	List<Consignment> findProductAllConsignment(Long productId);

	@Query("SELECT c FROM Consignment c "
			+ "Where c.id IN (?1)  AND c.deletedAt = false" )
	List<Consignment> findAllConsignmentByListId(List<Long> listConsignmentId);
	
	@Query("SELECT c.id FROM Consignment c "
			+ "Where c.product.id = ?1 AND importDate is not null AND quantity <> 0  AND c.deletedAt = false" )
	List<Long> findAllConsignmentByProductId(Long productId);
	
	@Query(nativeQuery = true , value = "SELECT c.id as id, c.expiration_date as expirationDate,"
			+ " c.import_date as importDate, c.quantity as quantity FROM consignment c "
			+ "Where c.product_id = ?1 AND c.warehouse_id = ?2 AND import_date is not null  AND c.deleted_at = false AND quantity > 0" )
	List<Consignment1> findProductAllConsignment1(Long productId, Long warehouseId);
	
}
