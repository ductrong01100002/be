package com.example.capstone_g25_be.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.example.capstone_g25_be.entities.Category;
import com.example.capstone_g25_be.entities.District;

@Repository
@EnableJpaRepositories
public interface DistrictRepository extends JpaRepository<District, Long>{
	
	@Query("SELECT d FROM District d Where d.province.id = ?1 AND d.deletedAt = false" )
	List<District> findListDistrictByProvinceId(Long categoryId);
	
	@Query("SELECT d FROM District d Where d.id = ?1 AND d.deletedAt = false" )
	District findDistrictById(Long categoryId);

}
