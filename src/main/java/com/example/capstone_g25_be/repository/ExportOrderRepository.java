package com.example.capstone_g25_be.repository;

import com.example.capstone_g25_be.entities.Order;
import com.example.capstone_g25_be.model.response.ExportResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public interface ExportOrderRepository extends JpaRepository<Order,Long> {

    /**
     * Get list export order
     */
    @Query(nativeQuery = true,value = "SELECT count(*) over() AS totalRecord,od.id AS orderId,c.id, \n" +
            "od.bill_reference_number AS billRefernce ,od.created_date AS createDate,\n" +
            "mn.name AS manufactorName,st.name AS statusName  ,sum(odt.quantity*odt.unit_price) AS totalPrice,od.confirm_date AS confirmDate" +
            ",u.username as userName,u.full_name as fullName\n" +
            "FROM `order` od \n" +
            "LEFT JOIN order_detail odt ON od. id = odt.order_id\n" +
            "LEFT JOIN consignment c ON c.id = odt.consignment_id\n" +
            "LEFT JOIN status st ON od.status_id = st.id\n" +
            "LEFT JOIN manufacturer mn ON od.manufacturer_id = mn.id\n" +
            "LEFT JOIN order_type ot ON ot.id = od.order_type_id\n" +
            "LEFT JOIN user u ON u.id = od.user_id\n" +
            "Where ot.id=2 \n" +
            " AND CASE WHEN  ?3 is not null AND ?4 is not null  THEN DATE(od.created_date) between ?3 AND ?4  \n" +
            " WHEN  ?4 is null AND ?3 is not null THEN DATE(od.created_date)  >= ?3 \n" +
            " WHEN  ?3 is null AND ?4 is not null THEN DATE(od.created_date)  <= ?4 \n" +
            " ELSE upper(od.bill_reference_number) Like UPPER('%' ?2 '%') END  \n" +
            "AND (?1 is null OR od.user_id = ?1) \n" +
            "AND (?2 is null OR upper(od.bill_reference_number) Like UPPER('%' ?2 '%')) \n" +
            "AND (?5 is null OR st.id = 1) \n" +
            " group by  od.id" +
            " ORDER BY st.id ASC,od.created_date DESC")
    List<Map<String,Object>> getListExportBySearchData(Long userId, String billReferenceNumber, Date startDate, Date endDate,Integer status, Pageable pageable);

    /**
     * Get list product Dropdown list
     * @return
     */
    @Query(nativeQuery = true,
            value = "SELECT distinct p.id AS productId, p.product_code AS productCode ," +
                    " p.name productName,p.unit_measure AS unitMeasure , p.unit_price AS unitPrice,p.number_of_wrap_unit_measure AS numberOfWrapUnitMeasure,p.wrap_unit_measure AS wrapUnitMeasure" +
                    " FROM consignment c\n" +
                    "JOIN product p ON c.product_id = p.id\n" +
                    "WHERE c.import_date is not null AND c.quantity > 0 ")
    List<Map<String,Object>>  getListProductByImportDate();

    /**
     * Get product Selected in Dropdown list
     */
    @Query(nativeQuery = true, value = "SELECT count(*) over() AS totalRecord,p.id AS productId, c.id AS id, wh.id AS wareHouseId,\n" +
                    " wh.name AS warehouseName, p.product_code AS productCode,p.name AS productName,p.unit_measure AS unitMeasure,\n" +
                    "p.unit_price AS unitPrice,c.import_date AS importDate,c.expiration_date AS expirationDate,\n" +
                    "p.number_of_wrap_unit_measure AS numberOfWrapUnitMeasure,p.wrap_unit_measure AS wrapUnitMeasure ,\n " +
                    "c.quantity AS quantityInstock\n" +
                    "from product p \n" +
                    "JOIN consignment c ON p.id = c.product_id AND c.deleted_at = 0 \n" +
                    "JOIN warehouse wh ON wh.id = c.warehouse_id\n" +
                    "WHERE  c.import_date is not null AND c.quantity > 0 AND p.id= ?1 \n" +
                    "order by c.expiration_date desc")
    List<Map<String,Object>>  getProductInstock(Long productId,Pageable pageable);

    /**
     * API 1 Get infor export order
     */
    @Query(nativeQuery = true,value = "SELECT DISTINCT od.bill_reference_number AS billRefernce ," +
            "mn.id AS manufactorId,mn.name AS manufactorName,st.name AS statusName , od.is_return AS isReturn , od.created_date AS createDate," +
            "ads.detail AS addressDetail,p.name AS provinceName,\n" +
            "d.name AS districtName, w.name AS wardName ,od.description AS description," +
            "u.id AS userId ,u.username AS createBy,u.full_name AS createdFullName, od.confirm_date AS confirmDate ,(select t.username  from (select u.username\n" +
            "from user u JOIN `order` od \n" +
            "ON  u.id = od.confirm_by\n" +
            "WHERE od.id = ?1) as t) as confirmBy,\n" +
            "(select t1.full_name  from (select u.full_name\n" +
            "from user u JOIN `order` od \n" +
            "ON  u.id = od.confirm_by\n" +
            "WHERE od.id = ?1) as t1) as confirmByFullName\n" +
            " FROM `order` od \n" +
            "LEFT JOIN manufacturer mn ON od.manufacturer_id = mn.id\n" +
            "LEFT JOIN status st ON od.status_id = st.id\n" +
            "LEFT JOIN address ads ON ads.id = mn.address_id\n" +
            "LEFT JOIN province p ON p.id = ads.province_id\n" +
            "LEFT JOIN district d ON d.id = ads.district_id\n" +
            "LEFT JOIN ward w ON w.id = ads.ward_id\n" +
            "LEFT JOIN order_detail odt ON odt.order_id= od.id\n" +
            "LEFT JOIN consignment sp ON sp.id = odt.consignment_id\n" +
            "LEFT JOIN warehouse i ON i.id = sp.warehouse_id\n" +
            "LEFT JOIN user u ON u.id = od.user_id\n" +
            "WHERE od.id=?1")
    Map<String, Object> getOrderDetail(Long orderId);

    /**
     * API 2 : Get list product in Detail Export order
     */
    @Query(nativeQuery = true,value = "SELECT  c.product_id AS productId, p.product_code AS productCode,p.name AS productName\n" +
            ",p.unit_measure AS unitMeasure,sum(odt.quantity) AS quantity,sum(odt.quantity*odt.unit_price) AS totalPrice\n" +
            ",odt.unit_price AS unitPrice, \n" +
            "p.number_of_wrap_unit_measure AS numberOfWrapUnitMeasure,p.wrap_unit_measure AS wrapUnitMeasure \n " +
            "FROM order_detail odt \n" +
            "LEFT JOIN consignment c ON odt.consignment_id = c.id\n" +
            "LEFT JOIN product p ON p.id = c.product_id\n" +
            "where odt.order_id = ?1\n" +
            "group by c.product_id")
    List<Map<String, Object>> getDetailExportGoods2(Long orderId, Pageable pagable);

    @Query(nativeQuery = true,value = "SELECT odt.quantity AS quantity ,odt.unit_price AS unitPrice , odt.consignment_id AS consignmentId" +
            " FROM order_detail odt WHERE odt.order_id= ?1")
    List<Map<String, Object>> getListConsignmentByOrderId(Long orderId);

    @Query(nativeQuery = true,value = "SELECT c.product_id AS productId, c.id consignmentId, " +
            "c.warehouse_id AS warehouseId,w.name AS warehouseName,c.import_date AS importDate," +
            "c.expiration_date AS expirationDate,odt.quantity AS quantity,c.quantity AS quantityInstock\n" +
            "FROM order_detail odt \n" +
            "JOIN consignment c ON odt.consignment_id = c.id\n" +
            "JOIN warehouse w On w.id = c.warehouse_id\n" +
            "where odt.order_id = ?1")
    List<Map<String, Object>> getListConsignment(Long orderId);

    @Query(nativeQuery = true,value = "select wh.id AS warehouseId, wh.name AS warehouseName,\n" +
            "ad.detail AS detailAddress, p.name AS provinceName,\n" +
            "dt.name AS districtName, w.name AS wardName\n" +
            " from warehouse wh \n" +
            "JOIN address ad ON wh.address_id = ad.id\n" +
            "JOIN province p ON p.id = ad.province_id\n" +
            "JOIN district dt ON dt.id = ad.district_id\n" +
            "JOIN ward w ON w.id = ad.ward_id")
    List<Map<String, Object>> getListAddressWarehouse();

    @Query(nativeQuery = true,value = "SELECT wh.id,wh.name,ad.detail AS detailAddress,d.name AS districtName ,p.name AS provinceName,w.name AS wardName\n" +
            " FROM warehouse as wh\n" +
            "LEFT JOIN address ad ON ad.id = wh.address_id\n" +
            "LEFT JOIN district d ON d.id = ad.district_id\n" +
            "LEFT JOIN province p ON p.id =  ad.province_id\n" +
            "LEFT JOIN ward w ON w.id = ad.ward_id\n" +
            "where wh.id in (:ids)")
    List<Map<String, Object>> getListAddressWarehouseByListId(@Param("ids") List<BigInteger> idsAddress);
}
