package com.example.capstone_g25_be.repository;

import com.example.capstone_g25_be.entities.Order;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public interface ImportOrderRepository extends JpaRepository<Order, Long> {

    @Query(nativeQuery = true, value = "SELECT DISTINCT od.bill_reference_number AS billRefernce " +
            ",mn.id AS manufactorId,mn.name AS manufactorName,st.name AS statusName , " +
            "od.created_date AS createDate,ads.detail AS addressDetail,p.name AS provinceName,\n" +
            "d.name AS districtName, w.name AS wardName ,od.description AS description,i.id AS wareHouseId," +
            "i.name AS wareHouseName ,u.id AS userId ,u.username AS createBy,u.full_name AS createdFullName, od.confirm_date AS confirmDate,(select t.username  from (select u.username\n" +
            "from user u JOIN `order` od \n" +
            "ON  u.id = od.confirm_by\n" +
            "WHERE od.id = ?1) as t) as confirmBy,\n" +
            "(select t1.full_name  from (select u.full_name\n" +
            "from user u JOIN `order` od \n" +
            "ON  u.id = od.confirm_by\n" +
            "WHERE od.id = ?1) as t1) as confirmByFullName\n" +
            " FROM `order` od \n" +
            "LEFT JOIN manufacturer mn ON od.manufacturer_id = mn.id\n" +
            "LEFT JOIN status st ON od.status_id = st.id\n" +
            "LEFT JOIN order_detail odt ON odt.order_id= od.id\n" +
            "LEFT JOIN consignment sp ON sp.id = odt.consignment_id\n" +
            "LEFT JOIN warehouse i ON i.id = sp.warehouse_id\n" +
            "LEFT JOIN address ads ON ads.id = i.address_id\n" +
            "LEFT JOIN province p ON p.id = ads.province_id\n" +
            "LEFT JOIN district d ON d.id = ads.district_id\n" +
            "LEFT JOIN ward w ON w.id = ads.ward_id\n" +
            "LEFT JOIN user u ON u.id = od.user_id\n" +
            "WHERE od.id=?1")
    Map<String, Object> getOrderDetail(Long orderId);

    @Query(nativeQuery = true, value = "SELECT count(*) over() AS totalRecord,sp.id AS consignmentId," +
            "p.id AS productId ,p.product_code AS productCode, p.name AS productName," +
            "p.unit_measure AS unitMeasure,sp.quantity AS quantity,sp.unit_price AS unitPrice,sp.expiration_date AS expirationDate," +
            "p.number_of_wrap_unit_measure AS numberOfWrapUnitMeasure,p.wrap_unit_measure AS wrapUnitMeasure \n " +
            "FROM `order` od \n" +
            " LEFT JOIN order_detail odt ON odt.order_id = od.id\n" +
            " LEFT JOIN consignment sp ON sp.id = odt.consignment_id\n" +
            " LEFT JOIN product p ON p.id = sp.product_id\n" +
            "Where od.id = ?1")
    List<Map<String, Object>> getDetailImportGoods2(Long orderId, Pageable pageable);

    //get list import
    @Query(nativeQuery = true, value = "SELECT count(*) over() AS totalRecord,od.id AS orderId," +
            " od.bill_reference_number AS billRefernce ,Date(c.import_date) AS importDate,od.created_date AS createDate,\n" +
            "mn.name AS manufactorName,st.name AS statusName  ,sum(c.quantity*c.unit_price) AS totalPrice, u.username as userName,u.full_name as fullName \n" +
            "FROM `order` od \n" +
            "JOIN order_detail odt ON od. id = odt.order_id\n" +
            "JOIN consignment c ON c.id = odt.consignment_id\n" +
            "JOIN status st ON od.status_id = st.id\n" +
            "JOIN manufacturer mn ON od.manufacturer_id = mn.id\n" +
            "JOIN order_type ot ON ot.id = od.order_type_id\n" +
            "LEFT JOIN user u ON u.id = od.user_id\n" +
            "Where ot.id=1 \n" +
            " AND CASE WHEN  ?3 is not null AND ?4 is not null  THEN DATE(od.created_date) between ?3 AND ?4  \n" +
            " WHEN  ?4 is null AND ?3 is not null THEN DATE(od.created_date)  >= ?3 \n" +
            " WHEN  ?3 is null AND ?4 is not null THEN DATE(od.created_date)  <= ?4 \n" +
            " ELSE  upper(od.bill_reference_number) Like UPPER('%' ?2 '%') END  \n" +
            "AND (?1 is null OR od.user_id = ?1) \n" +
            "AND (?2 is null OR upper(od.bill_reference_number) Like UPPER('%' ?2 '%')) \n" +
            "AND (?5 is null OR st.id = 1) \n" +
            " group by  od.id" +
            " ORDER BY st.id ASC")
    List<Map<String, Object>> getListImportGoodsBySearchData(Long userId, String billReferenceNumber, Date startDate, Date endDate, Integer status, Pageable pageable);

    @Query(nativeQuery = true, value = "SELECT c.id AS consignmentId, c.product_id AS productId ," +
            "c.unit_price AS unitPrice,c.quantity AS quantity " +
            "FROM `order` od JOIN order_detail odt ON od.id = odt.order_id\n" +
            "JOIN consignment c ON odt.consignment_id = c.id\n" +
            "WHERE order_id = ?1 AND c.import_date is null ")
    List<Map<String, Object>> getListConsignmentByOrderId(Long orderId);

    @Query(nativeQuery = true, value = "SELECT sum(quantity)FROM Consignment c where product_id = ?1\n" +
            "AND import_date is null ")
    Integer getListProductInstock(Long ids);

    @Query(nativeQuery = true, value = "select  u.id AS id ,u.full_name AS name,u.username AS userName\n" +
            "from user u\n" +
            "JOIN user_roles ur ON u.id = ur.user_id\n" +
            "JOIN role r ON r.id = ur.role_id ")
    List<Map<String, Object>> getCreateBy();
}
