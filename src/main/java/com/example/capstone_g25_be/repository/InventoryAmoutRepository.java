package com.example.capstone_g25_be.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.example.capstone_g25_be.entities.InventoryAmout;

@Repository
@EnableJpaRepositories
public interface InventoryAmoutRepository extends JpaRepository<InventoryAmout, Long>{
	
	@Query( value = "SELECT * FROM inventory_amout where date(save_date) = ?1"
			+ " order by id desc LIMIT 1", nativeQuery = true)
	InventoryAmout findAllByDesc(String date);
	
	@Query( value = "SELECT * FROM inventory_amout order by id desc LIMIT 1", nativeQuery = true)
	InventoryAmout findLatest();

}
