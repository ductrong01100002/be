package com.example.capstone_g25_be.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.example.capstone_g25_be.entities.InventoryCheckingHistoryDetail;

@Repository
@EnableJpaRepositories
public interface InventoryCheckingHistoryDetailRepository extends JpaRepository<InventoryCheckingHistoryDetail, Long> {
	@Query("SELECT inventoryDetail FROM InventoryCheckingHistoryDetail inventoryDetail "
			+ "Where inventoryDetail.inventoryCheckingHistory.id = ?1 " )
	List<InventoryCheckingHistoryDetail> findAllByInventoryCheckingHistoryId(Long productId);
}
