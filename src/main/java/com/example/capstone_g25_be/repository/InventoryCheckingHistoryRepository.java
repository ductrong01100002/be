package com.example.capstone_g25_be.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.example.capstone_g25_be.entities.Category;
import com.example.capstone_g25_be.entities.InventoryCheckingHistory;

@Repository
@EnableJpaRepositories
public interface InventoryCheckingHistoryRepository extends JpaRepository<InventoryCheckingHistory, Long>{

	@Query(value = "SELECT inventory FROM InventoryCheckingHistory inventory "
			+ "Where inventory.warehouse.id = CASE WHEN ?1 IS NOT NULL THEN ?1 ELSE inventory.warehouse.id END "
			+ "AND inventory.user.id = CASE WHEN ?2 IS NOT NULL THEN ?2 ELSE inventory.user.id END "
			+ "AND inventory.createDate >= CASE WHEN ?3 IS NOT NULL THEN ?3 ELSE inventory.createDate END "
			+ "AND inventory.createDate <= CASE WHEN ?4 IS NOT NULL THEN ?4 ELSE inventory.createDate END "
			+ "AND inventory.deletedAt = false ")
	Page<InventoryCheckingHistory> findAllInventoryCheckingHistory(Long wareHouseId, Long userId, String startDate,
			String endDate, Pageable pageable);
	
	@Query("SELECT i FROM InventoryCheckingHistory i Where i.id = ?1 AND i.deletedAt = false" )
	InventoryCheckingHistory findInventoryCheckingHistoryById(Long inventoryCheckingHistoryId);

}
