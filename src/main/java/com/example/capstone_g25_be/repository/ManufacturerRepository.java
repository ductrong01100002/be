package com.example.capstone_g25_be.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.capstone_g25_be.entities.Manufacturer;

@Repository
public interface ManufacturerRepository extends JpaRepository<Manufacturer, Long>{
	
	@Query(value = "SELECT * FROM manufacturer WHERE MATCH(name, email) "
			+ "AGAINST (?1) AND deleted_at = false", nativeQuery = true )
	Page<Manufacturer> findBySearch(String dataSearch, Pageable pageable);

	@Query("SELECT m FROM Manufacturer m "
			+ "Where m.id = ?1 AND m.deletedAt = false" )
	Manufacturer findManufactorById(Long manufactorId);
	
	Page<Manufacturer> findAllByDeletedAt(boolean deletedAt, Pageable pageable);
	
	@Query("SELECT m FROM Manufacturer m "
			+ "Where m.deletedAt = false" )
	List<Manufacturer> findAllNotPaging();

}
