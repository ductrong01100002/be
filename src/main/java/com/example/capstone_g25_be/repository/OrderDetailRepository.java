package com.example.capstone_g25_be.repository;

import com.example.capstone_g25_be.entities.Category;
import com.example.capstone_g25_be.entities.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDetailRepository extends JpaRepository<OrderDetail,Long> {
    OrderDetail findByOrder_IdAndConsignment_Id(Long orderId,Long consignmentId);

    @Query("SELECT od FROM OrderDetail od Where od.order.id = ?1 " )
    List<OrderDetail> findOrderDetailByOrderId(Long orderId);
}
