package com.example.capstone_g25_be.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.example.capstone_g25_be.entities.Order;
@Repository
@EnableJpaRepositories
public interface OrderRepository extends JpaRepository<Order, Long>{
	
	@Query("SELECT COUNT(*) "
			+ "  FROM Order "
			+ "  WHERE orderType.id = 1 And status.id = 1")
	Integer countImportOrderNotConfirmed();
	
	@Query("SELECT COUNT(*) "
			+ "  FROM Order "
			+ "  WHERE orderType.id = 2 And status.id = 1")
	Integer countExportOrderNotConfirmed();

	@Query("SELECT COUNT(*) "
			+ "  FROM Order "
			+ "  WHERE status.id = 2")
	Integer countOrderCompleted();
}
