package com.example.capstone_g25_be.repository;


import com.example.capstone_g25_be.entities.OrderType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface OrderTypeRepository extends JpaRepository<OrderType,Long> {
}
