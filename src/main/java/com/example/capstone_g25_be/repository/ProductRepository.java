package com.example.capstone_g25_be.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.example.capstone_g25_be.entities.Product;
import com.example.capstone_g25_be.model.response.Product1;

@Repository
@EnableJpaRepositories
public interface ProductRepository extends JpaRepository<Product, Long> {

	@Query(value = "SELECT * FROM product WHERE MATCH(name, product_code) "
            + "AGAINST (?1) "
			+ "AND category_id = CASE WHEN ?2 IS NULL THEN category_id ELSE ?2 END "
			+ "AND manufacturer_id = CASE WHEN ?3 IS NULL THEN manufacturer_id ELSE ?3 END "
			+ "AND deleted_at = false"
, nativeQuery = true )
	Page<Product> findBySearch(String productName, Long categoryId, Long manufactorId, Pageable pageable);
	
	@Query("SELECT p FROM Product p WHERE p.category.id = CASE WHEN ?1 IS NULL THEN p.category.id ELSE ?1 END "
			+ "AND p.manufacturer.id = CASE WHEN ?2 IS NULL THEN p.manufacturer.id ELSE ?2 END "
			+ "AND p.deletedAt = false")
	Page<Product> findAllBySearch(Long categoryId, Long manufactorId, Pageable pageable);
	
	
	@Query("SELECT p FROM Product p Where p.id = ?1 AND p.deletedAt = false" )
	Product findProductById(Long productId);
	
	@Query("SELECT p FROM Product p Where p.deletedAt = false Order By id DESC" )
	List<Product> findProductLatest();
	
	@Query("SELECT p FROM Product p WHERE p.manufacturer.id = ?1 AND p.deletedAt = false")
	Page<Product> findAllByManufactureId(Long manufactorId, Pageable pageable);
	
	@Query("SELECT p FROM Product p WHERE p.id IN (?1) AND p.deletedAt = false")
	List<Product> findListAllByConsignmentList(Set<Long> manufactorId);
	
	@Query("SELECT p FROM Product p Where p.name = ?1 AND p.deletedAt = false" )
	Product findProductByName(String productName);
	
	@Query("SELECT COUNT(*) FROM Product p WHERE p.deletedAt = false")
	Integer countProduct();
	
	@Query("SELECT p FROM Product p WHERE p.deletedAt = false")
	List<Product> findAllProduct();

	@Query("SELECT p FROM Product p WHERE p.manufacturer.id = ?1 AND p.deletedAt = false")
	List<Product> findAllByManufactureId(Long manufacturerId);
	
	@Query("SELECT p FROM Product p  Where id <> ?1 and name = ?2 AND deleted_at = false")
	Product findProductByIdAndName(Long id, String categoryName);

	@Query(nativeQuery = true , value = "SELECT p.id as id, p.name as name, p.product_code as productCode,"
			+ " p.unit_measure as unitMeasure, p.wrap_unit_measure as wrapUnitMeasure,"
			+ " p.unit_price as unitPrice, p.number_of_wrap_unit_measure as numberOfWrapUnitMeasure"
			+ " FROM product p Where p.id = ?1 AND p.deleted_at = false")
	Product1 findProductById1(Long productId);
	
	@Query(nativeQuery =  true, value = "SELECT p.id as id, p.name as name, p.product_code as productCode,"
			+ "  p.unit_measure as unitMeasure, p.wrap_unit_measure as wrapUnitMeasure,"
			+ " p.unit_price as unitPrice, p.number_of_wrap_unit_measure as numberOfWrapUnitMeasure "
			+ " FROM product p left join consignment c"
			+ " on p.id =  c.product_id WHERE p.deleted_at = false and c.quantity > 0"
			+ " and c.import_date is not null group by p.product_code")
	List<Product1> findProductInWareHouse();

	@Query("SELECT p FROM Product p  Where id <> ?1 and productCode = ?2 AND deleted_at = false")
	Product findProductByIdAndProductCode(Long id, String productCode);

	@Query("SELECT p FROM Product p Where p.productCode = ?1 AND p.deletedAt = false" )
	Product findProductByProductCode(String trim);
}
