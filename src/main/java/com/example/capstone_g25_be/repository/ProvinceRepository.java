package com.example.capstone_g25_be.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.example.capstone_g25_be.entities.Category;
import com.example.capstone_g25_be.entities.Province;

@Repository
@EnableJpaRepositories
public interface ProvinceRepository extends JpaRepository<Province, Long> {
	
	@Query("SELECT p FROM Province p Where p.id = ?1 AND p.deletedAt = false" )
	Province findProvinceById(Long categoryId);

}
