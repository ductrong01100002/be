package com.example.capstone_g25_be.repository;

import com.example.capstone_g25_be.entities.Order;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public interface ReturnOrderRepository extends JpaRepository<Order,Long> {

    /**
     * Get list return order
     */
    @Query(nativeQuery = true,value = "SELECT count(*) over() AS totalRecord,od.id AS orderId, \n" +
            "od.bill_reference_number AS billRefernce ,od.created_date AS createDate,u.id AS userId ,u.username AS createBy,\n" +
            "sum(odt.quantity*odt.unit_price) AS totalPrice \n" +
            "FROM `order` od \n" +
            "LEFT JOIN order_detail odt ON od. id = odt.order_id\n" +
            "LEFT JOIN consignment c ON c.id = odt.consignment_id\n" +
            "LEFT JOIN order_type ot ON ot.id = od.order_type_id\n" +
            "LEFT JOIN user u ON u.id = od.user_id\n" +
            "Where ot.id=3 \n" +
            "AND ((?3 is null AND ?4 is null)  OR DATE(od.created_date) between ?3 AND ?4 ) \n" +
            "AND (?1 is null OR od.user_id = ?1) \n" +
            "AND (?2 is null OR upper(od.bill_reference_number) Like UPPER('%' ?2 '%')) \n" +
            "group by  odt.order_id")
    List<Map<String, Object>> getListReturnOrderBySearchData(Long userId, String billReferenceNumber, Date startDate, Date endDate, Pageable pagable);

    /**
     * API 1 Get infor export order
     */
    @Query(nativeQuery = true,value = "SELECT DISTINCT od.bill_reference_number AS billRefernce ," +
            "mn.id AS manufactorId,mn.name AS manufactorName,st.name AS statusName , od.created_date AS createDate," +
            "ads.detail AS addressDetail,p.name AS provinceName,\n" +
            "d.name AS districtName, w.name AS wardName ,od.description AS description,u.id AS userId ,u.username AS createBy, od.confirm_date AS confirmDate\n" +
            " FROM `order` od \n" +
            "LEFT JOIN manufacturer mn ON od.manufacturer_id = mn.id\n" +
            "LEFT JOIN status st ON od.status_id = st.id\n" +
            "LEFT JOIN address ads ON ads.id = mn.address_id\n" +
            "LEFT JOIN province p ON p.id = ads.province_id\n" +
            "LEFT JOIN district d ON d.id = ads.district_id\n" +
            "LEFT JOIN ward w ON w.id = ads.ward_id\n" +
            "LEFT JOIN order_detail odt ON odt.order_id= od.id\n" +
            "LEFT JOIN consignment sp ON sp.id = odt.consignment_id\n" +
            "LEFT JOIN warehouse i ON i.id = sp.warehouse_id\n" +
            "LEFT JOIN user u ON u.id = od.user_id\n" +
            "WHERE od.id=?1")
    Map<String, Object> getOrderDetail(Long orderId);

    /**
     * API 2 : Get list product in Detail Export order
     */
    @Query(nativeQuery = true,value = "SELECT  c.product_id AS productId, p.product_code AS productCode,p.name AS productName\n" +
            ",p.unit_measure AS unitMeasure,sum(odt.quantity) AS quantity,sum(odt.quantity*odt.unit_price) AS totalPrice\n" +
            ",odt.unit_price AS unitPrice ,p.number_of_wrap_unit_measure AS numberOfWrapUnitMeasure,p.wrap_unit_measure AS wrapUnitMeasure\n" +
            "FROM order_detail odt \n" +
            "LEFT JOIN consignment c ON odt.consignment_id = c.id\n" +
            "LEFT JOIN product p ON p.id = c.product_id\n" +
            "where odt.order_id = ?1\n" +
            "group by c.product_id")
    List<Map<String, Object>> getDetailExportGoods2(Long orderId, Pageable pagable);

    @Query(nativeQuery = true,value = "SELECT c.product_id AS productId, c.id consignmentId,c.import_date AS importDate,\n" +
            "c.expiration_date AS expirationDate,odt.quantity AS quantity,i.id AS wareHouseId,i.name AS wareHouseName \n" +
            "FROM order_detail odt \n" +
            "LEFT JOIN consignment c ON odt.consignment_id = c.id\n" +
            "LEFT JOIN warehouse i ON i.id = c.warehouse_id\n" +
            "where odt.order_id = ?1")
    List<Map<String, Object>> getListConsignment(Long orderId);
}
