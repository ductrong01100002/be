package com.example.capstone_g25_be.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.example.capstone_g25_be.entities.ReturnToManufacturerDetail;

@Repository
@EnableJpaRepositories
public interface ReturnToManufacturerDetailRepository extends JpaRepository<ReturnToManufacturerDetail, Long>{

	@Query("SELECT returnToManufacturerDetail FROM ReturnToManufacturerDetail returnToManufacturerDetail "
			+ "Where returnToManufacturerDetail.returnToManufacturer.id = ?1 " )
	List<ReturnToManufacturerDetail> findAllByReturnToManufacturerId(Long returnToManufacturerId);

}
