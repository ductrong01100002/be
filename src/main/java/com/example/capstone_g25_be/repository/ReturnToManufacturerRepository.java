package com.example.capstone_g25_be.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.example.capstone_g25_be.entities.InventoryCheckingHistory;
import com.example.capstone_g25_be.entities.ReturnToManufacturer;

@Repository
@EnableJpaRepositories
public interface ReturnToManufacturerRepository extends JpaRepository<ReturnToManufacturer, Long> {

	@Query(value = "SELECT returnToManufacturer FROM ReturnToManufacturer returnToManufacturer "
	+ "Where returnToManufacturer.user.id = CASE WHEN ?1 IS NOT NULL THEN ?1 ELSE returnToManufacturer.user.id END "
	+ "AND returnToManufacturer.status.id = CASE WHEN ?2 IS NOT NULL THEN ?2 ELSE returnToManufacturer.status.id END "
	+ "AND returnToManufacturer.manufacturer.id = CASE WHEN ?3 IS NOT NULL THEN ?3 ELSE returnToManufacturer.manufacturer.id END "
	+ "AND returnToManufacturer.createDate >= CASE WHEN ?4 IS NOT NULL THEN ?4 ELSE returnToManufacturer.createDate END "
	+ "AND returnToManufacturer.createDate <= CASE WHEN ?5 IS NOT NULL THEN ?5 ELSE returnToManufacturer.createDate END ")
	Page<ReturnToManufacturer> findAllReturnToManufacturer(Long userId, Long statusId, Long manufacturerId,
			String startDate, String endDate, Pageable pageable);
	
	@Query("SELECT COUNT(*) "
			+ "  FROM ReturnToManufacturer "
			+ "  WHERE status.id = 1")
	Integer countReturnToManufacturerNotCompleted();
	
	@Query("SELECT r FROM ReturnToManufacturer r Where r.id = ?1 AND r.deletedAt = false" )
	ReturnToManufacturer findReturnToManufacturerById(Long returnToManufacturerId);

//	ReturnToManufacturer1 getById1(Long returnToManufacturerId);

}
