package com.example.capstone_g25_be.repository;


import com.example.capstone_g25_be.entities.ERole;
import com.example.capstone_g25_be.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {
    /*
       find role by role_name
     */
    Optional<Role> findByRoleName(ERole name);

    @Query("SELECT r from Role r WHERE r.id in (1,2)")
    List<Role> getRole();
}
