package com.example.capstone_g25_be.repository;

import com.example.capstone_g25_be.entities.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Map;

public interface StaffRepository extends JpaRepository<User, Long> {

    @Query(nativeQuery = true, value = "select count(*) over() AS totalRecord, u.id AS id,u.image AS imageUrl, u.username AS userName, u.full_name AS fullName, u.phone AS phone,r.id AS roleId,r.role_name AS roleName,u.active AS isActive\n" +
            "from user u\n" +
            "JOIN user_roles ur ON u.id = ur.user_id\n" +
            "JOIN role r ON r.id = ur.role_id\n" +
            "where r.id in(2,3) AND \n" +
            " CASE WHEN ?2 ='fullName' THEN UPPER(u.full_name)  Like UPPER('%' ?1 '%') \n " +
            "  WHEN ?2 ='userName' THEN UPPER(u.username)   Like UPPER('%' ?1 '%') \n" +
            "  WHEN ?2 ='phone'    THEN UPPER(u.phone)      Like UPPER('%' ?1 '%') \n " +
            " ELSE UPPER(u.full_name)  Like UPPER('%' ?1 '%') END order by u.id desc ")
    List<Map<String, Object>> getListStaffBySearchData(String keyWords, String sortColumn, Pageable pagable);

    @Query(nativeQuery = true, value = "select count(*) over() AS totalRecord, u.id AS id ,u.image AS imageUrl,u.username AS userName, u.full_name AS fullName, u.phone AS phone,r.id AS roleId,r.role_name AS roleName,u.active AS isActive\n" +
            "from user u\n" +
            "JOIN user_roles ur ON u.id = ur.user_id\n" +
            "JOIN role r ON r.id = ur.role_id WHERE r.id in(2,3) order by u.id desc")
    List<Map<String, Object>> getListAll(Pageable pagable);

    @Query(nativeQuery = true, value = "select u.image AS imageUrl, u.full_name AS fullName, u.username AS userName, u.phone AS phone,u.email AS email ,\n" +
            "u.identity_card AS identityCard,u.address_id AS addressId,ad.detail AS detailAddress,p.id AS provinceId,p.name AS provinceName , " +
            "dt.id AS districtId ,dt.name AS districtName,w.id AS wardId ,w.name AS wardName,\n" +
            "r.id AS roleId,r.role_name AS roleName,u.active AS isActive,\n" +
            "Date(u.date_of_birth) AS dateOfBirth , u .gender AS gender\n" +
            "from user u\n" +
            "JOIN user_roles ur ON u.id = ur.user_id\n" +
            "JOIN role r ON r.id = ur.role_id\n" +
            "JOIN address ad ON ad.id = u.address_id\n" +
            "JOIN province p ON p.id = ad.province_id\n" +
            "JOIN district dt ON dt.id = ad.district_id\n" +
            "JOIN ward w ON w.id = ad.ward_id\n" +
            "where u.id =?1")
    Map<String, Object> getDetailStaff(Long id);

    @Query(nativeQuery = true, value = "select u.image AS imageUrl,u.full_name AS fullName, u.username AS userName, u.phone AS phone,u.email AS email ,\n" +
            "u.identity_card AS identityCard,u.address_id AS addressId,ad.detail AS detailAddress,p.id AS provinceId,p.name AS provinceName , " +
            "dt.id AS districtId ,dt.name AS districtName,w.id AS wardId ,w.name AS wardName,\n" +
            "u.active AS isActive,\n" +
            "Date(u.date_of_birth) AS dateOfBirth , u .gender AS gender\n" +
            "from user u\n" +
            "JOIN user_roles ur ON u.id = ur.user_id\n" +
            "JOIN role r ON r.id = ur.role_id\n" +
            "JOIN address ad ON ad.id = u.address_id\n" +
            "JOIN province p ON p.id = ad.province_id\n" +
            "JOIN district dt ON dt.id = ad.district_id\n" +
            "JOIN ward w ON w.id = ad.ward_id\n" +
            "where u.id =?1")
    Map<String, Object> getDetailProfile(Long id);
}
