package com.example.capstone_g25_be.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.example.capstone_g25_be.entities.Status;
@Repository
@EnableJpaRepositories
public interface StatusRepository extends JpaRepository<Status, Long>{
	
	@Query("SELECT s FROM Status s where s.id = ?1")
	Status findStatusById(Long statusId);

}
