package com.example.capstone_g25_be.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.capstone_g25_be.entities.Category;
import com.example.capstone_g25_be.entities.SubCategory;
import com.example.capstone_g25_be.entities.Warehouse;

public interface SubCategoryRepository extends JpaRepository<SubCategory, Long>{
	
	@Query("SELECT sc FROM SubCategory sc Where sc.category.id = ?1 AND sc.deletedAt = false" )
	List<SubCategory> getSubCategoryByCategoryId(Long categoryId);
	
	@Query("SELECT sc FROM SubCategory sc "
			+ "Where sc.category.id = ?1 AND sc.deletedAt = false" )
	Page<SubCategory> findAllSubCategory(Long categoryId,Pageable pageable);
	
	@Query("SELECT sc FROM SubCategory sc Where sc.name = ?1 AND sc.deletedAt = false" )
	SubCategory findCategoryByName(String subCategoryName);
	
	@Query("SELECT sc FROM SubCategory sc Where sc.id = ?1 AND sc.deletedAt = false" )
	SubCategory findSubCategoryById(Long subCategoryId);
	
	@Query("SELECT sc FROM SubCategory sc  Where id <> ?1 and name = ?2 AND deleted_at = false")
	SubCategory findSubCategoryByIdAndName(Long id, String categoryName);

}
