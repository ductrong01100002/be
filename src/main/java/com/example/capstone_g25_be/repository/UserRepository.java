package com.example.capstone_g25_be.repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.example.capstone_g25_be.entities.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.example.capstone_g25_be.entities.User;

@Repository
@EnableJpaRepositories
public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findByUserName(String username);

	@Query("SELECT u.email FROM User u Where u.userName =?1")
	String findEmailByUserName(String username);

	Boolean existsByUserName(String username);

	Boolean existsByEmail(String email);

	Boolean existsByIdentityCard(String identityCard);

	Boolean existsByPhone(String phone);

	@Query("SELECT u FROM User u Where u.userName LIKE %?1% AND u.deleteAt = null")
	Page<User> findBySearch(String userName, Pageable pageable);

	@Query(value = "select u.id AS id,u.full_name AS fullName,u.phone AS phone, r.role_name AS role,u.active AS isActive from user u\n" +
			"JOIN user_roles ur ON u.id = ur.user_id\n" +
			"JOIN role r ON r.id = ur.role_id\n",nativeQuery = true)
	List<Map<String,Object>> findByUserBySearchData(String searchData, Pageable pageable);

	@Query("SELECT u FROM User u Where u.deleteAt = false Order By u.id DESC" )
    List<User> findUserLatest();

	Optional<User> findById(Long id);
	
	@Query("SELECT u FROM User u Where u.id =?1")
	User findUserById(Long userId);
	
	@Query(nativeQuery = true, value = "SELECT count(*) FROM user  join user_roles on user.id = user_roles.user_id"
			+ "  WHERE active = true and user_roles.role_id in (2,3)")
	Integer countUser();
}
