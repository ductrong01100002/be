package com.example.capstone_g25_be.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.example.capstone_g25_be.entities.District;
import com.example.capstone_g25_be.entities.Ward;

@Repository
@EnableJpaRepositories
public interface WardRepository extends JpaRepository<Ward, Long> {

	@Query("SELECT w FROM Ward w Where w.district.id = ?1 AND w.deletedAt = false" )
	List<Ward> findListWardByDistrictId(Long districtId);
	
	@Query("SELECT w FROM Ward w Where w.id = ?1 AND w.deletedAt = false" )
	Ward findWardById(Long categoryId);

}
