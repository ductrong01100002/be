package com.example.capstone_g25_be.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.capstone_g25_be.entities.Product;
import com.example.capstone_g25_be.entities.Warehouse;

@Repository
public interface WarehouseRepository extends JpaRepository<Warehouse,Long> {

	@Query(value = "SELECT * FROM warehouse WHERE MATCH(name) "
			+ "AGAINST (?1) AND deleted_at = false", nativeQuery = true )
	Page<Warehouse> findBySearch(String wareHouseNameSearch, Pageable pageable);
	
	@Query("SELECT wh FROM Warehouse wh where wh.deletedAt = false")
	Warehouse findAllWareHouse();
	
	@Query("SELECT wh FROM Warehouse wh where wh.id = ?1 AND wh.deletedAt = false ")
	Warehouse findWareHouseById(Long wareHouseId);
	
	Page<Warehouse> findAllByDeletedAt(boolean deletedAt, Pageable pageable);
	
	@Query("SELECT wh FROM Warehouse wh where wh.name = ?1 AND wh.deletedAt = false ")
	Warehouse findWareHouseByName(String wareHouseName);

	@Query("SELECT wh FROM Warehouse wh where wh.deletedAt = false ")
	List<Warehouse> findAllNotPaging();
	
	@Query("SELECT w FROM Warehouse w  Where id <> ?1 and name = ?2 AND deleted_at = false")
	Warehouse findWarehouseByIdAndName(Long id, String categoryName);
}
