package com.example.capstone_g25_be.security.services;

import com.example.capstone_g25_be.entities.User;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/*
chứa info user get from UserDetailServiceImpl
 */
@Data
public class UserDetailsImpl implements UserDetails {
    private Long id;
    private String userName;
    private String passWord;
    private String fullName;
    private String imageUrl;
    private Boolean isActive;
    private Collection<? extends GrantedAuthority> authorities;

    /*
    get info user and role of user
     */
    public static UserDetails build(User user) {
        List<GrantedAuthority> authorities = user.getRoles().stream()
                .map(role -> new SimpleGrantedAuthority(role.getRoleName().name()))
                .collect(Collectors.toList());
        String URL_IMAGE = "api/staff/download/image/"+user.getId();
        return new UserDetailsImpl(
                user.getId(),
                user.getUserName(),
                user.getPassWord(),
                user.getFullName(),
                URL_IMAGE,
                user.getActive(),
                authorities);
    }

    public UserDetailsImpl(Long id, String userName, String passWord, String fullName, String imageUrl, Boolean isActive, Collection<? extends GrantedAuthority> authorities) {
        this.id = id;
        this.userName = userName;
        this.passWord = passWord;
        this.fullName = fullName;
        this.imageUrl = imageUrl;
        this.isActive = isActive;
        this.authorities = authorities;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return passWord;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public Long getId() {
        return id;
    }
}
