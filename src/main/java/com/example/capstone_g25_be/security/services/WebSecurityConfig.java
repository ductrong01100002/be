package com.example.capstone_g25_be.security.services;


import com.example.capstone_g25_be.entities.ERole;
import com.example.capstone_g25_be.security.jwt.AuthEntryPointJwt;
import com.example.capstone_g25_be.security.jwt.AuthTokenFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        // securedEnabled = true,
        // jsr250Enabled = true,
        prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsServiceImpl userDetailService;
    private final AuthEntryPointJwt unauthorizedHandler;

    public WebSecurityConfig(UserDetailsServiceImpl userDetailService, AuthEntryPointJwt unauthorizedHandler) {
        this.userDetailService = userDetailService;
        this.unauthorizedHandler = unauthorizedHandler;
    }

    @Bean
    public AuthTokenFilter authenticationJwtTokenFilter() {
        return new AuthTokenFilter();
    }

    /*
     bắt buộc AuthenticationManagerBuilder mới gọi dc userDetailService, mọi request thông tin từ người dùng sẽ chui qua method này
     */
    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.userDetailsService(userDetailService).passwordEncoder(passwordEncoder());
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /*
        mọi request sẽ đi qua method này
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()// tat session id
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().authorizeRequests().antMatchers("/api/auth/signin").permitAll()
                .and().authorizeRequests().antMatchers("/api/auth/signup").permitAll()
                //configure import Order
                //.hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/import-order/create").permitAll()
                .and().authorizeRequests().antMatchers("/api/import-order/update").hasAnyAuthority(ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/import-order/list").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/import-order/detail/{orderId}").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/import-order/list-product").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/import-order/confirm/{orderId}/{confirmUserId}").hasAnyAuthority(ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/import-order/cancel/{orderId}").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/import-order/list-create-by").permitAll()
                //configure export Order
                //.hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/export-order/create").permitAll()
                .and().authorizeRequests().antMatchers("/api/export-order/list").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/export-order/detail/{orderId}").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/export-order/list-product-detail").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/export-order/list-quantity-instock").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/export-order/update").hasAnyAuthority(ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/export-order/confirm/{orderId}/{confirmUserId}").hasAnyAuthority(ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/export-order/cancel/{orderId}").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                //configure return order
                .and().authorizeRequests().antMatchers("/api/return-order/").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/return-order/list").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/return-order/detail").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                //configure  staff
                .and().authorizeRequests().antMatchers("/api/staff/get-role").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/staff/detail/{id}").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/staff/detail-profile/{id}").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/staff/update").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/staff/update-status/{id}").hasAnyAuthority(ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/staff/reset-password").hasAnyAuthority(ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/staff/update-role/{id}").hasAnyAuthority(ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/staff/add/image").permitAll()
                .and().authorizeRequests().antMatchers("/api/staff/update/image/{userId}").permitAll()
                .and().authorizeRequests().antMatchers("/api/staff/download/image/{userId}").permitAll()
                .and().authorizeRequests().antMatchers("/api/user/**").permitAll()
                //configure address
                .and().authorizeRequests().antMatchers("/api/address/add").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                //configure export category
                .and().authorizeRequests().antMatchers("/api/category/**").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/consignment/**").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/dashBoard/**").permitAll()
                .and().authorizeRequests().antMatchers("/api/district/**").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/ward/**").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/manufacturer/**").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/province/**").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/subCategory/**").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                //configure inventoryCheckingHistory
                .and().authorizeRequests().antMatchers("/api/inventoryCheckingHistory/**").hasAnyAuthority(ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                //configure product
                .and().authorizeRequests().antMatchers("/api/product").permitAll()
                .and().authorizeRequests().antMatchers("/api/product/{productId}").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/product/add").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/product/update").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/product/download-template").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/product/import-product").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/product/wareHouse/{wareHouseId}").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/product/consignment/{productId}").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/product/manufacturer/{manufacturerId}").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/product/add/image").permitAll()
                .and().authorizeRequests().antMatchers("/api/product/update/image/{productId}").permitAll()
                .and().authorizeRequests().antMatchers("/api/product/download/image/{productId}").permitAll()

                //configure ReturnToManufacturerController
                .and().authorizeRequests().antMatchers("/api/returnToManufacturer/add").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/returnToManufacturer/confirm/{returnToManufacturerId}").hasAnyAuthority(ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/returnToManufacturer/update/{returnToManufacturerId}").hasAnyAuthority(ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/returnToManufacturer").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/returnToManufacturer/detail/{returnToManufacturerId}").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/returnToManufacturer/delete/{returnToManufacturerId}").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                //configure warehouse
                .and().authorizeRequests().antMatchers("/api/warehouse/{wareHouseId}").hasAnyAuthority(ERole.ROLE_SELLER.name(), ERole.ROLE_STOREKEEPER.name(), ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/warehouse/add").hasAnyAuthority(ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/warehouse/update").hasAnyAuthority(ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/warehouse/delete/{wareHouseId}").hasAnyAuthority(ERole.ROLE_OWNER.name())
                .and().authorizeRequests().antMatchers("/api/warehouse/notPaging").permitAll()
                .and().authorizeRequests().antMatchers("/api/warehouse").permitAll()
                .anyRequest().authenticated();
        http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
    }
}
