package com.example.capstone_g25_be.services;

import org.springframework.http.ResponseEntity;

import com.example.capstone_g25_be.model.request.AddressRequest;

public interface AddressService {
	
	ResponseEntity<?> addAddress(AddressRequest addressRequest);

}
