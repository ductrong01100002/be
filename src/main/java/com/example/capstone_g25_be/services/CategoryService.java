package com.example.capstone_g25_be.services;

import org.springframework.http.ResponseEntity;

import com.example.capstone_g25_be.model.request.CategoryRequest;

public interface CategoryService {
	
	ResponseEntity<?> findAll(int pageIndex, int pageSize,
			String categoryName, String categoryDescription);
	
	ResponseEntity<?> addCategory(CategoryRequest categoryRequest);

	ResponseEntity<?> findById(Long categoryId, Integer pageIndex, Integer pageSize);

	ResponseEntity<?> deleteCategory(Long categoryId);

	ResponseEntity<?> updateCategory(CategoryRequest categoryRequest);

	ResponseEntity<?> findAllNotPaging();

}
