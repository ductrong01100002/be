package com.example.capstone_g25_be.services;

import org.springframework.http.ResponseEntity;

public interface ConsignmentService {
	
	ResponseEntity<?> findAll(int pageIndex, int pageSize);

}
