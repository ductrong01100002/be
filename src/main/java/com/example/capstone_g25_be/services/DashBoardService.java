package com.example.capstone_g25_be.services;

import org.springframework.http.ResponseEntity;


public interface DashBoardService {
	void dashBoardOwner();
	
	ResponseEntity<?> dashBoardOwnerlist();
}
