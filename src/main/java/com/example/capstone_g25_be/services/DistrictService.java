package com.example.capstone_g25_be.services;

import org.springframework.http.ResponseEntity;

public interface DistrictService {
	
	ResponseEntity<?> findByProvinceId(Long provinceId);

}
