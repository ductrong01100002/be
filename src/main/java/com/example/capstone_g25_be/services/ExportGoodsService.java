package com.example.capstone_g25_be.services;

import com.example.capstone_g25_be.entities.ServiceResult;
import com.example.capstone_g25_be.model.request.ExportGoodsRequest;
import com.example.capstone_g25_be.model.request.ImportGoodsRequest;
import com.example.capstone_g25_be.model.request.UpdateExportOrder;
import com.example.capstone_g25_be.model.request.UpdateImportGoodsRequest;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.Map;

public interface ExportGoodsService {
    ServiceResult<Map<String,Object>> getListExportOrder(Long userId, String billReferenceNumber, Date startDate, Date endDate,Integer status, Integer pageIndex, Integer pageSize);

    Map<String, Object> getListProductByImportDate();

    Map<String,Object> getProductInstock(Long productId,Integer pageIndex, Integer pageSize);

    ServiceResult<Map<String, Object>> getExportOrderDetail(Long orderId);

    ServiceResult<Map<String, Object>> getlistProductOfExportGoods(Long orderId, Integer pageIndex, Integer pageSize);

    ResponseEntity<?> confirm(Long orderId,Long confirmUserId);

    ResponseEntity<?> cancel(Long orderId);

    ResponseEntity<?> save(ExportGoodsRequest exportGoodsRequest);

    ResponseEntity<?> update(UpdateExportOrder updateExportOrder);
}
