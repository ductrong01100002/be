package com.example.capstone_g25_be.services;

import com.example.capstone_g25_be.entities.ServiceResult;
import com.example.capstone_g25_be.model.request.ImportGoodsRequest;
import com.example.capstone_g25_be.model.request.UpdateImportGoodsRequest;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.Map;

public interface ImportGoodsService {
    ResponseEntity<?> save(ImportGoodsRequest importGoodsRequest);

    ResponseEntity<?> update(UpdateImportGoodsRequest updateImportGoodsRequest);

    ResponseEntity<?> confirm(Long orderId,Long confirmUserId);

    ResponseEntity<?> cancel(Long orderId);

    ServiceResult<Map<String,Object>> getListImportGoods(Long userId, String billReferenceNumber, Date startDate, Date endDate,Integer status, Integer pageIndex, Integer pageSize);

    ServiceResult<Map<String, Object>> getInforDetailImportGoods(Long orderId);

    ServiceResult<Map<String, Object>> getlistProductOfImportGoods(Long orderId,Integer pageIndex, Integer pageSize);
}
