package com.example.capstone_g25_be.services;

import org.springframework.http.ResponseEntity;

import com.example.capstone_g25_be.model.request.InventoryCheckingHistoryRequest;

public interface InventoryCheckingHistoryService {
	
	ResponseEntity<?> addInventoryCheckingHistory(InventoryCheckingHistoryRequest inventoryCheckingHistoryRequest);
	
	ResponseEntity<?> findByIdAndFindListDetail(Long inventoryCheckingHistoryId);

	ResponseEntity<?> findAll(Integer pageIndex, Integer pageSize, Long wareHouseId, Long userId,
			String startDate, String endDate, String orderBy, String order);
	
}
