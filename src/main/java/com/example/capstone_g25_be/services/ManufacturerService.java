package com.example.capstone_g25_be.services;

import org.springframework.http.ResponseEntity;

import com.example.capstone_g25_be.model.request.ManufacturerRequest;

public interface ManufacturerService {

	ResponseEntity<?> findAll(int pageIndex, int pageSize,
			String manufactorName, String manufactorEmail);
	
	ResponseEntity<?> addManufacturer(ManufacturerRequest manufactorRequest);

	ResponseEntity<?> findById(Long manufactorId, int pageIndex, int pageSize);

	ResponseEntity<?> findAllNotPaging();

}
