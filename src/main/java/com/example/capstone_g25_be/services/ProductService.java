package com.example.capstone_g25_be.services;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.multipart.MultipartFile;

import com.example.capstone_g25_be.model.request.ProductRequest;

public interface ProductService {
	ResponseEntity<?> findAll(int pageIndex, int pageSize, String productName,
			String productCode, Long categoryId, Long manufactorId);
	
	ResponseEntity<?> findById(Long productId, int pageIndex, int pageSize);
	
	ResponseEntity<?> addProduct(ProductRequest productRequest);
	
	ResponseEntity<?> updateProduct(ProductRequest productRequest);

	ResponseEntity<?> addImageProduct(MultipartFile file);

	ResponseEntity<?> updateImageProduct(Long userProfileId, MultipartFile file);

	byte[] downloadProductImage(Long userProfileId);

	Object importProduct(MultipartFile file);
	
	ResponseEntity<?> findProductByWareHouseId(Long wareHouseId);
	
	ResponseEntity<?> findByIdAndFindListConsignment(Long productId, Long warehouseId);

	ResponseEntity<?> findProductBymanufacturerId(Long manufacturerId);

}
