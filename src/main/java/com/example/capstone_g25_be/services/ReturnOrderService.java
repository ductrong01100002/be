package com.example.capstone_g25_be.services;

import com.example.capstone_g25_be.entities.ServiceResult;
import com.example.capstone_g25_be.model.request.ExportGoodsRequest;
import com.example.capstone_g25_be.model.request.ReturnOrderRequest;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.Map;

public interface ReturnOrderService {
    ServiceResult<Map<String,Object>> getListReturnOrder(Long userId, String billReferenceNumber, Date startDate, Date endDate, Integer pageIndex, Integer pageSize);

    ServiceResult<Map<String, Object>> getDetailReturnOrder(Long orderId, Integer pageIndex, Integer pageSize);

    ResponseEntity<?> returnOrder(Long orderId,ReturnOrderRequest returnOrderRequest);
}
