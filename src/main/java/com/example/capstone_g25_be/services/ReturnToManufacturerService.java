package com.example.capstone_g25_be.services;

import org.springframework.http.ResponseEntity;

import com.example.capstone_g25_be.model.request.ReturnToManufacturerRequest;

public interface ReturnToManufacturerService {
	
	ResponseEntity<?> addReturnToManufacturer(ReturnToManufacturerRequest returnToManufacturerRequest);

	ResponseEntity<?> findAll(Integer pageIndex, Integer pageSize, Long dataSearch, Long userId, Long statusId, String startDate,
			String endDate);

	ResponseEntity<?> findByIdAndFindListDetail(Long returnToManufacturerId);

	ResponseEntity<?> confirmReturnToManufacturer(Long returnToManufacturerId, Long userConfirmedId);

	ResponseEntity<?> deleteReturnToManufacturer(Long returnToManufacturerId, Long userDeleteId);

	ResponseEntity<?> updateReturnToManufacturer(Long returnToManufacturerId,
			ReturnToManufacturerRequest returnToManufacturerRequest);

}
