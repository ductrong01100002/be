package com.example.capstone_g25_be.services;

import com.example.capstone_g25_be.model.request.UpdateStaffRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

public interface StaffService {
    Map<String, Object> getListStaff(String keyWords,String sortColumn,@RequestParam(required = false) Integer pageIndex,
                                           @RequestParam(required = false) Integer pageSize);
    Map<String, Object> getDetailStaff(Long id);

    ResponseEntity<Object> updateStaff(UpdateStaffRequest staffRequest);
}
