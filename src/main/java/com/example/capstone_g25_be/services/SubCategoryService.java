package com.example.capstone_g25_be.services;

import org.springframework.http.ResponseEntity;

import com.example.capstone_g25_be.model.request.SubCategoryRequest;

public interface SubCategoryService {
	
	ResponseEntity<?> findSubCategoryByCategoryId(Long cagegoryId);

	ResponseEntity<?> addSubCategory(SubCategoryRequest subCategoryRequest);

	ResponseEntity<?> updateSubCategory(SubCategoryRequest subCategoryRequest);

}
