package com.example.capstone_g25_be.services;

import com.example.capstone_g25_be.entities.ServiceResult;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

public interface UserService {
	
	ResponseEntity<?> findAll(int pageIndex, int pageSize, String userName);

    ResponseEntity<?> getListUser(Integer pageIndex, Integer pageSize, String searchData);

    ServiceResult<Boolean> resetPassword(String login);

    ResponseEntity<?> checkOTPandResetPass(String otp, String login);

    ResponseEntity<?> addImageUser(MultipartFile file);

    ResponseEntity<?> updateImageUser(Long productId, MultipartFile file);

    byte[] downloadUserImage(Long productId);

    ResponseEntity<?>settingPasswword(String oldPassword,String newPassword, String userName);

    ResponseEntity<?> createPasswword(String userName,String newPassword);
}
