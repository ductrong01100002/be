package com.example.capstone_g25_be.services;

import org.springframework.http.ResponseEntity;

public interface WardService {
	
	ResponseEntity<?> findByDistrictId(Long districtId);

}
