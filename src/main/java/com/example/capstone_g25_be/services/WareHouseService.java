package com.example.capstone_g25_be.services;

import org.springframework.http.ResponseEntity;

import com.example.capstone_g25_be.model.request.WareHouseRequest;

public interface WareHouseService {
	
	ResponseEntity<?> findAll(int pageIndex, int pageSize, String wareHouseName);

	ResponseEntity<?> addWareHouse(WareHouseRequest wareHouseRequest);

	ResponseEntity<?> deleteWareHouse(Long wareHouseId);

	ResponseEntity<?> findWareHouseById(Long wareHouseId);

	ResponseEntity<?> updateWareHouse(WareHouseRequest wareHouseRequest);

	ResponseEntity<?> findAllNotPaging();
	

}
