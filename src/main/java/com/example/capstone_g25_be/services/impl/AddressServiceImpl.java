package com.example.capstone_g25_be.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.capstone_g25_be.entities.Address;
import com.example.capstone_g25_be.entities.District;
import com.example.capstone_g25_be.entities.Province;
import com.example.capstone_g25_be.entities.Ward;
import com.example.capstone_g25_be.model.request.AddressRequest;
import com.example.capstone_g25_be.model.response.ResponseVo;
import com.example.capstone_g25_be.repository.AddressRepository;
import com.example.capstone_g25_be.repository.DistrictRepository;
import com.example.capstone_g25_be.repository.ProvinceRepository;
import com.example.capstone_g25_be.repository.WardRepository;
import com.example.capstone_g25_be.services.AddressService;
import com.example.capstone_g25_be.utils.AddressConvertUtil;

@Service
public class AddressServiceImpl implements AddressService{
	
	@Autowired
	AddressRepository addressRepository;
	
	@Autowired
	ProvinceRepository provinceRepository;
	
	@Autowired
	DistrictRepository districtRepository;
	
	@Autowired
	WardRepository wardRepository;
	
	@Autowired
	AddressConvertUtil addressConvertUtil;

	@Override
	public ResponseEntity<?> addAddress(AddressRequest addressRequest) {
		Province province = provinceRepository.findProvinceById(addressRequest.getProvinceId());
		District district = districtRepository.findDistrictById(addressRequest.getDistrictId());
		Ward ward = wardRepository.findWardById(addressRequest.getWardId());
		Address address = new Address();
		address.setProvince(province);
		address.setDistrict(district);
		address.setWard(ward);
		address.setDetail(addressRequest.getAddressDetail());
		addressRepository.save(address);
		ResponseVo responseVo = new ResponseVo();
		responseVo.setMessage("Tạo thành công !!");
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

}
