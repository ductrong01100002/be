package com.example.capstone_g25_be.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.example.capstone_g25_be.entities.Category;
import com.example.capstone_g25_be.entities.Consignment;
import com.example.capstone_g25_be.entities.SubCategory;
import com.example.capstone_g25_be.entities.Warehouse;
import com.example.capstone_g25_be.model.request.CategoryRequest;
import com.example.capstone_g25_be.model.response.CategoryDetailResponse;
import com.example.capstone_g25_be.model.response.ListCategoryResponse;
import com.example.capstone_g25_be.model.response.ListSubCategoryResponse;
import com.example.capstone_g25_be.model.response.ResponseVo;
import com.example.capstone_g25_be.repository.CategoryRepository;
import com.example.capstone_g25_be.repository.SubCategoryRepository;
import com.example.capstone_g25_be.services.CategoryService;
import com.example.capstone_g25_be.utils.CategoryConvertUtil;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	CategoryRepository categoryRepository;

	@Autowired
	SubCategoryRepository subCategoryRepository;

	@Autowired
	CategoryConvertUtil categoryConvertUtil;

	@Override
	public ResponseEntity<?> findAll(int pageIndex, int pageSize, String categoryName, String categoryDescription) {
		Pageable pageable = PageRequest.of(pageIndex - 1, pageSize);
		String nameSearch = "";
		Page<Category> categoryPage = null;
		if (!ObjectUtils.isEmpty(categoryName)) {
			nameSearch = categoryName;
			categoryPage = categoryRepository.findBySearch(nameSearch.trim(), pageable);
		}else {
			categoryPage = categoryRepository.findAllByDeletedAt(false,pageable);
		}

		Map<String, Object> mapDataSearch = new HashMap<>();
		mapDataSearch.put("categoryName", categoryName);
		mapDataSearch.put("categoryDescription", categoryDescription);
		ResponseVo responseVo = new ResponseVo();
		Map<String, Object> map = new HashMap<>();
		if (categoryPage.isEmpty()) {
			map.put("category", categoryPage.getContent());
			map.put("totalRecord", 0);
			responseVo.setMessage("Không tìm thấy List Category");
			responseVo.setData(map);
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		List<SubCategory> listSubCategory = new ArrayList<>();
		listSubCategory = subCategoryRepository.findAll();
		map.put("category", ListCategoryResponse.createSuccessData(categoryPage.getContent(), listSubCategory));
		map.put("searchData", mapDataSearch);
		map.put("currentPage", pageIndex);
		map.put("totalRecord", categoryPage.getTotalElements());
		map.put("pageSize", categoryPage.getSize());
		map.put("totalPage", categoryPage.getTotalPages());
		responseVo.setData(map);

		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> addCategory(CategoryRequest categoryRequest) {
		ResponseVo responseVo = new ResponseVo();
		Category categoryExist = categoryRepository.findCategoryByName(categoryRequest.getName().trim());
		if(categoryExist != null) {
			responseVo.setMessage("Tên danh mục đã bị trùng !!");
			return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
		}
		SubCategory subCategory = subCategoryRepository.findCategoryByName(categoryRequest.getName().trim());
		if(subCategory != null) {
			responseVo.setMessage("Tên danh mục cha không được trùng với danh mục con!!");
			return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
		}
		Category category = categoryConvertUtil.convertCategory(categoryRequest);
		category.setDeletedAt(false);
		categoryRepository.save(category);
		responseVo.setMessage("Tạo thành công !!");
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> findById(Long categoryId, Integer pageIndex, Integer pageSize) {
		ResponseVo responseVo = new ResponseVo();
		if (!ObjectUtils.isEmpty(categoryId)) {

			Category category = categoryRepository.findCategoryById(categoryId);
			Map<String, Object> map = new HashMap<>();

			if (!ObjectUtils.isEmpty(category)) {
				Pageable pageable = PageRequest.of(pageIndex - 1, pageSize);
				Page<SubCategory> subCategoryPage = subCategoryRepository.findAllSubCategory(categoryId, pageable);
				if (subCategoryPage.isEmpty()) {
					map.put("subCategory", subCategoryPage.getContent());
					map.put("totalRecord", 0);
					responseVo.setMessage("Không tìm thấy List Sub Category");
					responseVo.setData(map);
				}
				map.put("category", CategoryDetailResponse.createSuccessData(category));
				map.put("subCategory", ListSubCategoryResponse.createSuccessData(subCategoryPage.getContent()));
				map.put("totalRecord", subCategoryPage.getTotalElements());
				map.put("currentPage", pageIndex);
				map.put("pageSize", subCategoryPage.getSize());
				map.put("totalPage", subCategoryPage.getTotalPages());
				responseVo.setData(map);
				return new ResponseEntity<>(responseVo, HttpStatus.OK);
			}
			responseVo.setMessage("Không tìm thấy Product");
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
	}

	@Override
	public ResponseEntity<?> deleteCategory(Long categoryId) {
		ResponseVo responseVo = new ResponseVo();
		Category category = categoryRepository.findCategoryById(categoryId);
		if(category == null) {
			responseVo.setMessage("Không tìm thấy danh mục");
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		List<SubCategory> listSubCategory = subCategoryRepository.getSubCategoryByCategoryId(categoryId);
		if(listSubCategory.size() > 0) {
			responseVo.setMessage("Không thể xóa danh mục vì có danh mục con");
			return new ResponseEntity<>(responseVo, HttpStatus.METHOD_NOT_ALLOWED);
		}
		category.setDeletedAt(true);
		categoryRepository.save(category);
		responseVo.setMessage("Xóa danh mục thành công");
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> updateCategory(CategoryRequest categoryRequest) {
		ResponseVo responseVo = new ResponseVo();
		Category c = categoryRepository.findCategoryByIdAndName(categoryRequest.getId(), categoryRequest.getName());
		if(c != null) {
			responseVo.setMessage("Tên danh mục đã bị trùng !!");
			return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
		}
		SubCategory subCategory = subCategoryRepository.findCategoryByName(categoryRequest.getName().trim());
		if(subCategory != null) {
			responseVo.setMessage("Tên danh mục cha không được trùng với danh mục con!!");
			return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
		}
		Category category = categoryConvertUtil.convertCategory(categoryRequest);
		category.setDeletedAt(false);
		categoryRepository.save(category);
		responseVo.setMessage("Cập nhập thành công !!");
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> findAllNotPaging() {
		List<Category> listCategory = categoryRepository.findAllCategory();
		ResponseVo responseVo = new ResponseVo();
		Map<String, Object> map = new HashMap<>();
		if (listCategory.isEmpty()) {
			map.put("category", listCategory);
			map.put("totalRecord", 0);
			responseVo.setMessage("Không tìm thấy List Category");
			responseVo.setData(map);
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		List<SubCategory> listSubCategory = new ArrayList<>();
		listSubCategory = subCategoryRepository.findAll();
		map.put("category", ListCategoryResponse.createSuccessData(listCategory, listSubCategory));
		responseVo.setData(map);

		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

}
