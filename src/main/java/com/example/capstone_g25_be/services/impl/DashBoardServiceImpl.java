package com.example.capstone_g25_be.services.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.capstone_g25_be.entities.Consignment;
import com.example.capstone_g25_be.entities.InventoryAmout;
import com.example.capstone_g25_be.entities.Product;
import com.example.capstone_g25_be.model.response.DataDashBoardResponse;
import com.example.capstone_g25_be.model.response.ResponseVo;
import com.example.capstone_g25_be.repository.ConsignmentRepository;
import com.example.capstone_g25_be.repository.InventoryAmoutRepository;
import com.example.capstone_g25_be.repository.OrderRepository;
import com.example.capstone_g25_be.repository.ProductRepository;
import com.example.capstone_g25_be.repository.ReturnToManufacturerRepository;
import com.example.capstone_g25_be.repository.UserRepository;
import com.example.capstone_g25_be.services.DashBoardService;

@Service
public class DashBoardServiceImpl implements DashBoardService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	OrderRepository orderRepository;

	@Autowired
	InventoryAmoutRepository inventoryAmoutRepository;

	@Autowired
	ProductRepository productRepository;

	@Autowired
	ConsignmentRepository consignmentRepository;

	@Autowired
	ReturnToManufacturerRepository returnToManufacturerRepository;

	@Override
	public void dashBoardOwner() {
		List<Product> listProduct = productRepository.findAllProduct();
		double totalAmout = 0;
		for (Product product : listProduct) {
			List<Long> lstConsigment = consignmentRepository.findAllConsignmentByProductId(product.getId());
			if (product.getUnitPrice() != 0 && lstConsigment.size()>0) {
				int quantity = consignmentRepository.countQuantity(product.getId());
				totalAmout = totalAmout + quantity * product.getUnitPrice();
			}
		}
		InventoryAmout inventoryAmout = new InventoryAmout();
		inventoryAmout.setAmout(totalAmout);
		inventoryAmout.setSaveDate(LocalDateTime.now());
		inventoryAmoutRepository.save(inventoryAmout);
	}

	@Override
	public ResponseEntity<?> dashBoardOwnerlist() {
		Integer numberUser = userRepository.countUser();

		Integer numberProduct = productRepository.findProductInWareHouse().size();

		Integer numberImportOrderNotConfirmed = orderRepository.countImportOrderNotConfirmed();

		Integer numberExportOrderNotConfirmed = orderRepository.countExportOrderNotConfirmed();

		Integer numberReturnToManufacturerNotCompleted = returnToManufacturerRepository
				.countReturnToManufacturerNotCompleted();
		int flag = 0;
		int day = 30;
		int month = LocalDate.now().getMonthValue();
		int year = LocalDate.now().getYear();
		String date = "";
		List<String> returnDate = new ArrayList<>();
		int a[] = { 3, 6, 9, 12 };
		for (int i = 0; i < 3; i++) {
			if (a[i] > month) {
				flag = i;
			}
		}
		List<InventoryAmout> listInventoryAmoutsResponse = new ArrayList<>();
		for (int i = flag; i < a.length; i++) {
			if (a[i] == 3 || a[i] == 12) {
				day = 31;
			}
			date = year - 1 + "-" + a[i] + "-" + day;
			returnDate.add(date);
			InventoryAmout inventoryAmout = inventoryAmoutRepository.findAllByDesc(date);
			listInventoryAmoutsResponse.add(inventoryAmout);
			day = 30;
		}
		for (int i = 0; i < flag; i++) {
			if (a[i] == 3 || a[i] == 12) {
				day = 31;
			}
			date = year + "-" + a[i] + "-" + day;
			InventoryAmout inventoryAmout = inventoryAmoutRepository.findAllByDesc(date);
			listInventoryAmoutsResponse.add(inventoryAmout);
			returnDate.add(date);
			day = 30;
		}
		InventoryAmout lastInventoryAmout = inventoryAmoutRepository.findLatest();
		LocalDate lastDate = LocalDate.now().minusDays(1);
		ResponseVo responseVo = new ResponseVo();
		Map<String, Object> map = new HashMap<>();
		map.put("data", DataDashBoardResponse.createSuccessData(listInventoryAmoutsResponse, numberUser, numberProduct,
				numberImportOrderNotConfirmed, numberExportOrderNotConfirmed, numberReturnToManufacturerNotCompleted,
				lastInventoryAmout, lastDate, returnDate));
		responseVo.setData(map);
		responseVo.setStatus(false);
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

}
