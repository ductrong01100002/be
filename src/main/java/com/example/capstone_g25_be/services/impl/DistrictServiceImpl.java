package com.example.capstone_g25_be.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.example.capstone_g25_be.entities.District;
import com.example.capstone_g25_be.model.response.ListDistrictResponse;
import com.example.capstone_g25_be.model.response.ResponseVo;
import com.example.capstone_g25_be.repository.DistrictRepository;
import com.example.capstone_g25_be.services.DistrictService;

@Service
public class DistrictServiceImpl implements DistrictService{
	
	@Autowired
	private DistrictRepository districtRepository;

	@Override
	public ResponseEntity<?> findByProvinceId(Long provinceId) {
		ResponseVo responseVo = new ResponseVo();
		if (!ObjectUtils.isEmpty(provinceId)) {
			List<District> listDistrict = districtRepository.findListDistrictByProvinceId(provinceId);
			Map<String, Object> map = new HashMap<>();
			if (!ObjectUtils.isEmpty(listDistrict)) {
				map.put("district", ListDistrictResponse.createSuccessData(listDistrict));
				responseVo.setData(map);
				return new ResponseEntity<>(responseVo, HttpStatus.OK);
			}
			responseVo.setMessage("Không tìm thấy District");
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
	}

}
