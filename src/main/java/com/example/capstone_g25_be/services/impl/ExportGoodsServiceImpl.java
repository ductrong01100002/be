package com.example.capstone_g25_be.services.impl;

import com.example.capstone_g25_be.entities.*;
import com.example.capstone_g25_be.model.request.ConsignmentExport;
import com.example.capstone_g25_be.model.request.ExportGoodsRequest;
import com.example.capstone_g25_be.model.request.UpdateExportOrder;
import com.example.capstone_g25_be.model.response.*;
import com.example.capstone_g25_be.repository.*;
import com.example.capstone_g25_be.services.ExportGoodsService;
import com.example.capstone_g25_be.utils.Constant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ExportGoodsServiceImpl implements ExportGoodsService {
    private final static int DEFAULT_PAGE = 0;
    private final static int DEFAULT_SIZE = 10;
    private final ExportOrderRepository exportOrderRepository;
    private final ImportOrderRepository orderRepository;
    private final ConsignmentRepository consignmentRepository;
    private final OrderTypeRepository orderTypeRepository;
    private final UserRepository userRepository;
    private final OrderDetailRepository orderDetailRepository;
    private final ProductRepository productRepository;

    public ExportGoodsServiceImpl(ExportOrderRepository orderRepository,
                                  ImportOrderRepository orderRepository1,
                                  ConsignmentRepository consignmentRepository,
                                  OrderTypeRepository orderTypeRepository,
                                  UserRepository userRepository,
                                  OrderDetailRepository orderDetailRepository,
                                  ProductRepository productRepository) {
        this.exportOrderRepository = orderRepository;
        this.orderRepository = orderRepository1;
        this.consignmentRepository = consignmentRepository;
        this.orderTypeRepository = orderTypeRepository;
        this.userRepository = userRepository;
        this.orderDetailRepository = orderDetailRepository;
        this.productRepository = productRepository;
    }

    /**
     * Get list export order
     */
    @Override
    public ServiceResult<Map<String, Object>> getListExportOrder(Long userId, String billReferenceNumber, Date startDate, Date endDate,Integer status, Integer pageIndex, Integer pageSize) {

        ServiceResult<Map<String, Object>> mapServiceResult = new ServiceResult<>();
        Map<String,Object> output = new HashMap<>();
        Pageable pagable = PageRequest.of(pageIndex, pageSize,
                Sort.by("createDate").descending());

        try {
            List<Map<String,Object>> orderList =  exportOrderRepository.getListExportBySearchData(userId,billReferenceNumber,startDate,endDate,status,pagable);
            BigInteger totalRecord = BigInteger.valueOf(0);
            if(!orderList.isEmpty()){
                totalRecord = (BigInteger)orderList.get(0).get("totalRecord");
            }
            output.put("orderList",orderList);
            output.put("pageIndex",pageIndex);
            output.put("pageSize",pageSize);
            output.put("totalRecord",totalRecord);
            mapServiceResult.setData(output);
            mapServiceResult.setMessage("success");
            mapServiceResult.setStatus(HttpStatus.OK);
        }catch (Exception e){
            mapServiceResult.setMessage("fail");
            mapServiceResult.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
//            e.printStackTrace();
            return mapServiceResult;
        }
        return mapServiceResult;
    }

    /**
     * Get list product Dropdown list
     */
    @Override
    public Map<String, Object> getListProductByImportDate() {
        Map<String,Object> output = new HashMap<>();
        try {
            List<Map<String,Object>> listProduct =  exportOrderRepository.getListProductByImportDate();
            output.put("data",listProduct);
            output.put("message","success");
            output.put("status",200);
        }catch (Exception e){
            output.put("message","fail");
            output.put("status",HttpStatus.BAD_REQUEST);
            return output;
        }
        return output;
    }

    /**
     * Get product Selected in Dropdown list
     */
    @Override
    public Map<String, Object> getProductInstock(Long productId,Integer pageIndex, Integer pageSize) {
        Map<String,Object> output = new HashMap<>();
        Pageable pageable = PageRequest.of(pageIndex, pageSize);
        List<Map<String,Object>> productInstock =  exportOrderRepository.getProductInstock(productId,pageable);
        List<ExportResponse> exportResponseList = new ArrayList<>();
        List<ProductConsignment> consignments = new ArrayList<>();
        ExportResponse exportResponses = new ExportResponse();
        List<BigInteger> idsAddress = new ArrayList<>();
        for (Map<String,Object> map:productInstock) {
            ProductConsignment productConsignment = new ProductConsignment();
            exportResponses.setProductId((BigInteger)map.get("productId"));
            exportResponses.setProductCode(map.get("productCode").toString());
            exportResponses.setProductName(map.get("productName").toString());
            exportResponses.setUnitMeasure(map.get("unitMeasure").toString());
            exportResponses.setUnitPrice((Double) map.get("unitPrice"));
            exportResponses.setQuantity(0);
            exportResponses.setNumberOfWrapUnitMeasure(map.get("numberOfWrapUnitMeasure")==null?0:(Integer) map.get("numberOfWrapUnitMeasure"));
            exportResponses.setWrapUnitMeasure(map.get("wrapUnitMeasure")==null?null:map.get("wrapUnitMeasure").toString());
            exportResponseList.add(exportResponses);

            productConsignment.setId((BigInteger) map.get("id"));
            idsAddress.add((BigInteger) map.get("wareHouseId"));
            productConsignment.setWarehouseId((BigInteger) map.get("wareHouseId"));
            productConsignment.setWarehouseName(map.get("warehouseName").toString());
            productConsignment.setImportDate(map.get("importDate")==null?null:map.get("importDate").toString()+"+00:00" );
            productConsignment.setExpirationDate(map.get("expirationDate")==null?null:map.get("expirationDate").toString()+"+00:00");
            productConsignment.setQuantityInstock((Integer) map.get("quantityInstock"));
            productConsignment.setQuantity(0);
            consignments.add(productConsignment);
            exportResponses.setConsignmentList(consignments);

        }
        BigInteger totalRecord = BigInteger.valueOf(0);
        if(!productInstock.isEmpty()){
            totalRecord = (BigInteger)productInstock.get(0).get("totalRecord");
        }
        List<Map<String,Object>> addressWarehouse = exportOrderRepository.getListAddressWarehouseByListId(idsAddress);

        output.put("productList",exportResponses);
        output.put("addressWarehouse",addressWarehouse);
        output.put("pageIndex",pageIndex);
        output.put("pageSize",pageSize);
        output.put("totalRecord",totalRecord);
        output.put("status",200);
       return output;
    }

    /**
     * API 1 Get infor export order
     */
    @Override
    public ServiceResult<Map<String, Object>> getExportOrderDetail(Long orderId) {
        ServiceResult<Map<String, Object>> mapServiceResult = new ServiceResult<>();
        Map<String,Object> output = new HashMap<>();

        try {
            Map<String,Object> inforExportDetail =  exportOrderRepository.getOrderDetail(orderId);
            output.put("inforExportDetail",inforExportDetail);
            mapServiceResult.setData(output);
            mapServiceResult.setMessage("success");
            mapServiceResult.setStatus(HttpStatus.OK);
        }catch (Exception e){
            mapServiceResult.setMessage("fail");
            mapServiceResult.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            return mapServiceResult;
        }
        return mapServiceResult;
    }

    /**
     * API 2 : Get list product in Detail Export order
     */
    @Override
    public ServiceResult<Map<String, Object>> getlistProductOfExportGoods(Long orderId, Integer pageIndex, Integer pageSize) {
        pageIndex = pageIndex == null ? DEFAULT_PAGE : pageIndex;
        pageSize = pageSize == null ? DEFAULT_SIZE : pageSize;
        Pageable pagable = PageRequest.of(pageIndex, pageSize);
        ServiceResult<Map<String, Object>> mapServiceResult = new ServiceResult<>();
        Map<String,Object> output = new HashMap<>();
        try {
            List<Map<String,Object>> productList  =  exportOrderRepository.getDetailExportGoods2(orderId,pagable);
            List<Map<String,Object>> consignmentList = exportOrderRepository.getListConsignment(orderId);
            List<BigInteger> idsAddress = new ArrayList<>();
            for (Map<String,Object> map:consignmentList) {
                idsAddress.add((BigInteger) map.get("wareHouseId"));
            }
            List<Map<String,Object>> addressWarehouse = exportOrderRepository.getListAddressWarehouseByListId(idsAddress);
            List<DetailExportReponse> reponse = DetailExportReponse.createSuccessData(productList,consignmentList);
            output.put("productList",reponse);
            output.put("addressWarehouse",addressWarehouse);
            output.put("pageIndex",pageIndex);
            output.put("pageSize",pageSize);
            mapServiceResult.setData(output);
            mapServiceResult.setMessage("success");
            mapServiceResult.setStatus(HttpStatus.OK);
        }catch (Exception e){
            mapServiceResult.setMessage("fail");
            mapServiceResult.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            return mapServiceResult;
        }
        return mapServiceResult;
    }


    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    @Override
    public ResponseEntity<?> save(ExportGoodsRequest exportGoodsRequest) {
        MessageResponse messageResponse = new MessageResponse();
        Order order = new Order();
        User user;
        Status status = new Status();
        try {
            List<ConsignmentExport>  consignmentExports = exportGoodsRequest.getConsignmentExports();

            order.setBillReferenceNumber(exportGoodsRequest.getBillReferenceNumber());
            order.setCreatedDate(LocalDateTime.now());
            order.setDescription(exportGoodsRequest.getDescription());
            user = userRepository.getById(exportGoodsRequest.getUserId());
            order.setUser(user);
            //set type for order
            order.setOrderType(orderTypeRepository.getById(Constant.EXPORT));
            //set status for export order
            status.setId(Constant.PENDING);
            order.setStatus(status);

            orderRepository.save(order);

            //set list-product save into order
            for (ConsignmentExport request : consignmentExports) {
                OrderDetail orderDetail = new OrderDetail();
                orderDetail.setConsignment(consignmentRepository.getById(request.getId()));
                orderDetail.setOrder(order);
                orderDetail.setQuantity(request.getQuantity());
                orderDetail.setUnitPrice(request.getUnitPrice());
                orderDetailRepository.save(orderDetail);
            }
            messageResponse.setMessage("Tạo phiếu xuất kho thành công !");
            messageResponse.setStatus(Constant.SUCCESS);
            log.info("-- End ExportGoodsServiceImpl : save() ----");
            return new ResponseEntity<>(messageResponse, HttpStatus.OK);
        } catch (Exception e) {
            messageResponse.setStatus(Constant.FAIL);
            messageResponse.setMessage("Tạo phiếu xuất kho thất bại !");
            return ResponseEntity
                    .badRequest()
                    .body(messageResponse);
        }
    }

    //con luong update chua lam
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    @Override
    public ResponseEntity<?> update(UpdateExportOrder updateExportOrder) {
        MessageResponse messageResponse = new MessageResponse();
        Order order = orderRepository.findById(updateExportOrder.getOrderId()).orElseThrow(() -> new RuntimeException("Lỗi: Đơn hàng không tồn tại !"));
        User user;
        Status status = new Status();
        try {
            order.setBillReferenceNumber(updateExportOrder.getBillReferenceNumber());
            order.setUpdateDate(LocalDateTime.now());
            order.setDescription(updateExportOrder.getDescription());
            user = userRepository.getById(updateExportOrder.getUserId());
            order.setUser(user);
            //set type for order
            order.setOrderType(orderTypeRepository.getById(Constant.EXPORT));
            //set status for export order
            status.setId(Constant.PENDING);
            order.setStatus(status);
            orderRepository.save(order);

            List<ConsignmentExport>  consignmentExports = updateExportOrder.getConsignmentExports();
                for (ConsignmentExport request : consignmentExports) {
                    OrderDetail odt = orderDetailRepository.findByOrder_IdAndConsignment_Id(updateExportOrder.getOrderId(),request.getId());
                    odt.setQuantity(request.getQuantity());
                    odt.setUnitPrice(request.getUnitPrice());
                    orderDetailRepository.save(odt);
                }
            messageResponse.setMessage("Cập nhật phiếu xuất kho thành công !");
            messageResponse.setStatus(Constant.SUCCESS);
            return new ResponseEntity<>(messageResponse, HttpStatus.OK);
        } catch (Exception e) {
            messageResponse.setStatus(Constant.FAIL);
            messageResponse.setMessage("Cập nhật phiếu xuất kho thất bại !");
            return ResponseEntity
                    .badRequest()
                    .body(messageResponse);
        }
    }

    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    @Override
    public ResponseEntity<?> confirm(Long orderId,Long confirmUserId) {
        MessageResponse messageResponse = new MessageResponse();
        List<Map<String,Object>> consignmentList = exportOrderRepository.getListConsignmentByOrderId(orderId);
        try {
            Order order = orderRepository.getById(orderId);
            if(order.getStatus().getId()==2 ||order.getStatus().getId()==3){
                messageResponse.setMessage("Đơn hàng đã được hủy hoặc xác nhận!");
                messageResponse.setStatus(Constant.FAIL);
                return ResponseEntity
                        .badRequest()
                        .body(messageResponse);
            }
            Status status = new Status();
            status.setId(Constant.COMPLETED);
            order.setConfirmDate(LocalDateTime.now());
            order.setConfirmBy(confirmUserId);
            for (Map<String,Object> c:consignmentList) {
                Consignment consignment = new Consignment();
                int quantityInOrderDetail = (int) c.get("quantity");
                BigInteger consignmentIdInOrderDetail = (BigInteger) c.get("consignmentId");

                Long id = consignmentIdInOrderDetail.longValue();
                consignment = consignmentRepository.getById(id);
                Integer quantityInConsignment = consignment.getQuantity();

                if(quantityInOrderDetail>quantityInConsignment){
                    messageResponse.setMessage("Số lượng hàng trong kho không đủ để xuất kho !");
                    messageResponse.setStatus(409);
                    return  ResponseEntity
                            .badRequest()
                            .body(messageResponse);
                }

                int  quantityFinal = quantityInConsignment - quantityInOrderDetail;
                consignment.setQuantity(quantityFinal);
                consignmentRepository.save(consignment);
            }
            order.setStatus(status);
            messageResponse.setMessage("Xác nhận phiếu xuất kho thành công !");
            messageResponse.setStatus(Constant.SUCCESS);
            return new ResponseEntity<>(messageResponse, HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            messageResponse.setStatus(Constant.FAIL);
            messageResponse.setMessage("Xác nhận phiếu xuất kho thất bại !");
            return ResponseEntity
                    .badRequest()
                    .body(messageResponse);
        }
    }

    @Override
    public ResponseEntity<?> cancel(Long orderId) {
        MessageResponse messageResponse = new MessageResponse();
        Order order = exportOrderRepository.findById(orderId).orElseThrow(() -> new RuntimeException("Lỗi: Đơn hàng không tồn tại !"));
        if(order.getStatus().getId()==2 ||order.getStatus().getId()==3){
            messageResponse.setMessage("Đơn hàng đã được hủy hoặc xác nhận!");
            messageResponse.setStatus(Constant.FAIL);
            return ResponseEntity
                    .badRequest()
                    .body(messageResponse);
        }
        Status status = new Status();
        status.setId(Constant.CANCEL);
        order.setStatus(status);
        exportOrderRepository.save(order);
        return new ResponseEntity<>(messageResponse, HttpStatus.OK);
    }

}
