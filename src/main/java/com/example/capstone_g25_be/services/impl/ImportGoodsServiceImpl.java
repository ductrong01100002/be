package com.example.capstone_g25_be.services.impl;

import com.example.capstone_g25_be.entities.*;
import com.example.capstone_g25_be.model.request.ConsignmentRequest;
import com.example.capstone_g25_be.model.request.ImportGoodsRequest;
import com.example.capstone_g25_be.model.request.UpdateImportGoodsRequest;
import com.example.capstone_g25_be.model.response.ListProductImport;
import com.example.capstone_g25_be.model.response.MessageResponse;
import com.example.capstone_g25_be.repository.*;
import com.example.capstone_g25_be.security.services.UserDetailsImpl;
import com.example.capstone_g25_be.services.ImportGoodsService;
import com.example.capstone_g25_be.utils.Constant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

@Service
@Slf4j
public class ImportGoodsServiceImpl implements ImportGoodsService {
    private final ConsignmentRepository consignmentRepository;
    private final WarehouseRepository inventoryRepository;
    private final ProductRepository productRepository;
    private final ImportOrderRepository orderRepository;
    private final OrderTypeRepository orderTypeRepository;
    private final UserRepository userRepository;
    private final ManufacturerRepository manufactorRepository;
    private final OrderDetailRepository orderDetailRepository;
    private final static int DEFAULT_PAGE = 0;
    private final static int DEFAULT_SIZE = 10;

    public ImportGoodsServiceImpl(ConsignmentRepository consignmentRepository,
                                  WarehouseRepository inventoryRepository,
                                  ProductRepository productRepository,
                                  ImportOrderRepository orderRepository, OrderTypeRepository orderTypeRepository, UserRepository userRepository, ManufacturerRepository manufactorRepository, OrderDetailRepository orderDetailRepository) {
        this.consignmentRepository = consignmentRepository;
        this.inventoryRepository = inventoryRepository;
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
        this.orderTypeRepository = orderTypeRepository;
        this.userRepository = userRepository;
        this.manufactorRepository = manufactorRepository;
        this.orderDetailRepository = orderDetailRepository;
    }

    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    @Override
    public ResponseEntity<?> save(ImportGoodsRequest importGoodsRequest) {
        MessageResponse messageResponse = new MessageResponse();
        Warehouse warehouse;
        Order order = new Order();
        User user;
        Manufacturer manufacturer;
        Status status = new Status();
        try {
            order.setBillReferenceNumber(importGoodsRequest.getBillReferenceNumber());
            order.setCreatedDate(LocalDateTime.now());
            order.setDescription(importGoodsRequest.getDescription());
            order.setOrderType(orderTypeRepository.getById(Constant.IMPORT));
            user = userRepository.getById(importGoodsRequest.getUserId());
            order.setUser(user);
            manufacturer = manufactorRepository.getById(importGoodsRequest.getManufactorId());
            order.setManufacturer(manufacturer);
            status.setId(Constant.PENDING);
            order.setStatus(status);
            orderRepository.save(order);

            List<ConsignmentRequest> consignmentList = importGoodsRequest.getConsignmentRequests();
            //set list-product save into order
            for (ConsignmentRequest request : consignmentList) {
                if (request.getQuantity() < 1) {
                    messageResponse.setStatus(Constant.FAIL);
                    messageResponse.setMessage("Lỗi số lượng nhập vào !");
                    return new ResponseEntity<>(messageResponse, HttpStatus.BAD_REQUEST);
                }
                OrderDetail orderDetail = new OrderDetail();
                Consignment consignment = new Consignment();
                Product product = productRepository.getById(request.getProductId());
                consignment.setProduct(product);
                consignment.setExpirationDate(request.getExpirationDate());
                consignment.setQuantity(request.getQuantity());
                consignment.setUnitPrice(request.getUnitPrice());
                consignment.setDeletedAt(false);
                warehouse = inventoryRepository.getById(importGoodsRequest.getWareHouseId());
                consignment.setWarehouse(warehouse);
                orderDetail.setConsignment(consignment);
                orderDetail.setOrder(order);
                orderDetail.setQuantity(request.getQuantity());
                orderDetail.setUnitPrice(request.getUnitPrice());
                consignmentRepository.save(consignment);
                orderDetailRepository.save(orderDetail);
            }
            messageResponse.setMessage("Tạo phiếu nhập kho thành công !");
            messageResponse.setStatus(Constant.SUCCESS);
            return new ResponseEntity<>(messageResponse, HttpStatus.OK);
        } catch (Exception e) {
            messageResponse.setStatus(Constant.FAIL);
            messageResponse.setMessage("Tạo phiếu nhập kho thất bại !");
            return ResponseEntity
                    .badRequest()
                    .body(messageResponse);
        }
    }

    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    @Override
    public ResponseEntity<?> update(UpdateImportGoodsRequest updateImportGoodsRequest) {
        MessageResponse messageResponse = new MessageResponse();
        if (updateImportGoodsRequest.getWareHouseId() == null) {
            messageResponse.setMessage("Nhà kho không thể để trống !");
            messageResponse.setStatus(Constant.FAIL);
            return new ResponseEntity<>(messageResponse, HttpStatus.BAD_REQUEST);
        }
        try {
            Order order = orderRepository.getById(updateImportGoodsRequest.getOrderId());
            User user;
            Manufacturer manufactor;
            Status status = new Status();
            Warehouse warehouse;

            List<ConsignmentRequest> consignmentList = updateImportGoodsRequest.getConsignmentRequests();
            for (ConsignmentRequest request : consignmentList) {
                if(request.getExpirationDate()!=null){
                    if (request.getExpirationDate().isBefore(LocalDateTime.now())) {
                        messageResponse.setMessage("Hạn lưu kho phải lớn hơn ngày hiện tại !");
                        messageResponse.setStatus(Constant.FAIL);
                        return new ResponseEntity<>(messageResponse, HttpStatus.BAD_REQUEST);
                    }
                }
                Consignment consignment = consignmentRepository.getById(request.getConsignmentId());
                consignment.setExpirationDate(request.getExpirationDate());
                Product product = productRepository.getById(request.getProductId());
                consignment.setProduct(product);
                consignment.setExpirationDate(request.getExpirationDate());
                consignment.setQuantity(request.getQuantity());
                consignment.setUnitPrice(request.getUnitPrice());
                warehouse = inventoryRepository.getById(updateImportGoodsRequest.getWareHouseId());
                consignment.setWarehouse(warehouse);
                consignmentRepository.save(consignment);
            }
            order.setBillReferenceNumber(updateImportGoodsRequest.getBillReferenceNumber());
            order.setUpdateDate(LocalDateTime.now());
            order.setDescription(updateImportGoodsRequest.getDescription());
            order.setOrderType(orderTypeRepository.getById(Constant.IMPORT));
            user = userRepository.getById(updateImportGoodsRequest.getUserId());// user ko cho update
            order.setUser(user);
            manufactor = manufactorRepository.getById(updateImportGoodsRequest.getManufactorId());
            order.setManufacturer(manufactor);
            status.setId(Constant.PENDING);
            order.setStatus(status);
            orderRepository.save(order);
            messageResponse.setMessage("Cập nhật phiếu nhập kho thành công !");
            messageResponse.setStatus(Constant.SUCCESS);
            return new ResponseEntity<>(messageResponse, HttpStatus.OK);
        } catch (Exception e) {
            messageResponse.setMessage("Cập nhật phiếu nhập kho thất bại !");
            messageResponse.setStatus(Constant.FAIL);
            return ResponseEntity
                    .badRequest()
                    .body(messageResponse);
        }
    }

    // luwu y local DAte khi deploy thi se lay local date cuar nguoi ta ?
    @Override
    public ResponseEntity<?> confirm(Long orderId, Long confirmUserId) {
        MessageResponse messageResponse = new MessageResponse();
        if (confirmUserId == null || confirmUserId.equals("")) {
            messageResponse.setMessage("Người xác nhận không thể bỏ trống !");
            messageResponse.setStatus(Constant.FAIL);
            return new ResponseEntity<>(messageResponse, HttpStatus.BAD_REQUEST);
        }
        if (orderId == null || orderId.equals("")) {
            messageResponse.setMessage("Đơn hàng không tồn tại !");
            messageResponse.setStatus(Constant.FAIL);
            return new ResponseEntity<>(messageResponse, HttpStatus.BAD_REQUEST);
        }
        try {
            Order order = orderRepository.getById(orderId);
            if(order.getStatus().getId()==2 ||order.getStatus().getId()==3){
                messageResponse.setMessage("Đơn hàng đã được hủy hoặc xác nhận!");
                messageResponse.setStatus(Constant.FAIL);
                return ResponseEntity
                        .badRequest()
                        .body(messageResponse);
            }
            Status status = new Status();
            status.setId(Constant.COMPLETED);

            order.setConfirmBy(confirmUserId);
            order.setConfirmDate(LocalDateTime.now());

            List<OrderDetail> listOrderDetail = orderDetailRepository.findOrderDetailByOrderId(orderId);
            List<Long> listConsignmentId = new ArrayList<>();
            for (OrderDetail od : listOrderDetail) {
                listConsignmentId.add(od.getConsignment().getId());
            }
            List<Consignment> listConsignment = new ArrayList<>();
            for (Long consigmentId : listConsignmentId
            ) {
                listConsignment.add(consignmentRepository.findConsignmentById(consigmentId));
            }
            List<ListProductImport> listProductVo = new ArrayList<>();

            Set<Long> listProductId = new HashSet<>();
            for (Consignment c : listConsignment) {
                ListProductImport productVo = new ListProductImport();

                productVo.setId(c.getProduct().getId());
                productVo.setNewQuantity(consignmentRepository.countQuantityWithConsignmentNotImport(c.getId()).getQuantity());
                productVo.setOldQuantity(consignmentRepository.countQuantity(c.getProduct().getId()));

                listProductVo.add(productVo);
                listProductId.add(c.getProduct().getId());
            }

            for (ListProductImport pVo : listProductVo) {
                Product p = productRepository.findProductById(pVo.getId());
                for (Consignment c : listConsignment) {
                    if(p.getId().equals(c.getProduct().getId())){
                        double newUnitPrice = c.getUnitPrice();
                        if (pVo.getOldQuantity() != null) {
                            newUnitPrice = ((p.getUnitPrice() * pVo.getOldQuantity()) + (c.getUnitPrice() * pVo.getNewQuantity())) / (pVo.getNewQuantity() + pVo.getOldQuantity());
                        }
                        p.setUnitPrice(Math.round(newUnitPrice));
                        c.setImportDate(LocalDateTime.now());
                        consignmentRepository.save(c);
                    }
                }
                productRepository.save(p);
            }
            order.setStatus(status);
            orderRepository.save(order);
            messageResponse.setMessage("Xác nhận đơn hàng thành công !");
            messageResponse.setStatus(Constant.SUCCESS);
            return new ResponseEntity<>(messageResponse, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            messageResponse.setMessage("Xác nhận đơn hàng thất bại !");
            messageResponse.setStatus(Constant.FAIL);
            return ResponseEntity
                    .badRequest()
                    .body(messageResponse);
        }

    }

    @Override
    public ResponseEntity<?> cancel(Long orderId) {
        MessageResponse messageResponse = new MessageResponse();
        Order order = orderRepository.findById(orderId).orElseThrow(() -> new RuntimeException("Lỗi: Đơn hàng không tồn tại !"));
        if(order.getStatus().getId()==2 ||order.getStatus().getId()==3){
            messageResponse.setMessage("Đơn hàng đã được hủy hoặc xác nhận !");
            messageResponse.setStatus(Constant.FAIL);
            return ResponseEntity
                    .badRequest()
                    .body(messageResponse);
        }
        Status status = new Status();
        status.setId(Constant.CANCEL);
        order.setStatus(status);
        orderRepository.save(order);
        return new ResponseEntity<>(messageResponse, HttpStatus.OK);
    }

    @Override
    public ServiceResult<Map<String, Object>> getListImportGoods(Long userId, String billReferenceNumber, Date startDate, Date endDate, Integer status, Integer pageIndex, Integer pageSize) {
        ServiceResult<Map<String, Object>> mapServiceResult = new ServiceResult<>();
        Map<String, Object> output = new HashMap<>();
        Pageable pagable = PageRequest.of(pageIndex, pageSize,
                Sort.by("createDate").descending());

        try {
            List<Map<String, Object>> orderList = orderRepository.getListImportGoodsBySearchData(userId, billReferenceNumber, startDate, endDate, status, pagable);
            BigInteger totalRecord = BigInteger.valueOf(0);
            if (!orderList.isEmpty()) {
                totalRecord = (BigInteger) orderList.get(0).get("totalRecord");
            }
            output.put("orderList", orderList);
            output.put("pageIndex", pageIndex);
            output.put("pageSize", pageSize);
            output.put("totalRecord", totalRecord);
            mapServiceResult.setData(output);
            mapServiceResult.setMessage("success");
            mapServiceResult.setStatus(HttpStatus.OK);
        } catch (Exception e) {
            mapServiceResult.setMessage("fail");
            mapServiceResult.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            e.printStackTrace();
            return mapServiceResult;
        }
        return mapServiceResult;
    }

    @Override
    public ServiceResult<Map<String, Object>> getInforDetailImportGoods(Long orderId) {
        ServiceResult<Map<String, Object>> mapServiceResult = new ServiceResult<>();
        Map<String, Object> output = new HashMap<>();

        try {
            Map<String, Object> inforDetail = orderRepository.getOrderDetail(orderId);
            output.put("inforDetail", inforDetail);
            mapServiceResult.setData(output);
            mapServiceResult.setMessage("success");
            mapServiceResult.setStatus(HttpStatus.OK);
        } catch (Exception e) {
            mapServiceResult.setMessage("fail");
            mapServiceResult.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            e.printStackTrace();
            return mapServiceResult;
        }
        return mapServiceResult;
    }

    @Override
    public ServiceResult<Map<String, Object>> getlistProductOfImportGoods(Long orderId, Integer pageIndex, Integer pageSize) {
        pageIndex = pageIndex == null ? DEFAULT_PAGE : pageIndex;
        pageSize = pageSize == null ? DEFAULT_SIZE : pageSize;
        Pageable pagable = PageRequest.of(pageIndex, pageSize);
        ServiceResult<Map<String, Object>> mapServiceResult = new ServiceResult<>();
        Map<String, Object> output = new HashMap<>();

        try {
            List<Map<String, Object>> listProduct = orderRepository.getDetailImportGoods2(orderId, pagable);
            BigInteger totalRecord = BigInteger.valueOf(0);
            if (!listProduct.isEmpty()) {
                totalRecord = (BigInteger) listProduct.get(0).get("totalRecord");
            }
            output.put("listProduct", listProduct);
            output.put("pageIndex", pageIndex);
            output.put("pageSize", pageSize);
            output.put("totalRecord", totalRecord);
            mapServiceResult.setData(output);
            mapServiceResult.setMessage("success");
            mapServiceResult.setStatus(HttpStatus.OK);
        } catch (Exception e) {
            mapServiceResult.setMessage("fail");
            mapServiceResult.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            e.printStackTrace();
            return mapServiceResult;
        }
        return mapServiceResult;
    }
}
