package com.example.capstone_g25_be.services.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.example.capstone_g25_be.entities.Consignment;
import com.example.capstone_g25_be.entities.InventoryCheckingHistory;
import com.example.capstone_g25_be.entities.InventoryCheckingHistoryDetail;
import com.example.capstone_g25_be.entities.Product;
import com.example.capstone_g25_be.model.request.InventoryCheckingHistoryDetailRequest;
import com.example.capstone_g25_be.model.request.InventoryCheckingHistoryRequest;
import com.example.capstone_g25_be.model.response.InventoryCheckingHistoryDetailResponse;
import com.example.capstone_g25_be.model.response.ListInventoryCheckingHistoryResponse;
import com.example.capstone_g25_be.model.response.ResponseVo;
import com.example.capstone_g25_be.repository.ConsignmentRepository;
import com.example.capstone_g25_be.repository.InventoryCheckingHistoryDetailRepository;
import com.example.capstone_g25_be.repository.InventoryCheckingHistoryRepository;
import com.example.capstone_g25_be.repository.ProductRepository;
import com.example.capstone_g25_be.services.InventoryCheckingHistoryService;
import com.example.capstone_g25_be.utils.InventoryCheckingHistoryConvertUtil;

@Service
public class InventoryCheckingHistoryServiceImpl implements InventoryCheckingHistoryService {

	@Autowired
	InventoryCheckingHistoryConvertUtil inventoryCheckingHistoryConvertUtil;

	@Autowired
	InventoryCheckingHistoryRepository inventoryCheckingHistoryRepository;

	@Autowired
	InventoryCheckingHistoryDetailRepository inventoryCheckingHistoryDetailRepository;

	@Autowired
	ConsignmentRepository consignmentRepository;

	@Autowired
	ProductRepository productRepository;

	@Override
	public ResponseEntity<?> addInventoryCheckingHistory(
			InventoryCheckingHistoryRequest inventoryCheckingHistoryRequest) {
		InventoryCheckingHistory inventoryCheckingHistory = inventoryCheckingHistoryConvertUtil
				.convertInventoryCheckingHistory(inventoryCheckingHistoryRequest);
		inventoryCheckingHistoryRepository.save(inventoryCheckingHistory);
		List<InventoryCheckingHistoryDetailRequest> listDetailRequest = inventoryCheckingHistoryRequest
				.getListCheckingHistoryDetailRequest();

		for (InventoryCheckingHistoryDetailRequest inventoryCheckingHistoryDetailRequest : listDetailRequest) {
			Consignment consignment = consignmentRepository
					.findConsignmentById(inventoryCheckingHistoryDetailRequest.getConsignmentId());
			consignment.setQuantity(inventoryCheckingHistoryDetailRequest.getRealityQuantity());
			InventoryCheckingHistoryDetail inventoryCheckingHistoryDetail = new InventoryCheckingHistoryDetail();
			inventoryCheckingHistoryDetail.setConsignment(consignment);
			inventoryCheckingHistoryDetail
					.setInstockQuantity(inventoryCheckingHistoryDetailRequest.getInstockQuantity());
			inventoryCheckingHistoryDetail
					.setRealityQuantity(inventoryCheckingHistoryDetailRequest.getRealityQuantity());
			inventoryCheckingHistoryDetail.setDifferentAmout(inventoryCheckingHistoryDetailRequest.getDifferentAmout());
			inventoryCheckingHistoryDetail.setInventoryCheckingHistory(inventoryCheckingHistory);
			inventoryCheckingHistoryDetailRepository.save(inventoryCheckingHistoryDetail);
		}
		ResponseVo responseVo = new ResponseVo();
		responseVo.setMessage("Tạo phiếu kiểm kho thành công !!");
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> findByIdAndFindListDetail(Long inventoryCheckingHistoryId) {
		ResponseVo responseVo = new ResponseVo();
		if (!ObjectUtils.isEmpty(inventoryCheckingHistoryId)) {
			InventoryCheckingHistory inventoryCheckingHistory = inventoryCheckingHistoryRepository
					.findInventoryCheckingHistoryById(inventoryCheckingHistoryId);
			Map<String, Object> map = new HashMap<>();

			if (!ObjectUtils.isEmpty(inventoryCheckingHistory)) {
				List<InventoryCheckingHistoryDetail> listDetail = inventoryCheckingHistoryDetailRepository
						.findAllByInventoryCheckingHistoryId(inventoryCheckingHistoryId);

				List<Long> listConsignmentId = new ArrayList<>();
				for (InventoryCheckingHistoryDetail inventoryCheckingHistoryDetail : listDetail) {
					listConsignmentId.add(inventoryCheckingHistoryDetail.getConsignment().getId());
				}
				List<Consignment> listConsignment = consignmentRepository.findAllConsignmentByListId(listConsignmentId);

				Set<Long> setProductId = new HashSet<>();
				for (Consignment consignment : listConsignment) {
					setProductId.add(consignment.getProduct().getId());
				}
				List<Product> listProduct = productRepository.findListAllByConsignmentList(setProductId);

				if (listDetail.isEmpty()) {
					responseVo.setMessage("Không tìm thấy chi tiết đơn kiểm kho");
					responseVo.setData(map);
				}
				map.put("inventoryCheckingHistoryDetail", InventoryCheckingHistoryDetailResponse
						.createSuccessData(inventoryCheckingHistory, listDetail, listProduct, listConsignment));
				responseVo.setData(map);
				return new ResponseEntity<>(responseVo, HttpStatus.OK);
			}
			responseVo.setMessage("Không tìm thấy đơn kiểm kho");
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
	}

	@Override
	public ResponseEntity<?> findAll(Integer pageIndex, Integer pageSize, Long wareHouseId, Long userId,
			String startDate, String endDate, String orderBy, String order) {
		String orderData = "createDate";
		if (orderBy != null) {
			orderData = orderBy;
		}
		Sort sort = Sort.by(orderData).descending();
		if (order.equalsIgnoreCase("asc")) {
			sort = Sort.by(orderData).ascending();
		}
		Pageable pageable = PageRequest.of(pageIndex - 1, pageSize, sort);
		if (endDate != null) {
			LocalDate endDateSearch = LocalDate.parse(endDate);
			endDate = endDateSearch.plusDays(1).toString();
		}
		Page<InventoryCheckingHistory> pageInventoryCheckingHistory = inventoryCheckingHistoryRepository
				.findAllInventoryCheckingHistory(wareHouseId, userId, startDate, endDate, pageable);

		ResponseVo responseVo = new ResponseVo();
		Map<String, Object> map = new HashMap<>();
		if (pageInventoryCheckingHistory.isEmpty()) {
			map.put("listInventoryCheckingHistory", pageInventoryCheckingHistory.getContent());
			map.put("totalRecord", 0);
			responseVo.setMessage("Không tìm thấy phiếu kiểm kho");
			responseVo.setData(map);
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}

		map.put("listInventoryCheckingHistory",
				ListInventoryCheckingHistoryResponse.createSuccessData(pageInventoryCheckingHistory.getContent()));
		map.put("currentPage", pageIndex);
		map.put("totalRecord", pageInventoryCheckingHistory.getTotalElements());
		map.put("pageSize", pageInventoryCheckingHistory.getSize());
		map.put("totalPage", pageInventoryCheckingHistory.getTotalPages());
		responseVo.setData(map);
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

}
