package com.example.capstone_g25_be.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.example.capstone_g25_be.entities.Address;
import com.example.capstone_g25_be.entities.District;
import com.example.capstone_g25_be.entities.Manufacturer;
import com.example.capstone_g25_be.entities.Product;
import com.example.capstone_g25_be.entities.Province;
import com.example.capstone_g25_be.entities.Ward;
import com.example.capstone_g25_be.model.request.ManufacturerRequest;
import com.example.capstone_g25_be.model.response.ListManufacturerResponse;
import com.example.capstone_g25_be.model.response.ListProductVo;
import com.example.capstone_g25_be.model.response.ManufacturerDetailReponse;
import com.example.capstone_g25_be.model.response.ResponseVo;
import com.example.capstone_g25_be.repository.AddressRepository;
import com.example.capstone_g25_be.repository.ConsignmentRepository;
import com.example.capstone_g25_be.repository.DistrictRepository;
import com.example.capstone_g25_be.repository.ManufacturerRepository;
import com.example.capstone_g25_be.repository.ProductRepository;
import com.example.capstone_g25_be.repository.ProvinceRepository;
import com.example.capstone_g25_be.repository.WardRepository;
import com.example.capstone_g25_be.services.ManufacturerService;
import com.example.capstone_g25_be.utils.AddressConvertUtil;
import com.example.capstone_g25_be.utils.ManufacturerConvertUtil;

@Service
public class ManufacturerServiceImpl implements ManufacturerService {
	
	@Autowired
	ManufacturerRepository manufacturerRepository;
	
	@Autowired
	AddressRepository addressRepository;
	
	@Autowired
	ManufacturerConvertUtil manufactorConvertUtil;
	
	@Autowired
	AddressConvertUtil addressConvertUtil;
		
	@Autowired
	ProvinceRepository provinceRepository;
	
	@Autowired
	DistrictRepository districtRepository;
	
	@Autowired
	WardRepository wardRepository;
	
	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	ConsignmentRepository consignmentRepository;

	@Override
	public ResponseEntity<?> findAll(int pageIndex, int pageSize,
			String manufactorName, String manufactorEmail) {
		Pageable pageable = PageRequest.of(pageIndex - 1, pageSize);
		String dataSearch = "";
		Page<Manufacturer> manufactorPage = null;
		if(!ObjectUtils.isEmpty(manufactorName)) {
			dataSearch = manufactorName;
			manufactorPage =
					manufacturerRepository.findBySearch(dataSearch.trim(), pageable);
		}else {
			manufactorPage = manufacturerRepository.findAllByDeletedAt(false,pageable);
		}
		Map<String, Object> mapDataSearch = new HashMap<>();
		mapDataSearch.put("manufactorName", manufactorName);
		mapDataSearch.put("manufactoremail", manufactorEmail);
		ResponseVo responseVo = new ResponseVo();
		Map<String, Object> map = new HashMap<>();
		if (manufactorPage.isEmpty()) {
			map.put("manufacturer", manufactorPage.getContent());
			map.put("totalRecord", 0);
			responseVo.setMessage("Không tìm thấy List Manufacturer");
			responseVo.setData(map);
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		map.put("manufacturer", ListManufacturerResponse.createSuccessData(manufactorPage.getContent()));
		map.put("searchData", mapDataSearch);
		map.put("currentPage", pageIndex);
		map.put("totalRecord", manufactorPage.getTotalElements());
		map.put("pageSize", manufactorPage.getSize());
		map.put("totalPage", manufactorPage.getTotalPages());
		responseVo.setData(map);
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> addManufacturer(ManufacturerRequest manufacturerRequest) {
		Province province = provinceRepository.findProvinceById(manufacturerRequest.getProvinceId());
		District district = districtRepository.findDistrictById(manufacturerRequest.getDistrictId());
		Ward ward = wardRepository.findWardById(manufacturerRequest.getWardId());
		Address address = new Address();
		address.setProvince(province);
		address.setDistrict(district);
		address.setWard(ward);
		address.setDetail(manufacturerRequest.getAddressDetail());
		addressRepository.save(address);
		Manufacturer manufactor = manufactorConvertUtil.convertManufactor(manufacturerRequest);
		manufactor.setAddress(address);
		manufactor.setDeletedAt(false);
		manufacturerRepository.save(manufactor);
		ResponseVo responseVo = new ResponseVo();
		responseVo.setMessage("Tạo thành công !!");
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> findById(Long manufactorId,int pageIndex, int pageSize) {
		ResponseVo responseVo = new ResponseVo();
		Pageable pageable = PageRequest.of(pageIndex - 1, pageSize);
		if (!ObjectUtils.isEmpty(manufactorId)) {
			Manufacturer manufactor = manufacturerRepository.findManufactorById(manufactorId);
			Page<Product> productPage = productRepository.findAllByManufactureId(manufactorId, pageable);
			List<ListProductVo> listProductVo = new ArrayList<>();

			for (Product product : productPage) {
				ListProductVo productVo = new ListProductVo();
				productVo.setId(product.getId());
				productVo.setQuantity(consignmentRepository.countQuantity(product.getId()));
				listProductVo.add(productVo);
			}
			Map<String, Object> map = new HashMap<>();
			if (!ObjectUtils.isEmpty(manufactor)) {
				map.put("manufactor",
						ManufacturerDetailReponse.createSuccessData(manufactor, productPage.getContent(), listProductVo));
				map.put("currentPage", pageIndex);
				map.put("totalRecord", productPage.getTotalElements());
				map.put("pageSize", productPage.getSize());
				map.put("totalPage", productPage.getTotalPages());
				if (productPage.isEmpty()) {
					map.put("product", productPage.getContent());
					map.put("totalRecord", 0);
					responseVo.setMessage("Không tìm thấy List Product");
					responseVo.setData(map);
					return new ResponseEntity<>(responseVo, HttpStatus.OK);
				}
				responseVo.setData(map);
				return new ResponseEntity<>(responseVo, HttpStatus.OK);
			}
			responseVo.setMessage("Không tìm thấy Manufactor");
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
	}

	@Override
	public ResponseEntity<?> findAllNotPaging() {
		List<Manufacturer> listManufactor = manufacturerRepository.findAllNotPaging();
		ResponseVo responseVo = new ResponseVo();
		Map<String, Object> map = new HashMap<>();
		if (listManufactor.size() == 0) {
			map.put("manufacturer", null);
			map.put("totalRecord", 0);
			responseVo.setMessage("Không tìm thấy List Manufacturer");
			responseVo.setData(map);
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		map.put("manufacturer", ListManufacturerResponse.createSuccessData(listManufactor));
		responseVo.setData(map);
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}



}
