package com.example.capstone_g25_be.services.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import org.apache.http.entity.ContentType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;

import com.example.capstone_g25_be.bucket.BucketName;
import com.example.capstone_g25_be.entities.Category;
import com.example.capstone_g25_be.entities.Consignment;
import com.example.capstone_g25_be.entities.Manufacturer;
import com.example.capstone_g25_be.entities.Product;
import com.example.capstone_g25_be.entities.SubCategory;
import com.example.capstone_g25_be.fileStore.FileStore;
import com.example.capstone_g25_be.model.request.ProductRequest;
import com.example.capstone_g25_be.model.response.Consignment1;
import com.example.capstone_g25_be.model.response.ListConsignmentResponse;
import com.example.capstone_g25_be.model.response.ListProductByWareHouse;
import com.example.capstone_g25_be.model.response.ListProductResponse;
import com.example.capstone_g25_be.model.response.ListProductVo;
import com.example.capstone_g25_be.model.response.Product1;
import com.example.capstone_g25_be.model.response.ProductAndListConsignmentResponse;
import com.example.capstone_g25_be.model.response.ProductDetailResponse;
import com.example.capstone_g25_be.model.response.ProductImportExcelDto;
import com.example.capstone_g25_be.model.response.ResponseVo;
import com.example.capstone_g25_be.profile.UserProfileService;
import com.example.capstone_g25_be.repository.CategoryRepository;
import com.example.capstone_g25_be.repository.ConsignmentRepository;
import com.example.capstone_g25_be.repository.ManufacturerRepository;
import com.example.capstone_g25_be.repository.ProductRepository;
import com.example.capstone_g25_be.repository.SubCategoryRepository;
import com.example.capstone_g25_be.services.ProductService;
import com.example.capstone_g25_be.utils.ProductConvertUtil;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ConsignmentRepository consignmentRepository;

	@Autowired
	private ManufacturerRepository manufacturerRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private SubCategoryRepository subCategoryRepository;

	@Autowired
	private ProductConvertUtil productConvertUtil;

	@Autowired
	UserProfileService userProfileService;

	@Autowired
	FileStore fileStore;

	@Override
	public ResponseEntity<?> findAll(int pageIndex, int pageSize, String productName, String productCode,
			Long categoryId, Long manufactorId) {
		Pageable pageable = PageRequest.of(pageIndex - 1, pageSize);

		String nameSearch = "";
		Page<Product> productPage = null;
		if (!ObjectUtils.isEmpty(productName)) {
			nameSearch = productName;
			productPage = productRepository.findBySearch(nameSearch.trim(), categoryId, manufactorId, pageable);
		} else {
			productPage = productRepository.findAllBySearch(categoryId, manufactorId, pageable);
		}

		Map<String, Object> mapDataSearch = new HashMap<>();
		mapDataSearch.put("productName", productName);
		mapDataSearch.put("productCode", productCode);
		mapDataSearch.put("categoryId", categoryId);
		mapDataSearch.put("manufactorId", manufactorId);
		ResponseVo responseVo = new ResponseVo();
		Map<String, Object> map = new HashMap<>();
		if (productPage.isEmpty()) {
			map.put("product", productPage.getContent());
			map.put("totalRecord", 0);
			responseVo.setMessage("Không tìm thấy List Product");
			responseVo.setData(map);
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		List<ListProductVo> listProductVo = new ArrayList<>();

		for (Product product : productPage) {
			ListProductVo productVo = new ListProductVo();
			productVo.setId(product.getId());
			productVo.setQuantity(consignmentRepository.countQuantity(product.getId()));
			listProductVo.add(productVo);
		}
		map.put("product", ListProductResponse.createSuccessData(productPage.getContent(), listProductVo));
		map.put("searchData", mapDataSearch);
		map.put("currentPage", pageIndex);
		map.put("totalRecord", productPage.getTotalElements());
		map.put("pageSize", productPage.getSize());
		map.put("totalPage", productPage.getTotalPages());
		responseVo.setData(map);
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> findById(Long productId, int pageIndex, int pageSize) {
		ResponseVo responseVo = new ResponseVo();
		if (!ObjectUtils.isEmpty(productId)) {
			Pageable pageable = PageRequest.of(pageIndex - 1, pageSize);
			Product product = productRepository.findProductById(productId);
			Map<String, Object> map = new HashMap<>();
			ListProductVo listProductVo = new ListProductVo();
			if (!ObjectUtils.isEmpty(product)) {
				listProductVo.setQuantity(consignmentRepository.countQuantity(product.getId()));
				Page<Consignment> consignmentPage = consignmentRepository.findAllConsignment(productId, pageable);
				if (consignmentPage.isEmpty()) {
					map.put("consignment", consignmentPage.getContent());
					map.put("totalRecord", 0);
					responseVo.setMessage("Không tìm thấy List Consignment");
					responseVo.setData(map);
				}
				map.put("product", ProductDetailResponse.createSuccessData(product, listProductVo));
				map.put("consignment", ListConsignmentResponse.createSuccessData(consignmentPage.getContent()));
				map.put("totalRecord", consignmentPage.getTotalElements());
				map.put("currentPage", pageIndex);
				map.put("pageSize", consignmentPage.getSize());
				map.put("totalPage", consignmentPage.getTotalPages());
				responseVo.setData(map);
				return new ResponseEntity<>(responseVo, HttpStatus.OK);
			}
			responseVo.setMessage("Không tìm thấy Product");
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
	}

	@Override
	public ResponseEntity<?> addProduct(ProductRequest productRequest) {
		ResponseVo responseVo = new ResponseVo();
		Product productExist = productRepository.findProductByName(productRequest.getName().trim());
		if (productExist != null) {
			responseVo.setMessage("Tên sản phẩm đã bị trùng !!");
			return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
		}
		Product productExist2 = productRepository.findProductByProductCode(productRequest.getProductCode().trim());
		if (productExist2 != null) {
			responseVo.setMessage("Mã sản phẩm đã bị trùng !!");
			return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
		}
		Product product = productConvertUtil.convertProduct(productRequest);
		Category category = categoryRepository.findCategoryById(productRequest.getCategoryId());
		if (productRequest.getSubCategoryId() != null) {
			SubCategory subCategory = subCategoryRepository.findSubCategoryById(productRequest.getSubCategoryId());
			product.setSubCategory(subCategory);
		}
		Manufacturer manufacturer = manufacturerRepository.findManufactorById(productRequest.getManufactorId());
		product.setCategory(category);
		product.setManufacturer(manufacturer);
		product.setDeletedAt(false);
		productRepository.save(product);
		responseVo.setMessage("Tạo thành công !!");
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> updateProduct(ProductRequest productRequest) {
		ResponseVo responseVo = new ResponseVo();
		Product p = 
				productRepository.findProductByIdAndName(productRequest.getId(), productRequest.getName().trim());
		if (p != null) {
			responseVo.setMessage("Tên sản phẩm đã bị trùng !!");
			return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
		}
		Product p2 = 
				productRepository.findProductByIdAndProductCode(
						productRequest.getId(), productRequest.getProductCode().trim());
		if (p2 != null) {
			responseVo.setMessage("Mã sản phẩm đã bị trùng !!");
			return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
		}
		Product productOld = productRepository.findProductById(productRequest.getId());
		Product product = productConvertUtil.convertProduct(productRequest);
		Category category = categoryRepository.findCategoryById(productRequest.getCategoryId());
		if (productRequest.getSubCategoryId() != null) {
			SubCategory subCategory = subCategoryRepository.findSubCategoryById(productRequest.getSubCategoryId());
			product.setSubCategory(subCategory);
		}
		Manufacturer manufacturer = manufacturerRepository.findManufactorById(productRequest.getManufactorId());
		product.setCategory(category);
		product.setManufacturer(manufacturer);
		product.setDeletedAt(false);
		product.setImage(productOld.getImage());
		product.setUnitPrice(productOld.getUnitPrice());
		productRepository.save(product);
		responseVo.setMessage("Cập nhập thành công !!");
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> addImageProduct(MultipartFile file) {
		if (file.isEmpty()) {
			throw new IllegalStateException("Cannot upload empty file [ " + file.getSize() + " ]");
		}
		if (!Arrays.asList(ContentType.IMAGE_JPEG.getMimeType(), ContentType.IMAGE_PNG.getMimeType(),
				ContentType.IMAGE_GIF.getMimeType()).contains(file.getContentType())) {
			throw new IllegalStateException("File must be an image");
		}
		Product product = productRepository.findProductLatest().get(0);

		Map<String, String> metadata = new HashMap<>();
		metadata.put("Content-Type", file.getContentType());
		metadata.put("Content-Length", String.valueOf(file.getSize()));

		String path = String.format("%s/%s/%s", BucketName.PROFILE_IMAGE.getBucketName(), "Product", product.getId());
		String fileName = String.format("%s", file.getOriginalFilename());

		try {
			fileStore.save(path, fileName, Optional.of(metadata), file.getInputStream());
			product.setImage(fileName);
			productRepository.save(product);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		ResponseVo responseVo = new ResponseVo();
		responseVo.setMessage("Thêm ảnh thành công !!");
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> updateImageProduct(Long productIdId, MultipartFile file) {
		if (file.isEmpty()) {
			throw new IllegalStateException("Cannot upload empty file [ " + file.getSize() + " ]");
		}
		if (!Arrays.asList(ContentType.IMAGE_JPEG.getMimeType(), ContentType.IMAGE_PNG.getMimeType(),
				ContentType.IMAGE_GIF.getMimeType()).contains(file.getContentType())) {
			throw new IllegalStateException("File must be an image");
		}
		Product product = productRepository.findProductById(productIdId);

		Map<String, String> metadata = new HashMap<>();
		metadata.put("Content-Type", file.getContentType());
		metadata.put("Content-Length", String.valueOf(file.getSize()));

		String path = String.format("%s/%s/%s", BucketName.PROFILE_IMAGE.getBucketName(), "Product", product.getId());
		String fileName = String.format("%s", file.getOriginalFilename());

		try {
			fileStore.save(path, fileName, Optional.of(metadata), file.getInputStream());
			product.setImage(fileName);
			productRepository.save(product);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		ResponseVo responseVo = new ResponseVo();
		responseVo.setMessage("Cập nhập ảnh thành công !!");
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

	@Override
	public byte[] downloadProductImage(Long productId) {
		Product product = productRepository.findProductById(productId);
		String path = String.format("%s/%s/%s", BucketName.PROFILE_IMAGE.getBucketName(), "Product", product.getId());
		return fileStore.download(path, product.getImage());
	}

	@Override
	public Object importProduct(MultipartFile file) {
		ResponseVo responseVo = new ResponseVo();
		try {
			InputStream inputStream = file.getInputStream();
			Workbook workbook = new XSSFWorkbook(inputStream);
			Sheet dataTypeSheet = workbook.getSheetAt(0);
			DataFormatter df = new DataFormatter();
			Iterator<Row> iterator = dataTypeSheet.iterator();
			iterator.next();

			List<ProductImportExcelDto> dtoList = new ArrayList<>();
			while (iterator.hasNext()) {
				Row currentRow = iterator.next();
				String productName = df.formatCellValue(currentRow.getCell(0));
				String productCode = df.formatCellValue(currentRow.getCell(1));
				String unitMeasure = df.formatCellValue(currentRow.getCell(2));
				String color = df.formatCellValue(currentRow.getCell(3));
				String description = df.formatCellValue(currentRow.getCell(4));
				String wrapUnitMeasure = df.formatCellValue(currentRow.getCell(5));
				String numberOfWrapUnitMeasure = df.formatCellValue(currentRow.getCell(6));
				String category = df.formatCellValue(currentRow.getCell(7));
				String subCategory = df.formatCellValue(currentRow.getCell(8));
				String manufacture = df.formatCellValue(currentRow.getCell(9));
				ProductImportExcelDto importExcelDto = new ProductImportExcelDto(productName, productCode, unitMeasure,
						color, description, wrapUnitMeasure, numberOfWrapUnitMeasure, category, subCategory,
						manufacture);
				dtoList.add(importExcelDto);
			}
			responseVo.setData(dtoList);
			responseVo.setMessage("success");
			responseVo.setStatus(true);
			workbook.close();
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
			responseVo.setData("");
			responseVo.setMessage("fail");
			responseVo.setStatus(false);
		}
		return responseVo;
	}

	@Override
	public ResponseEntity<?> findProductByWareHouseId(Long wareHouseId) {
		List<Long> listConsignments = consignmentRepository.findAllConsignmentByWareHouseId(wareHouseId);
		Set<Long> listProductId = new HashSet<>();
		for (Long c : listConsignments) {
			listProductId.add(c);
		}

		List<Product> product = productRepository.findListAllByConsignmentList(listProductId);
		Map<String, Object> map = new HashMap<>();
		ResponseVo responseVo = new ResponseVo();
		if (!ObjectUtils.isEmpty(product)) {
			map.put("listProduct", ListProductByWareHouse.createSuccessData(product));
			responseVo.setData(map);
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		responseVo.setMessage("Không tìm thấy Product");
		return new ResponseEntity<>(responseVo, HttpStatus.OK);

	}

	@Override
	public ResponseEntity<?> findByIdAndFindListConsignment(Long productId, Long warehouseId) {
		ResponseVo responseVo = new ResponseVo();
		if (!ObjectUtils.isEmpty(productId)) {
			List<Consignment1> listConsignment = 
					consignmentRepository.findProductAllConsignment1(productId, warehouseId);
			Product1 product = productRepository.findProductById1(productId);
			Map<String, Object> map = new HashMap<>();

			ListProductVo listProductVo = new ListProductVo();
			if (!ObjectUtils.isEmpty(product)) {
				listProductVo.setQuantity(consignmentRepository.countQuantity(product.getId()));
				if (listConsignment.isEmpty()) {
					map.put("consignment", listConsignment);
					responseVo.setMessage("Không tìm thấy List Consignment");
					responseVo.setData(map);
				}
				map.put("product", ProductAndListConsignmentResponse.createSuccessData(product, listConsignment));
				responseVo.setData(map);
				return new ResponseEntity<>(responseVo, HttpStatus.OK);
			}
			responseVo.setMessage("Không tìm thấy Product");
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
	}

	@Override
	public ResponseEntity<?> findProductBymanufacturerId(Long manufacturerId) {
		List<Product> listProduct = productRepository.findAllByManufactureId(manufacturerId);
		ResponseVo responseVo = new ResponseVo();
		Map<String, Object> map = new HashMap<>();
		if (listProduct.size() == 0) {
			map.put("product", null);
			map.put("totalRecord", 0);
			responseVo.setMessage("Không tìm thấy List Manufacturer");
			responseVo.setData(map);
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		map.put("product", ListProductResponse.createSuccessData2(listProduct));
		responseVo.setData(map);
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

}
