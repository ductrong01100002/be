package com.example.capstone_g25_be.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.capstone_g25_be.entities.Province;
import com.example.capstone_g25_be.model.response.ListProvinceResponse;
import com.example.capstone_g25_be.model.response.ResponseVo;
import com.example.capstone_g25_be.repository.ProvinceRepository;
import com.example.capstone_g25_be.services.ProvinceService;

@Service
public class ProvinceServiceImpl implements ProvinceService{
	
	@Autowired
	private ProvinceRepository provinceRepository;

	@Override
	public ResponseEntity<?> findAll() {
		List<Province> listProvince = provinceRepository.findAll();
		ResponseVo responseVo = new ResponseVo();
		Map<String, Object> map = new HashMap<>();
		if (listProvince.isEmpty()) {
			map.put("message", "Không tìm thấy Province");
			responseVo.setData(map);
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		map.put("province", ListProvinceResponse.createSuccessData(listProvince));
		responseVo.setData(map);
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

}
