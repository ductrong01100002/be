package com.example.capstone_g25_be.services.impl;

import com.example.capstone_g25_be.entities.*;
import com.example.capstone_g25_be.model.request.ConsignmentExport;
import com.example.capstone_g25_be.model.request.ConsignmentReturn;
import com.example.capstone_g25_be.model.request.ReturnOrderRequest;
import com.example.capstone_g25_be.model.response.DetailExportReponse;
import com.example.capstone_g25_be.model.response.DetailReturnResponse;
import com.example.capstone_g25_be.model.response.MessageResponse;
import com.example.capstone_g25_be.repository.*;
import com.example.capstone_g25_be.services.ReturnOrderService;
import com.example.capstone_g25_be.utils.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class ReturnOrderServiceImpl implements ReturnOrderService {
    private final static int DEFAULT_PAGE = 0;
    private final static int DEFAULT_SIZE = 10;
    @Autowired
    private ReturnOrderRepository returnOrderRepository;

    @Autowired
    private ExportOrderRepository exportOrderRepository;

    @Autowired
    private ImportOrderRepository orderRepository;

    @Autowired
    private ConsignmentRepository consignmentRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private  ProductRepository productRepository;

    @Autowired
    private OrderTypeRepository orderTypeRepository;
    @Autowired
    private OrderDetailRepository orderDetailRepository;

    @Override
    public ServiceResult<Map<String, Object>> getListReturnOrder(Long userId, String billReferenceNumber, Date startDate,
                                                                 Date endDate, Integer pageIndex, Integer pageSize) {

        ServiceResult<Map<String, Object>> mapServiceResult = new ServiceResult<>();
        Map<String, Object> output = new HashMap<>();
        Pageable pagable = PageRequest.of(pageIndex, pageSize,
                Sort.by("createDate").descending());

        try {
            List<Map<String, Object>> returnOrderList = returnOrderRepository.getListReturnOrderBySearchData(userId, billReferenceNumber, startDate, endDate, pagable);
            BigInteger totalRecord = BigInteger.valueOf(0);
            if(!returnOrderList.isEmpty()){
                totalRecord = (BigInteger) returnOrderList.get(0).get("totalRecord");
           }
            output.put("returnOrderList", returnOrderList);
            output.put("pageIndex", pageIndex);
            output.put("pageSize", pageSize);
            output.put("totalRecord", totalRecord);

            mapServiceResult.setData(output);
            mapServiceResult.setMessage("success");
            mapServiceResult.setStatus(HttpStatus.OK);
        } catch (Exception e) {
            mapServiceResult.setMessage("fail");
            mapServiceResult.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            e.printStackTrace();
        }
        return mapServiceResult;
    }

    @Override
    public ServiceResult<Map<String, Object>> getDetailReturnOrder(Long orderId, Integer pageIndex, Integer pageSize) {
        pageIndex = pageIndex == null ? DEFAULT_PAGE : pageIndex;
        pageSize = pageSize == null ? DEFAULT_SIZE : pageSize;
        Pageable pagable = PageRequest.of(pageIndex, pageSize);
        ServiceResult<Map<String, Object>> mapServiceResult = new ServiceResult<>();
        Map<String, Object> output = new HashMap<>();
        try {
            Map<String, Object> inforExportDetail = returnOrderRepository.getOrderDetail(orderId);
            List<Map<String, Object>> productList = returnOrderRepository.getDetailExportGoods2(orderId, pagable);
            List<Map<String, Object>> consignmentList = returnOrderRepository.getListConsignment(orderId);
            List<DetailReturnResponse> response = DetailReturnResponse.createSuccessData(productList, consignmentList);
            List<BigInteger> idsAddress = new ArrayList<>();
            for (Map<String, Object> map : consignmentList) {
                idsAddress.add((BigInteger) map.get("wareHouseId"));
            }
            List<Map<String, Object>> addressWarehouse = exportOrderRepository.getListAddressWarehouseByListId(idsAddress);
            output.put("inforExportDetail", inforExportDetail);
            output.put("addressWarehouse", addressWarehouse);
            output.put("productList", response);
            output.put("pageIndex", pageIndex);
            output.put("pageSize", pageSize);
            mapServiceResult.setData(output);
            mapServiceResult.setMessage("success");
            mapServiceResult.setStatus(HttpStatus.OK);
        } catch (Exception e) {
            mapServiceResult.setMessage("fail");
            mapServiceResult.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            e.printStackTrace();
        }
        return mapServiceResult;
    }

    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    @Override
    public ResponseEntity<?> returnOrder(Long orderId, ReturnOrderRequest returnOrderRequest) {
        MessageResponse messageResponse = new MessageResponse();
        List<Map<String, Object>> consignmentList = exportOrderRepository.getListConsignmentByOrderId(orderId);

        Order order1 = orderRepository.findById(orderId).orElseThrow(() -> new RuntimeException("Lỗi: Đơn hàng không tồn tại !"));
        try {
            Order order = new Order();
            Status status = new Status();
            if(order1.getIsReturn()!=null){
                if(order1.getIsReturn().equals(true)){
                    messageResponse.setMessage("Đơn hàng đã được trả hàng!");
                    messageResponse.setStatus(Constant.FAIL);
                    return ResponseEntity
                            .badRequest()
                            .body(messageResponse);
                }
            }
            status.setId(Constant.RETURNED);
            order.setBillReferenceNumber(returnOrderRequest.getBillReferenceNumber());
            order.setDescription(returnOrderRequest.getDescription());
            order.setUser(userRepository.getById(returnOrderRequest.getUserId()));
            order.setStatus(status);
            order.setCreatedDate(LocalDateTime.now());
            order.setOrderType(orderTypeRepository.getById(Constant.RETURN));

            for (Map<String, Object> c : consignmentList) {
                Long id = ((BigInteger) c.get("consignmentId")).longValue();

                Consignment consignment = consignmentRepository.getById(id);
                for (ConsignmentReturn returnOrderList : returnOrderRequest.getConsignmentReturns()) {
                    if (id.equals(returnOrderList.getId())) {
                        //get quantity in return order
                        int quantityInReturnOrder = returnOrderList.getQuantity();

                        //get quantity in consignment
                        Integer quantityInConsignment = consignment.getQuantity();


                        Long productId = returnOrderList.getProductId();
                        int quantity = returnOrderList.getQuantity();
                        double unitPrice = returnOrderList.getUnitPrice();

                        Product product = productRepository.findProductById(productId);
                        int quantityInOrderDetail = consignmentRepository.countQuantity(productId);
                        double unitPriceOfProduct = product.getUnitPrice();
                        double newPrice = ((quantity*unitPrice)+(quantityInOrderDetail*unitPriceOfProduct))/(quantityInOrderDetail+quantity);

                        product.setUnitPrice(newPrice);
                        productRepository.save(product);
                        int quantityFinal = quantityInConsignment + quantityInReturnOrder;
                        consignment.setQuantity(quantityFinal);
                        consignmentRepository.save(consignment);
                    }
                }
            }


            for (ConsignmentReturn request : returnOrderRequest.getConsignmentReturns()) {
                Consignment consignment = consignmentRepository.getById(request.getId());
//                Product product = productRepository.getById(request.getProductId());
                OrderDetail orderDetail = new OrderDetail();
                orderDetail.setConsignment(consignment);
                orderDetail.setOrder(order);
                orderDetail.setQuantity(request.getQuantity());
                orderDetail.setUnitPrice(request.getUnitPrice());
                orderDetailRepository.save(orderDetail);
            }
            orderRepository.save(order);
            order1.setIsReturn(true);
            orderRepository.save(order1);
            messageResponse.setMessage("Trả hàng thành công !");
            messageResponse.setStatus(Constant.SUCCESS);
            return new ResponseEntity<>(messageResponse, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            messageResponse.setStatus(Constant.FAIL);
            messageResponse.setMessage("Trả hàng thất bại !");
            return ResponseEntity
                    .badRequest()
                    .body(messageResponse);
        }
    }
}
