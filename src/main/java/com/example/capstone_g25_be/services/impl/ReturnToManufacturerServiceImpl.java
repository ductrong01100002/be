package com.example.capstone_g25_be.services.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.example.capstone_g25_be.entities.ReturnToManufacturer;
import com.example.capstone_g25_be.entities.ReturnToManufacturerDetail;
import com.example.capstone_g25_be.entities.Status;
import com.example.capstone_g25_be.entities.User;
import com.example.capstone_g25_be.entities.Warehouse;
import com.example.capstone_g25_be.model.request.ReturnToManufacturerDetailRequest;
import com.example.capstone_g25_be.model.request.ReturnToManufacturerRequest;
import com.example.capstone_g25_be.model.response.ListReturnToManufacturerResponse;
import com.example.capstone_g25_be.model.response.ResponseVo;
import com.example.capstone_g25_be.model.response.ReturnToManufacturerDetailResponse;
import com.example.capstone_g25_be.repository.ReturnToManufacturerDetailRepository;
import com.example.capstone_g25_be.repository.ReturnToManufacturerRepository;
import com.example.capstone_g25_be.repository.StatusRepository;
import com.example.capstone_g25_be.repository.UserRepository;
import com.example.capstone_g25_be.repository.WarehouseRepository;
import com.example.capstone_g25_be.services.ReturnToManufacturerService;
import com.example.capstone_g25_be.utils.ReturnToManufacturerConvertUtil;
import com.example.capstone_g25_be.utils.ReturnToManufacturerDetailConvertUtil;

@Service
public class ReturnToManufacturerServiceImpl implements ReturnToManufacturerService {

	@Autowired
	ReturnToManufacturerConvertUtil returnToManufacturerConvertUtil;

	@Autowired
	ReturnToManufacturerDetailConvertUtil returnToManufacturerDetailConvertUtil;

	@Autowired
	ReturnToManufacturerRepository returnToManufacturerRepository;

	@Autowired
	ReturnToManufacturerDetailRepository returnToManufacturerDetailRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	WarehouseRepository warehouseRepository;

	@Autowired
	StatusRepository statusRepository;

	@Override
	public ResponseEntity<?> addReturnToManufacturer(ReturnToManufacturerRequest returnToManufacturerRequest) {
		ReturnToManufacturer returnToManufacturer = returnToManufacturerConvertUtil
				.convertReturnToManufacturer(returnToManufacturerRequest);
		List<ReturnToManufacturerDetailRequest> listRequest = returnToManufacturerRequest
				.getListReturnToManufacturerDetailRequest();
		List<ReturnToManufacturerDetail> listDetail = returnToManufacturerDetailConvertUtil
				.convertReturnToManufacturerDetail(listRequest, returnToManufacturer);
		returnToManufacturerRepository.save(returnToManufacturer);
		returnToManufacturerDetailRepository.saveAll(listDetail);
		ResponseVo responseVo = new ResponseVo();
		responseVo.setMessage("Tạo phiếu trả hàng cho nhà sản xuất thành công !!");
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> findAll(Integer pageIndex, Integer pageSize, Long manufacturerId, Long userId,
			Long statusId, String startDate, String endDate) {
		Sort sort = Sort.by("status.id").ascending().and(Sort.by("createDate").descending());
		Pageable pageable = PageRequest.of(pageIndex - 1, pageSize, sort);
		if (endDate != null) {
			LocalDate endDateSearch = LocalDate.parse(endDate);
			endDate = endDateSearch.plusDays(1).toString();
		}
		Page<ReturnToManufacturer> pageReturnToManufacturer = returnToManufacturerRepository
				.findAllReturnToManufacturer(userId, statusId, manufacturerId, startDate, endDate, pageable);

		ResponseVo responseVo = new ResponseVo();
		Map<String, Object> map = new HashMap<>();
		if (pageReturnToManufacturer.isEmpty()) {
			map.put("listInventoryCheckingHistory", pageReturnToManufacturer.getContent());
			map.put("totalRecord", 0);
			responseVo.setMessage("Không tìm thấy phiếu trả hàng cho nhà sản xuất");
			responseVo.setData(map);
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}

		map.put("listReturnToManufacturer",
				ListReturnToManufacturerResponse.createSuccessData(pageReturnToManufacturer.getContent()));
		map.put("currentPage", pageIndex);
		map.put("totalRecord", pageReturnToManufacturer.getTotalElements());
		map.put("pageSize", pageReturnToManufacturer.getSize());
		map.put("totalPage", pageReturnToManufacturer.getTotalPages());
		responseVo.setData(map);
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> findByIdAndFindListDetail(Long returnToManufacturerId) {
		ResponseVo responseVo = new ResponseVo();
		if (!ObjectUtils.isEmpty(returnToManufacturerId)) {

			ReturnToManufacturer returnToManufacturer = returnToManufacturerRepository
					.findReturnToManufacturerById(returnToManufacturerId);
			if (!ObjectUtils.isEmpty(returnToManufacturer)) {
				String userConfirmedName = null;
				String fullNameConfirmed = null;
				if (returnToManufacturer.getUserConfirmedId() != null) {
					User u = userRepository.findUserById(returnToManufacturer.getUserConfirmedId());
					userConfirmedName = u.getUserName();
					fullNameConfirmed = u.getFullName();
				}

				Warehouse warehouse = warehouseRepository.findWareHouseById(returnToManufacturer.getWarehouse().getId());

				List<ReturnToManufacturerDetail> listDetail = returnToManufacturerDetailRepository
						.findAllByReturnToManufacturerId(returnToManufacturerId);

				Map<String, Object> map = new HashMap<>();

				if (listDetail.isEmpty()) {
					responseVo.setMessage("Không tìm thấy chi tiết phiếu trả hàng cho nhà sản xuất");
					responseVo.setData(map);
				}
				map.put("returnToManufacturerDetail", ReturnToManufacturerDetailResponse.createSuccessData(
						returnToManufacturer, listDetail, warehouse, userConfirmedName, fullNameConfirmed));
				responseVo.setData(map);
				return new ResponseEntity<>(responseVo, HttpStatus.OK);
			}
			responseVo.setMessage("Không tìm thấy phiếu trả hàng cho nhà sản xuất");
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
	}

	@Override
	public ResponseEntity<?> confirmReturnToManufacturer(Long returnToManufacturerId, Long userConfirmedId) {
		ResponseVo responseVo = new ResponseVo();
		if (!ObjectUtils.isEmpty(returnToManufacturerId)) {
			ReturnToManufacturer returnToManufacturer = 
					returnToManufacturerRepository.findReturnToManufacturerById(returnToManufacturerId);
			if (!ObjectUtils.isEmpty(returnToManufacturer)) {
				Status status = statusRepository.findStatusById((long) 2);
				returnToManufacturer.setStatus(status);
				returnToManufacturer.setConfirmedDate(LocalDateTime.now());
				returnToManufacturer.setReturnDate(LocalDateTime.now());
				returnToManufacturer.setUserConfirmedId(userConfirmedId);
				returnToManufacturerRepository.save(returnToManufacturer);
				responseVo.setMessage("Xác nhận phiếu trả hàng cho nhà sản xuất thành công");
				return new ResponseEntity<>(responseVo, HttpStatus.OK);
			}
			responseVo.setMessage("Không tìm thấy phiếu trả hàng cho nhà sản xuất thành công");
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
	}

	@Override
	public ResponseEntity<?> deleteReturnToManufacturer(Long returnToManufacturerId, Long userConfirmedId) {
		ResponseVo responseVo = new ResponseVo();
		if (!ObjectUtils.isEmpty(returnToManufacturerId)) {
			ReturnToManufacturer returnToManufacturer =
					returnToManufacturerRepository.findReturnToManufacturerById(returnToManufacturerId);
			if (!ObjectUtils.isEmpty(returnToManufacturer)) {
				Status status = statusRepository.findStatusById((long) 3);
				returnToManufacturer.setStatus(status);
				returnToManufacturer.setConfirmedDate(LocalDateTime.now());
				returnToManufacturer.setUserConfirmedId(userConfirmedId);
				returnToManufacturerRepository.save(returnToManufacturer);
				responseVo.setMessage("Xóa phiếu trả hàng cho nhà sản xuất thành công");
				return new ResponseEntity<>(responseVo, HttpStatus.OK);
			}
			responseVo.setMessage("Không tìm thấy phiếu trả hàng cho nhà sản xuất thành công");
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
	}

	@Override
	public ResponseEntity<?> updateReturnToManufacturer(Long returnToManufacturerId,
			ReturnToManufacturerRequest returnToManufacturerRequest) {
		ReturnToManufacturer returnToManufacturer = returnToManufacturerRepository.findReturnToManufacturerById(returnToManufacturerId);
		Warehouse w = new Warehouse();
		w.setId(returnToManufacturerRequest.getWarehouseId());
		returnToManufacturer.setWarehouse(w);
		if (returnToManufacturerRequest.getExpectedReturnDate() != null) {
			returnToManufacturer.setExpectedReturnDate(returnToManufacturerRequest.getExpectedReturnDate());
		}
		returnToManufacturer.setDescription(returnToManufacturerRequest.getDescription());
		List<ReturnToManufacturerDetailRequest> listRequest = returnToManufacturerRequest
				.getListReturnToManufacturerDetailRequest();
		List<ReturnToManufacturerDetail> listDetail = returnToManufacturerDetailConvertUtil
				.convertReturnToManufacturerDetail(listRequest, returnToManufacturer);
		returnToManufacturerRepository.save(returnToManufacturer);
		returnToManufacturerDetailRepository.saveAll(listDetail);
		ResponseVo responseVo = new ResponseVo();
		responseVo.setMessage("Chỉnh sửa phiếu trả hàng cho nhà sản xuất thành công !!");
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

}
