package com.example.capstone_g25_be.services.impl;

import com.example.capstone_g25_be.entities.*;
import com.example.capstone_g25_be.model.request.UpdateStaffRequest;
import com.example.capstone_g25_be.model.response.ListStaffResponse;
import com.example.capstone_g25_be.model.response.MessageResponse;
import com.example.capstone_g25_be.model.response.ResponseVo;
import com.example.capstone_g25_be.model.response.UserResponse;
import com.example.capstone_g25_be.repository.*;
import com.example.capstone_g25_be.services.StaffService;
import com.example.capstone_g25_be.utils.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigInteger;
import java.util.*;

@Service
public class StaffServiceImpl implements StaffService {
    private final static int DEFAULT_PAGE = 0;
    private final static int DEFAULT_SIZE = 10;
    @Autowired
    private StaffRepository staffRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private ProvinceRepository provinceRepository;

    @Autowired
    private DistrictRepository districtRepository;

    @Autowired
    private WardRepository wardRepository;

    @Override
    public Map<String, Object> getListStaff(String keyWords,String sortColumn,@RequestParam(required = false) Integer pageIndex,
                                                  @RequestParam(required = false) Integer pageSize) {
        pageIndex = pageIndex == null ? DEFAULT_PAGE : pageIndex;
        pageSize = pageSize == null ? DEFAULT_SIZE : pageSize;
        Pageable pagable = PageRequest.of(pageIndex, pageSize);
        Map<String, Object> output = new HashMap<>();
        List<Map<String, Object>>  listStaff;
        List<ListStaffResponse> responseList = new ArrayList<>();
        if(keyWords.equals("")){
            listStaff = staffRepository.getListAll(pagable);
            responseList= ListStaffResponse.createSuccessData(listStaff);
        }else{
            listStaff = staffRepository.getListStaffBySearchData(keyWords.trim(),sortColumn,pagable);
            responseList= ListStaffResponse.createSuccessData(listStaff);
        }
        BigInteger totalRecord = BigInteger.valueOf(0);
        if(!listStaff.isEmpty()){
            totalRecord = (BigInteger)listStaff.get(0).get("totalRecord");
        }

        output.put("listStaff",responseList);
        output.put("totalRecord",totalRecord);
        output.put("pageIndex",pageIndex);
        output.put("pageSize",pageSize);
        output.put("status", Constant.SUCCESS);
        return output;
    }

    @Override
    public Map<String, Object> getDetailStaff(Long id) {
        Map<String, Object> response = new HashMap<>();
        User user = userRepository.findById(id).orElseThrow(() -> new RuntimeException("Error: User is not found."));;
        Map<String, Object> output = staffRepository.getDetailStaff(id);
        response.put("data",UserResponse.createSuccessData(user,output));
        response.put("status",Constant.SUCCESS);
        return response;
    }

    @Override
    public ResponseEntity<Object> updateStaff(UpdateStaffRequest staffRequest) {
        Map<String, Object> response = new HashMap<>();
        User user = userRepository.findById(staffRequest.getId()).orElseThrow(() -> new RuntimeException("Error: User is not found."));
            user.setFullName(staffRequest.getFullName().trim());
            user.setIdentityCard(staffRequest.getIdentityCard().trim());
            user.setDateOfBirth(staffRequest.getDateOfBirth());
            user.setGender(staffRequest.getGender());
            user.setPhone(staffRequest.getPhone().trim());
            user.setEmail(staffRequest.getEmail().trim());
            Address address = addressRepository.getById(staffRequest.getAddressId());
            Province province = provinceRepository.getById(staffRequest.getProvinceId());
            District district = districtRepository.getById(staffRequest.getDistrictId());
            Ward ward = wardRepository.getById(staffRequest.getWardId());
            address.setProvince(province);
            address.setDistrict(district);
            address.setWard(ward);
            address.setDetail(staffRequest.getDetailAddress().trim());
            user.setAddress(address);
            userRepository.save(user);
            response.put("status",Constant.SUCCESS);
        return new ResponseEntity<>( response,HttpStatus.OK);
    }
}
