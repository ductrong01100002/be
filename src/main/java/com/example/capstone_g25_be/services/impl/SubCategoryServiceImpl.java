package com.example.capstone_g25_be.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.example.capstone_g25_be.entities.Category;
import com.example.capstone_g25_be.entities.SubCategory;
import com.example.capstone_g25_be.model.request.SubCategoryRequest;
import com.example.capstone_g25_be.model.response.ResponseVo;
import com.example.capstone_g25_be.model.response.SubCategoryResponse;
import com.example.capstone_g25_be.repository.CategoryRepository;
import com.example.capstone_g25_be.repository.SubCategoryRepository;
import com.example.capstone_g25_be.services.SubCategoryService;
import com.example.capstone_g25_be.utils.SubCategoryConvertUtil;

@Service
public class SubCategoryServiceImpl implements SubCategoryService{
	
	@Autowired
	SubCategoryRepository subCategoryRepository;
	
	@Autowired
	SubCategoryConvertUtil subCategoryConvertUtil;
	
	@Autowired
	CategoryRepository categoryRepository;

	@Override
	public ResponseEntity<?> findSubCategoryByCategoryId(Long cagegoryId) {
		ResponseVo responseVo = new ResponseVo();
		if (!ObjectUtils.isEmpty(cagegoryId)) {
			List<SubCategory> listSubCategory = subCategoryRepository.getSubCategoryByCategoryId(cagegoryId);
			Map<String, Object> map = new HashMap<>();
			if (!ObjectUtils.isEmpty(listSubCategory)) {
				map.put("subCategory", 
						SubCategoryResponse.createSuccessData(listSubCategory));
				responseVo.setData(map);
				return new ResponseEntity<>(responseVo, HttpStatus.OK);
			}
			responseVo.setMessage("Không tìm thấy Sub Category");
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
	}

	@Override
	public ResponseEntity<?> addSubCategory(SubCategoryRequest subCategoryRequest) {
		ResponseVo responseVo = new ResponseVo();
		SubCategory s = subCategoryRepository.findCategoryByName(subCategoryRequest.getName().trim());
		if(s != null) {
			responseVo.setMessage("Tên danh mục con đã bị trùng !!");
			return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
		}
		Category c = categoryRepository.findCategoryByName(subCategoryRequest.getName().trim());
		if(c != null) {
			responseVo.setMessage("Tên danh mục con không được trùng với tên danh mục cha!!");
			return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
		}
		SubCategory subCategory = subCategoryConvertUtil.convertSubCategory(subCategoryRequest);
		Category category = categoryRepository.findCategoryById(subCategoryRequest.getCategoryId());
		subCategory.setCategory(category);
		subCategory.setDeletedAt(false);
		subCategoryRepository.save(subCategory);
		responseVo.setMessage("Tạo thành công !!");
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> updateSubCategory(SubCategoryRequest subCategoryRequest) {
		ResponseVo responseVo = new ResponseVo();
		Category c = categoryRepository.findCategoryByName(subCategoryRequest.getName().trim());
		if(c != null) {
			responseVo.setMessage("Tên danh mục con không được trùng với tên danh mục cha!!");
			return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
		}
		
		SubCategory s = subCategoryRepository.findSubCategoryByIdAndName(subCategoryRequest.getId(),
				subCategoryRequest.getName().trim());
		if(s != null) {
			responseVo.setMessage("Tên danh mục con đã bị trùng !!");
			return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
		}
		SubCategory subCategory = subCategoryConvertUtil.convertSubCategory(subCategoryRequest);
		Category category = categoryRepository.findCategoryById(subCategoryRequest.getCategoryId());
		subCategory.setCategory(category);
		subCategory.setDeletedAt(false);
		subCategoryRepository.save(subCategory);
		responseVo.setMessage("Cập nhập thành công !!");
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}
	
	

}
