package com.example.capstone_g25_be.services.impl;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;

import com.example.capstone_g25_be.bucket.BucketName;
import com.example.capstone_g25_be.entities.Product;
import com.example.capstone_g25_be.entities.ServiceResult;
import com.example.capstone_g25_be.fileStore.FileStore;
import com.example.capstone_g25_be.model.response.UserListDTO;
import com.example.capstone_g25_be.utils.*;
import org.apache.http.entity.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.capstone_g25_be.entities.User;
import com.example.capstone_g25_be.model.response.ListProductVo;
import com.example.capstone_g25_be.model.response.ListUserResponse;
import com.example.capstone_g25_be.model.response.ResponseVo;
import com.example.capstone_g25_be.repository.UserRepository;
import com.example.capstone_g25_be.services.UserService;
import org.springframework.web.multipart.MultipartFile;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private EmailSender emailSender;

	@Autowired
	private PasswordEncoder encoder;

	@Autowired
	private FileStore fileStore;
	
	@Override
	public ResponseEntity<?> findAll(int pageIndex, int pageSize, String userName) {
		Pageable pageable = PageRequest.of(pageIndex - 1, pageSize);

		String userNameSearch = "";
		
		if (userName != null) {
			userNameSearch = userName;
		}
		
		Page<User> userPage = userRepository.findBySearch(userNameSearch, pageable);

		Map<String, Object> mapDataSearch = new HashMap<>();
		mapDataSearch.put("userName", userName);
		ResponseVo responseVo = new ResponseVo();
		Map<String, Object> map = new HashMap<>();
		if (userPage.isEmpty()) {
			map.put("user", userPage.getContent());
			map.put("totalRecord", 0);
			responseVo.setMessage("Not found List User");
			responseVo.setData(map);
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		List<ListProductVo> listProductVo = new ArrayList<>();

		
		map.put("user", ListUserResponse.createSuccessData(userPage.getContent()));
		map.put("searchData", mapDataSearch);
		map.put("currentPage", pageIndex);
		map.put("totalRecord", userPage.getTotalElements());
		map.put("pageSize", userPage.getSize());
		map.put("totalPage", userPage.getTotalPages());
		responseVo.setData(map);
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> getListUser(Integer pageIndex, Integer pageSize, String searchData) {
		Pageable pageable = PageRequest.of(pageIndex, pageSize);
		List<Map<String, Object>> listUser = userRepository.findByUserBySearchData(searchData, pageable);
		List<UserListDTO> list = UserListDTO.createSuccessData(listUser);
		Map<String, Object> output = new HashMap<>();
		output.put("data",list);
		ResponseVo responseVo = new ResponseVo();
		responseVo.setMessage("success");
		responseVo.setStatus(true);
		responseVo.setData(output);
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}
	@Override
	public ServiceResult<Boolean> resetPassword(String userName) {
		Optional<User> user = userRepository.findByUserName(userName);
		if (!user.isPresent()) {
			return new ServiceResult<>("Tên đăng nhập không tồn tại", 500);
		} else {
			String email = userRepository.findEmailByUserName(userName);
			String OTP =  String.valueOf(Generate.generateOTP(6));
			boolean result = emailSender.sendOtp(email, OTP,user.get().getFullName());
			LocalDateTime resetDate
					= LocalDateTime.now().plusMinutes(1);
			user.get().setResetDate(resetDate);
			user.get().setOtp(OTP);
			userRepository.save(user.get());
			if (result) {
				return new ServiceResult<>( "Gửi email thành công", 200);
			} else {
				return new ServiceResult<>( "Gửi email thất bại", 500);
			}
		}

	}

	@Override
	public ResponseEntity<?> checkOTPandResetPass(String otp, String userName) {
		Map<String, Object> output = new HashMap<>();
		Optional<User> user = userRepository.findByUserName(userName);
		if (!user.isPresent()) {
			output.put("status",Constant.FAIL);
			output.put("message","Tên đăng nhập không tồn tại");
		} else {
			String otpDB = user.get().getOtp();
			if (!otpDB.equals(otp)) {
				output.put("status",Constant.FAIL);
				output.put("message","OTP nhập sai");
			}
			else if (LocalDateTime.now().isAfter(user.get().getResetDate())) {

				output.put("status",Constant.FAIL);
				output.put("message","Thời gian nhập OTP đã hết");
			}else {
				output.put("status", Constant.SUCCESS);
				output.put("message", "OTP chính xác");
			}
		}
		return new ResponseEntity<>(output, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> addImageUser(MultipartFile file) {
		if (file.isEmpty()) {
			throw new IllegalStateException("Cannot upload empty file [ " + file.getSize() + " ]");
		}
		if (!Arrays.asList(
				ContentType.IMAGE_JPEG.getMimeType(),
				ContentType.IMAGE_PNG.getMimeType(),
				ContentType.IMAGE_GIF.getMimeType())
				.contains(file.getContentType())) {
			throw new IllegalStateException("File must be an image");
		}
		User user = userRepository.findUserLatest().get(0);

		Map<String, String> metadata = new HashMap<>();
		metadata.put("Content-Type", file.getContentType());
		metadata.put("Content-Length", String.valueOf( file.getSize()));

		String path = String.format("%s/%s/%s", BucketName.PROFILE_IMAGE.getBucketName(),"User", user.getId());
		String fileName = String.format("%s", file.getOriginalFilename());

		try {
			fileStore.save(path, fileName, Optional.of(metadata), file.getInputStream());
			user.setImage(fileName);
			userRepository.save(user);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		ResponseVo responseVo = new ResponseVo();
		responseVo.setMessage("Thêm ảnh thành công !!");
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> updateImageUser(Long userId, MultipartFile file) {
		if (file.isEmpty()) {
			throw new IllegalStateException("Cannot upload empty file [ " + file.getSize() + " ]");
		}
		if (!Arrays.asList(
				ContentType.IMAGE_JPEG.getMimeType(),
				ContentType.IMAGE_PNG.getMimeType(),
				ContentType.IMAGE_GIF.getMimeType())
				.contains(file.getContentType())) {
			throw new IllegalStateException("File must be an image");
		}
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new RuntimeException("Mã nhân viên không tồn tại !"));

		Map<String, String> metadata = new HashMap<>();
		metadata.put("Content-Type", file.getContentType());
		metadata.put("Content-Length", String.valueOf( file.getSize()));

		String path = String.format("%s/%s/%s", BucketName.PROFILE_IMAGE.getBucketName(),"User", user.getId());
		String fileName = String.format("%s", file.getOriginalFilename());

		try {
			fileStore.save(path, fileName, Optional.of(metadata), file.getInputStream());
			user.setImage(fileName);
			userRepository.save(user);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		ResponseVo responseVo = new ResponseVo();
		responseVo.setMessage("Cập nhập ảnh thành công !!");
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

	@Override
	public byte[] downloadUserImage(Long userId) {
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new RuntimeException("User Name: User is not found."));
		String path = String.format("%s/%s/%s",
				BucketName.PROFILE_IMAGE.getBucketName(),
				"User",
				user.getId());
		return fileStore.download(path, user.getImage());
	}

	@Override
	public ResponseEntity<?> settingPasswword(String oldPassword,String newPassword, String userName) {
		Map<String, Object> map = new HashMap<>();
		try {
			Optional<User> user = userRepository.findByUserName(userName);
			if (!user.isPresent()) {
				map.put("status",Constant.FAIL);
				map.put("message","Tên đăng nhập không tồn tại");
				return new ResponseEntity<>(map,HttpStatus.OK);
			}
			String password = user.get().getPassWord();
			boolean checkOldPassword = encoder.matches(oldPassword,password);
			if(checkOldPassword){
				user.get().setPassWord(encoder.encode(newPassword));
				map.put("message", "Cập nhật mật khẩu thành công !");
				map.put("code", Constant.SUCCESS);
				userRepository.save(user.get());
			}else{
				map.put("message", "Mật khẩu cũ của bạn không chính xác!");
				map.put("code", Constant.FAIL);
			}
		}catch (Exception e){
			map.put("message", "Cập nhật mật khẩu thất bại !");
			map.put("code", Constant.FAIL);
			e.printStackTrace();
		}
		return new ResponseEntity<>(map,HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> createPasswword(String userName,String newPassword) {
		Map<String, Object> map = new HashMap<>();
		try {
			Optional<User> user = userRepository.findByUserName(userName);
			if (!user.isPresent()) {
				map.put("status",Constant.FAIL);
				map.put("message","Tên đăng nhập không tồn tại");
				return new ResponseEntity<>(map,HttpStatus.OK);
			}
			user.get().setPassWord(encoder.encode(newPassword));
			userRepository.save(user.get());
			map.put("message", "Cập nhật mật khẩu thành công !");
			map.put("code", Constant.SUCCESS);
		}catch (Exception e){
			map.put("message", "Cập nhật mật khẩu thất bại !");
			map.put("code", Constant.FAIL);
			e.printStackTrace();
		}
		return new ResponseEntity<>(map,HttpStatus.OK);
	}

}
