package com.example.capstone_g25_be.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.example.capstone_g25_be.entities.Ward;
import com.example.capstone_g25_be.model.response.ListWardResponse;
import com.example.capstone_g25_be.model.response.ResponseVo;
import com.example.capstone_g25_be.repository.WardRepository;
import com.example.capstone_g25_be.services.WardService;

@Service
public class WardServiceImpl implements WardService{
	
	@Autowired
	private WardRepository wardRepository;

	@Override
	public ResponseEntity<?> findByDistrictId(Long districtId) {
		ResponseVo responseVo = new ResponseVo();
		if (!ObjectUtils.isEmpty(districtId)) {
			List<Ward> listWard = wardRepository.findListWardByDistrictId(districtId);
			Map<String, Object> map = new HashMap<>();
			if (!ObjectUtils.isEmpty(listWard)) {
				map.put("ward", ListWardResponse.createSuccessData(listWard));
				responseVo.setData(map);
				return new ResponseEntity<>(responseVo, HttpStatus.OK);
			}
			responseVo.setMessage("Không tìm thấy Ward");
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
	}

}
