package com.example.capstone_g25_be.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.example.capstone_g25_be.entities.Address;
import com.example.capstone_g25_be.entities.Consignment;
import com.example.capstone_g25_be.entities.District;
import com.example.capstone_g25_be.entities.Province;
import com.example.capstone_g25_be.entities.Ward;
import com.example.capstone_g25_be.entities.Warehouse;
import com.example.capstone_g25_be.model.request.WareHouseRequest;
import com.example.capstone_g25_be.model.response.ListWareHouseResponse;
import com.example.capstone_g25_be.model.response.ResponseVo;
import com.example.capstone_g25_be.model.response.WareHouseDetailResponse;
import com.example.capstone_g25_be.repository.AddressRepository;
import com.example.capstone_g25_be.repository.ConsignmentRepository;
import com.example.capstone_g25_be.repository.DistrictRepository;
import com.example.capstone_g25_be.repository.ProvinceRepository;
import com.example.capstone_g25_be.repository.WardRepository;
import com.example.capstone_g25_be.repository.WarehouseRepository;
import com.example.capstone_g25_be.services.WareHouseService;
import com.example.capstone_g25_be.utils.WareHouseConvertUtil;
@Service
public class WareHouseServiceImpl implements WareHouseService{
	
	@Autowired
	WarehouseRepository warehouseRepository;
	
	@Autowired
	AddressRepository addressRepository;
	
	@Autowired
	ProvinceRepository provinceRepository;
	
	@Autowired
	DistrictRepository districtRepository;
	
	@Autowired
	WardRepository wardRepository;
	
	@Autowired
	ConsignmentRepository consignmentRepository;
	
	@Autowired
	WareHouseConvertUtil wareHouseConvertUtil;

	@Override
	public ResponseEntity<?> findAll(int pageIndex, int pageSize, String wareHouseName) {
		Pageable pageable = PageRequest.of(pageIndex - 1, pageSize);
		String wareHouseNameSearch = "";
		Page<Warehouse> wareHousePage = null;
		if (!ObjectUtils.isEmpty(wareHouseName)) {
			wareHouseNameSearch = wareHouseName;
			wareHousePage = warehouseRepository.findBySearch(wareHouseNameSearch.trim(), pageable);
		}else {
			wareHousePage = warehouseRepository.findAllByDeletedAt(false, pageable);
		}

		Map<String, Object> mapDataSearch = new HashMap<>();
		mapDataSearch.put("wareHouseName", wareHouseName);
		ResponseVo responseVo = new ResponseVo();
		Map<String, Object> map = new HashMap<>();
		if (wareHousePage.isEmpty()) {
			map.put("warehouse", wareHousePage.getContent());
			map.put("totalRecord", 0);
			responseVo.setMessage("Không tìm thấy List Ware House");
			responseVo.setData(map);
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		map.put("warehouse", ListWareHouseResponse.createSuccessData(wareHousePage.getContent()));
		map.put("searchData", mapDataSearch);
		map.put("currentPage", pageIndex);
		map.put("totalRecord", wareHousePage.getTotalElements());
		map.put("pageSize", wareHousePage.getSize());
		map.put("totalPage", wareHousePage.getTotalPages());
		responseVo.setData(map);

		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> addWareHouse(WareHouseRequest wareHouseRequest) {
		ResponseVo responseVo = new ResponseVo();
		Warehouse warehouseExist = warehouseRepository.findWareHouseByName(wareHouseRequest.getName().trim());
		if(warehouseExist != null) {
			responseVo.setMessage("Tên kho đã bị trùng !!");
			return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
		}
		Warehouse wareHouse = wareHouseConvertUtil.convertWareHouse(wareHouseRequest);
		Province province = provinceRepository.findProvinceById(wareHouseRequest.getProvinceId());
		District district = districtRepository.findDistrictById(wareHouseRequest.getDistrictId());
		Ward ward = wardRepository.findWardById(wareHouseRequest.getWardId());
		Address address = new Address();
		address.setProvince(province);
		address.setDistrict(district);
		address.setWard(ward);
		address.setDetail(wareHouseRequest.getAddressDetail());
		address.setDeletedAt(false);
		addressRepository.save(address);
		wareHouse.setAddress(address);
		wareHouse.setDeletedAt(false);
		warehouseRepository.save(wareHouse);
		responseVo.setMessage("Tạo thành công !!");
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> deleteWareHouse(Long wareHouseId) {
		ResponseVo responseVo = new ResponseVo();
		Warehouse wareHouse = warehouseRepository.findWareHouseById(wareHouseId);
		if(wareHouse == null) {
			responseVo.setMessage("Không tìm thấy kho hàng");
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		List<Long> listConsignment = consignmentRepository.findAllConsignmentByWareHouseId(wareHouseId);
		if(listConsignment.size() > 0) {
			responseVo.setMessage("Không thể xóa kho hàng vì trong kho vẫn còn sản phẩm");
			return new ResponseEntity<>(responseVo, HttpStatus.METHOD_NOT_ALLOWED);
		}
		wareHouse.setDeletedAt(true);
		warehouseRepository.save(wareHouse);
		responseVo.setMessage("Xóa kho hàng thành công");
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> findWareHouseById(Long wareHouseId) {
		ResponseVo responseVo = new ResponseVo();
		if (!ObjectUtils.isEmpty(wareHouseId)) {
			Warehouse warehouse = warehouseRepository.findWareHouseById(wareHouseId);
			Map<String, Object> map = new HashMap<>();
			if (!ObjectUtils.isEmpty(warehouse)) {
				map.put("warehouse", WareHouseDetailResponse.createSuccessData(warehouse));
				responseVo.setData(map);
				return new ResponseEntity<>(responseVo, HttpStatus.OK);
			}
			responseVo.setMessage("Không tìm thấy nhà kho");
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
	}

	@Override
	public ResponseEntity<?> updateWareHouse(WareHouseRequest wareHouseRequest) {
		ResponseVo responseVo = new ResponseVo();
		Warehouse w = warehouseRepository.findWarehouseByIdAndName(wareHouseRequest.getId(), wareHouseRequest.getName().trim());
		if(w != null) {
			responseVo.setMessage("Tên kho hàng đã bị trùng !!");
			return new ResponseEntity<>(responseVo, HttpStatus.BAD_REQUEST);
		}
		Warehouse wareHouse = wareHouseConvertUtil.convertWareHouse(wareHouseRequest);
		Province province = provinceRepository.findProvinceById(wareHouseRequest.getProvinceId());
		District district = districtRepository.findDistrictById(wareHouseRequest.getDistrictId());
		Ward ward = wardRepository.findWardById(wareHouseRequest.getWardId());
		Address address = new Address();
		address.setProvince(province);
		address.setDistrict(district);
		address.setWard(ward);
		address.setDetail(wareHouseRequest.getAddressDetail());
		address.setDeletedAt(false);
		addressRepository.save(address);
		wareHouse.setAddress(address);
		wareHouse.setDeletedAt(false);
		warehouseRepository.save(wareHouse);
		responseVo.setMessage("Cập nhập thành công !!");
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> findAllNotPaging() {
		List<Warehouse> listWarehouse = warehouseRepository.findAllNotPaging();
		ResponseVo responseVo = new ResponseVo();
		Map<String, Object> map = new HashMap<>();
		if (listWarehouse.size() == 0) {
			map.put("warehouse", null);
			map.put("totalRecord", 0);
			responseVo.setMessage("Không tìm thấy List Warehouse");
			responseVo.setData(map);
			return new ResponseEntity<>(responseVo, HttpStatus.OK);
		}
		map.put("warehouse", ListWareHouseResponse.createSuccessData(listWarehouse));
		responseVo.setData(map);
		return new ResponseEntity<>(responseVo, HttpStatus.OK);
	}
	
	

}
