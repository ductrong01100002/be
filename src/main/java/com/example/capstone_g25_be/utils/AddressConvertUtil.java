package com.example.capstone_g25_be.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.capstone_g25_be.entities.Address;
import com.example.capstone_g25_be.entities.District;
import com.example.capstone_g25_be.entities.Province;
import com.example.capstone_g25_be.entities.Ward;
import com.example.capstone_g25_be.model.request.ManufacturerRequest;
import com.example.capstone_g25_be.repository.AddressRepository;

@Component
public class AddressConvertUtil {
	
	@Autowired
	AddressRepository addressRepository;
	
	public Address convertAddress(ManufacturerRequest manufactorRequest) {
		Address address = new Address();
		Province province = new Province();
		District district = new District();
		Ward ward = new Ward();
		
		province.setId(manufactorRequest.getId());
		
		district.setId(manufactorRequest.getDistrictId());
		
		ward.setId(manufactorRequest.getWardId());
		address.setProvince(province);
		address.setDistrict(district);
		address.setWard(ward);
		address.setDetail(manufactorRequest.getAddressDetail());
		return address;
	}

}
