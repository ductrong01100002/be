package com.example.capstone_g25_be.utils;

import org.springframework.stereotype.Component;

import com.example.capstone_g25_be.entities.Category;
import com.example.capstone_g25_be.model.request.CategoryRequest;

@Component
public class CategoryConvertUtil {
	
	public Category convertCategory(CategoryRequest categoryRequest) {
		Category category = new Category();
		category.setId(categoryRequest.getId());
		category.setName(categoryRequest.getName().trim());
		category.setDescription(categoryRequest.getDescription());
		return category;
	}

}
