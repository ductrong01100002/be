package com.example.capstone_g25_be.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Component
public class EmailSender {


    @Value("${mail.username}")
    private String username;
    @Value("${mail.password}")
    private String password;
    @Value("${mail.smtp.auth}")
    private String auth;
    @Value("${mail.smtp.starttls.enable}")
    private String enable;
    @Value("${mail.smtp.host}")
    private String host;
    @Value("${mail.smtp.port}")
    private String port;


    public boolean sendMailChangePassword(String to, String OTP,String fullName) {

        Properties props = new Properties();
        props.put("mail.smtp.auth", auth);
        props.put("mail.smtp.starttls.enable", enable);
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtp.ssl.protocols", "TLSv1.2");
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        try {
            Message message = new MimeMessage(session);
            message.setContent(message, "text/plain; charset=UTF-8");
            message.setFrom(new InternetAddress(username));
            System.out.println(username);
            System.out.println(password);
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));
            message.setSubject("[CMIMS] CÀI LẠI MẬT KHẨU");
            message.setText("Xin chào, " + fullName+"\n** Đây là tin nhắn tự động - vui lòng không trả lời vì bạn sẽ không nhận được phản hồi**\n"+
                    "Mật khẩu mới của bạn là: " + OTP+"\nCảm ơn."+"\nVU HUNG CMIMS\n");

            Transport.send(message);

            System.out.println("Done");

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return true;
    }
    public boolean sendOtp(String to, String OTP,String fullName) {

        Properties props = new Properties();
        props.put("mail.smtp.auth", auth);
        props.put("mail.smtp.starttls.enable", enable);
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtp.ssl.protocols", "TLSv1.2");
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        try {
            Message message = new MimeMessage(session);
            message.setContent(message, "text/plain; charset=UTF-8");
            message.setFrom(new InternetAddress(username));
            System.out.println(username);
            System.out.println(password);
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));
            message.setSubject("[CMIMS] Mã OTP ");
            message.setText("Xin chào, " + fullName+"\n** Đây là tin nhắn tự động - vui lòng không trả lời vì bạn sẽ không nhận được phản hồi**\n"+
                    "OTP cài đặt lại mật khẩu thời giạn sử dụng trong phong 1 phút: " + OTP+"\nCảm ơn."+"\nVU HUNG CMIMS\n");

            Transport.send(message);

            System.out.println("Done");

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return true;
    }

    public boolean sendMailSingUp(String to, String OTP,String fullName,String userName) {

        Properties props = new Properties();
        props.put("mail.smtp.auth", auth);
        props.put("mail.smtp.starttls.enable", enable);
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtp.ssl.protocols", "TLSv1.2");
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        try {
            Message message = new MimeMessage(session);
            message.setContent(message, "text/plain; charset=UTF-8");
            message.setFrom(new InternetAddress(username));
            System.out.println(username);
            System.out.println(password);
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));
            message.setSubject("[CMIMS] Công Ty Vũ Hưng chào mừng thành viên mới");
            message.setText("Xin chào, " + fullName+"\n** Đây là tin nhắn tự động - vui lòng không trả lời vì bạn sẽ không nhận được phản hồi**\n"+"Tên đăng nhập của bạn là: " + userName+
                    "\nMật khẩu của bạn là: " + OTP+"\nVui lòng bấm vào link để đăng nhập: "+"\n https://www.vuhungcmims.tk/"+"\nCảm ơn."+"\nVU HUNG CMIMS\n");
            Transport.send(message);

            System.out.println("Done");

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return true;
    }
}
