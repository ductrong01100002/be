package com.example.capstone_g25_be.utils;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.capstone_g25_be.entities.InventoryCheckingHistory;
import com.example.capstone_g25_be.model.request.InventoryCheckingHistoryRequest;
import com.example.capstone_g25_be.repository.UserRepository;
import com.example.capstone_g25_be.repository.WarehouseRepository;

@Component
public class InventoryCheckingHistoryConvertUtil {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	WarehouseRepository warehouseRepository;
	
	public InventoryCheckingHistory convertInventoryCheckingHistory(InventoryCheckingHistoryRequest inventoryCheckingHistoryRequest) {
		InventoryCheckingHistory inventoryCheckingHistory = new InventoryCheckingHistory();
		inventoryCheckingHistory.setUser(
				userRepository.findUserById(inventoryCheckingHistoryRequest.getUserId()));
		inventoryCheckingHistory.setWarehouse(
				warehouseRepository.findWareHouseById(inventoryCheckingHistoryRequest.getWareHouseId()));
		inventoryCheckingHistory.setTotalDifferentAmout(inventoryCheckingHistoryRequest.getTotalDifferentAmout());
		inventoryCheckingHistory.setCreateDate(LocalDateTime.now());
		inventoryCheckingHistory.setDeletedAt(false);
		return inventoryCheckingHistory;
	}

}
