package com.example.capstone_g25_be.utils;

import org.springframework.stereotype.Component;

import com.example.capstone_g25_be.entities.Manufacturer;
import com.example.capstone_g25_be.model.request.ManufacturerRequest;

@Component
public class ManufacturerConvertUtil {
	
	public Manufacturer convertManufactor(ManufacturerRequest manufactorRequest) {
		Manufacturer manufactor = new Manufacturer();
		manufactor.setId(manufactorRequest.getId());
		manufactor.setName(manufactorRequest.getName().trim());
		manufactor.setEmail(manufactorRequest.getEmail().trim());
		manufactor.setPhone(manufactorRequest.getPhone().trim());
		return manufactor;
	}

}
