package com.example.capstone_g25_be.utils;

import org.springframework.stereotype.Component;

import com.example.capstone_g25_be.entities.Product;
import com.example.capstone_g25_be.model.request.ProductRequest;

@Component
public class ProductConvertUtil {
	
	public Product convertProduct(ProductRequest productRequest) {
		Product product = new Product();
		product.setId(productRequest.getId());
		product.setName(productRequest.getName().trim());
		product.setProductCode(productRequest.getProductCode());
		product.setUnitMeasure(productRequest.getUnitMeasure());
		product.setWrapUnitMeasure(productRequest.getWrapUnitMeasure());
		product.setNumberOfWrapUnitMeasure(productRequest.getNumberOfWrapUnitMeasure());
		product.setColor(productRequest.getColor());
		product.setDescription(productRequest.getDescription());
		product.setUnitPrice(productRequest.getUnitPrice());
		if(productRequest.getImageName() != null) {
			product.setImage(productRequest.getImageName());
		}
		return product;
	}

}
