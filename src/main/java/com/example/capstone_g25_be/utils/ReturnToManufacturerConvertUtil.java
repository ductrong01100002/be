package com.example.capstone_g25_be.utils;

import java.time.LocalDateTime;
import java.time.ZoneId;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.capstone_g25_be.entities.ReturnToManufacturer;
import com.example.capstone_g25_be.model.request.ReturnToManufacturerRequest;
import com.example.capstone_g25_be.repository.ManufacturerRepository;
import com.example.capstone_g25_be.repository.StatusRepository;
import com.example.capstone_g25_be.repository.UserRepository;
import com.example.capstone_g25_be.repository.WarehouseRepository;

@Component
public class ReturnToManufacturerConvertUtil {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	WarehouseRepository warehouseRepository;
	
	@Autowired
	ManufacturerRepository manufacturerRepository;
	
	@Autowired
	StatusRepository statusRepository;

	public ReturnToManufacturer convertReturnToManufacturer(ReturnToManufacturerRequest returnToManufacturerRequest) {
		ReturnToManufacturer returnToManufacturer = new ReturnToManufacturer();
		returnToManufacturer.setCreateDate(LocalDateTime.now());
		returnToManufacturer.setExpectedReturnDate(returnToManufacturerRequest.getExpectedReturnDate());
		returnToManufacturer.setUser(userRepository.findUserById(returnToManufacturerRequest.getUserCreateId()));
		returnToManufacturer.setWarehouse(warehouseRepository.findWareHouseById(returnToManufacturerRequest.getWarehouseId()));
		returnToManufacturer.setManufacturer(manufacturerRepository.findManufactorById(returnToManufacturerRequest.getManufacturerId()));
		returnToManufacturer.setStatus(statusRepository.findStatusById( (long) 1));
		returnToManufacturer.setDeletedAt(false);
		returnToManufacturer.setTotalAmout(returnToManufacturerRequest.getTotalAmout());
		returnToManufacturer.setDescription(returnToManufacturerRequest.getDescription());
		return returnToManufacturer;
	}

}
