package com.example.capstone_g25_be.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.example.capstone_g25_be.entities.ReturnToManufacturer;
import com.example.capstone_g25_be.entities.ReturnToManufacturerDetail;
import com.example.capstone_g25_be.model.request.ReturnToManufacturerDetailRequest;

@Component
public class ReturnToManufacturerDetailConvertUtil {
	
	public List<ReturnToManufacturerDetail> convertReturnToManufacturerDetail
			(List<ReturnToManufacturerDetailRequest> listReturnToManufacturerDetailRequest,
					ReturnToManufacturer returnToManufacturer) {
		List<ReturnToManufacturerDetail> response = new ArrayList<>();
		for (ReturnToManufacturerDetailRequest r: listReturnToManufacturerDetailRequest) {
			ReturnToManufacturerDetail returnToManufacturerDetail = new ReturnToManufacturerDetail();
			if(r.getId() != null) {
				returnToManufacturerDetail.setId(r.getId());
			}
			returnToManufacturerDetail.setProductName(r.getProductName().trim());
			returnToManufacturerDetail.setQuantity(r.getQuantity());
			returnToManufacturerDetail.setUnitMeasure(r.getUnitMeasure());
			returnToManufacturerDetail.setUnitPrice(r.getUnitPrice());
			returnToManufacturerDetail.setReturnToManufacturer(returnToManufacturer);
			response.add(returnToManufacturerDetail);
		}
		
		return response;
	}

}
