package com.example.capstone_g25_be.utils;

import org.springframework.stereotype.Component;

import com.example.capstone_g25_be.entities.SubCategory;
import com.example.capstone_g25_be.model.request.SubCategoryRequest;
@Component
public class SubCategoryConvertUtil {
	
	public SubCategory convertSubCategory(SubCategoryRequest subCategoryRequest) {
		SubCategory subCategory = new SubCategory();
		subCategory.setId(subCategoryRequest.getId());
		subCategory.setName(subCategoryRequest.getName().trim());
		subCategory.setDescription(subCategoryRequest.getDescription());
		return subCategory;
	}

}
