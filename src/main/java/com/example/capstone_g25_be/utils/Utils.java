package com.example.capstone_g25_be.utils;

import com.example.capstone_g25_be.model.response.MessageResponse;
import com.example.capstone_g25_be.security.services.UserDetailsImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;

public class Utils {
    public static boolean checkIsActive(){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Boolean isActive = true;
        if (principal instanceof UserDetailsImpl) {
            isActive = ((UserDetailsImpl)principal).getIsActive();
        }
        if(!isActive){
            isActive = false;
        }
        return isActive;
    }
}
