package com.example.capstone_g25_be.utils;

import org.springframework.stereotype.Component;

import com.example.capstone_g25_be.entities.Warehouse;
import com.example.capstone_g25_be.model.request.WareHouseRequest;

@Component
public class WareHouseConvertUtil {
	
	public Warehouse convertWareHouse(WareHouseRequest wareHouseRequest) {
		Warehouse wareHouse = new Warehouse();
		wareHouse.setId(wareHouseRequest.getId());
		wareHouse.setName(wareHouseRequest.getName().trim());
		return wareHouse;
	}

}
