package com.example.capstone_g25_be.utils.validate;

import com.example.capstone_g25_be.model.request.ImportGoodsRequest;
import com.example.capstone_g25_be.model.request.UpdateImportGoodsRequest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class OrderValidator {
    public static String validateOrder(ImportGoodsRequest importGoodsRequest)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDateTime currentDate = LocalDateTime.parse(LocalDateTime.now().toString(),formatter);
        LocalDateTime createDate = LocalDateTime.parse(importGoodsRequest.getCreatedDate().toString(),formatter);
       if(importGoodsRequest.getManufactorId().equals(null) || importGoodsRequest.getManufactorId().equals("")){
            return "Manufactor can not empty";
        }
        else if(importGoodsRequest.getBillReferenceNumber().equals(null) || importGoodsRequest.getManufactorId().equals("")){
            return "BillReferenceNumber can not empty";
        }
        else return "success";
    }

    public static String validateUpdateOrder(UpdateImportGoodsRequest updateImportGoodsRequest)
    {
        LocalDateTime currentDate = LocalDateTime.now();
        LocalDateTime createDate = updateImportGoodsRequest.getCreatedDate();
        if(createDate.isBefore(currentDate)){
            return "Date false";
        }
        else if(updateImportGoodsRequest.getManufactorId().equals(null) || updateImportGoodsRequest.getManufactorId().equals("")){
            return "Manufactor can not empty";
        }
        else if(updateImportGoodsRequest.getBillReferenceNumber().equals(null) || updateImportGoodsRequest.getManufactorId().equals("")){
            return "BillReferenceNumber can not empty";
        }
        else return "success";
    }
}
