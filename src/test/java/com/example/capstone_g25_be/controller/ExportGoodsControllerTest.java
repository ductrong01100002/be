//package com.example.capstone_g25_be.controller;
//
//import com.example.capstone_g25_be.model.request.ConsignmentExport;
//import com.example.capstone_g25_be.model.request.ConsignmentRequest;
//import com.example.capstone_g25_be.model.request.ExportGoodsRequest;
//import com.example.capstone_g25_be.model.request.ImportGoodsRequest;
//import com.example.capstone_g25_be.model.response.MessageResponse;
//import com.example.capstone_g25_be.repository.ExportOrderRepository;
//import com.example.capstone_g25_be.repository.ImportOrderRepository;
//import com.example.capstone_g25_be.services.ExportGoodsService;
//import com.example.capstone_g25_be.utils.Constant;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Test;
//import org.mockito.Mock;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.MvcResult;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//
//import java.time.LocalDateTime;
//import java.util.ArrayList;
//import java.util.List;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.mockito.Mockito.when;
//
//@AutoConfigureMockMvc
//@SpringBootTest
//public class ExportGoodsControllerTest {
//
//    @Mock
//    private ExportGoodsService exportGoodsService;
//    @Mock
//    private ExportOrderRepository exportOrderRepository;
//    @Autowired
//    private MockMvc mockMvc;
//    @Autowired
//    private ObjectMapper objectMapper;
//
//    @BeforeAll
//    static void setUpBeforeClass() throws Exception {
//    }
//
//    @Test
//    void testExportGoodsControllerSuccess() throws Exception {
//
//        String billReferenceNumber = "Imp001";
//        LocalDateTime createdDate = LocalDateTime.now();
//        String description = "Nhap hang";
//        Long userId = 1L;
//
//
//        Long consignmentId = 47L;
//        int quantity = 10;
//        double unitPrice = 20.00;
//
//        ConsignmentExport consignmentExport = new ConsignmentExport(consignmentId,quantity,unitPrice);
//        List<ConsignmentExport> consignmentRequestList = new ArrayList<>();
//        consignmentRequestList.add(consignmentExport);
//
//        ExportGoodsRequest exportGoodsRequest = new ExportGoodsRequest(billReferenceNumber, createdDate, description, userId, consignmentRequestList);
//        MessageResponse messageResponse = new MessageResponse();
//        messageResponse.setMessage("Tạo phiếu xuất kho thành công !");
//        messageResponse.setStatus(Constant.SUCCESS);
//        String uri = "/api/export-order/create";
//        ResponseEntity ResponseEntity = new ResponseEntity(messageResponse, HttpStatus.OK);
//        when(exportGoodsService.save(exportGoodsRequest)).thenReturn(ResponseEntity);
//        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(uri, exportGoodsRequest)
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(objectMapper.writeValueAsString(exportGoodsRequest)))
//                .andReturn();
//        int status = mvcResult.getResponse().getStatus();
//        assertEquals(200, status);
//    }
//}
