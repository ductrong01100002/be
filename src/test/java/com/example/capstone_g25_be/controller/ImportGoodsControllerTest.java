//package com.example.capstone_g25_be.controller;
//
//import com.example.capstone_g25_be.entities.User;
//import com.example.capstone_g25_be.model.request.ConsignmentRequest;
//import com.example.capstone_g25_be.model.request.ImportGoodsRequest;
//import com.example.capstone_g25_be.model.response.MessageResponse;
//import com.example.capstone_g25_be.repository.*;
//import com.example.capstone_g25_be.security.services.UserDetailsServiceImpl;
//import com.example.capstone_g25_be.services.ImportGoodsService;
//import com.example.capstone_g25_be.utils.Constant;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Test;
//import org.mockito.Mock;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import static org.junit.jupiter.api.Assertions.*;
//import static org.mockito.Mockito.when;
//
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.MvcResult;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//
//import java.time.LocalDateTime;
//import java.util.ArrayList;
//import java.util.List;
//
//@AutoConfigureMockMvc
//@SpringBootTest
//public class ImportGoodsControllerTest {
//    @Mock
//    private ConsignmentRepository consignmentRepository;
//
//    @Mock
//    private WarehouseRepository inventoryRepository;
//
//    @Mock
//    private ProductRepository productRepository;
//
//    @Mock
//    private ImportOrderRepository orderRepository;
//
//    @Mock
//    private OrderTypeRepository orderTypeRepository;
//
//    @Mock
//    private UserRepository userRepository;
//
//    @Mock
//    private ManufacturerRepository manufacturerRepository;
//
//    @Mock
//    private OrderDetailRepository orderDetailRepository;
//
//    @Mock
//    private ImportGoodsService importGoodsService;
//    @Mock
//    private ImportOrderRepository importOrderRepository;
//    @Autowired
//    private MockMvc mockMvc;
//    @Autowired
//    private ObjectMapper objectMapper;
//
//    @BeforeAll
//    static void setUpBeforeClass() throws Exception {
//    }
//
//    @Test
//    void testImportGoodsControllerSuccess() throws Exception {
//        String billReferenceNumber = "Imp001";
//        LocalDateTime createdDate = LocalDateTime.now();
//        String description = "Nhap hang";
//        Long userId = 1L;
//        Long manufactorId = 1L;
//        Long wareHouseId = 6L;
//
//        Long consignmentId = 1L;
//        Long productId = 5L;
//        LocalDateTime expirationDate = LocalDateTime.now();
//        LocalDateTime importDate = LocalDateTime.now();
//        double unitPrice = 20.00;
//        int quantity = 10;
//
//        ConsignmentRequest consignmentRequest = new ConsignmentRequest(consignmentId, productId, expirationDate, importDate, unitPrice, quantity);
//        List<ConsignmentRequest> consignmentRequestList = new ArrayList<>();
//        consignmentRequestList.add(consignmentRequest);
//
//        ImportGoodsRequest importGoodsRequest = new ImportGoodsRequest(billReferenceNumber, createdDate, description, userId, manufactorId, wareHouseId, consignmentRequestList);
//        MessageResponse messageResponse = new MessageResponse();
//        messageResponse.setMessage("Tạo phiếu nhập kho thành công !");
//        messageResponse.setStatus(Constant.SUCCESS);
//        String uri = "/api/import-order/create";
//        ResponseEntity ResponseEntity = new ResponseEntity(messageResponse, HttpStatus.OK);
//        when(importGoodsService.save(importGoodsRequest)).thenReturn(ResponseEntity);
//        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(uri)
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(objectMapper.writeValueAsString(importGoodsRequest)))
//                .andReturn();
//        int status = mvcResult.getResponse().getStatus();
//        assertEquals(200, status);
//    }
//
//}
