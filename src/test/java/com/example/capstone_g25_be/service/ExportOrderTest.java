//package com.example.capstone_g25_be.service;
//
//import com.example.capstone_g25_be.entities.*;
//import com.example.capstone_g25_be.model.request.*;
//import com.example.capstone_g25_be.model.response.MessageResponse;
//import com.example.capstone_g25_be.repository.*;
//import com.example.capstone_g25_be.services.impl.ExportGoodsServiceImpl;
//import com.example.capstone_g25_be.utils.Constant;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import javassist.NotFoundException;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Spy;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.mockito.junit.jupiter.MockitoSettings;
//import org.mockito.quality.Strictness;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.web.client.HttpServerErrorException;
//
//import javax.transaction.SystemException;
//import java.math.BigDecimal;
//import java.math.BigInteger;
//import java.text.SimpleDateFormat;
//import java.time.LocalDate;
//import java.time.LocalDateTime;
//import java.util.*;
//
//import static org.junit.Assert.assertThrows;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.*;
//
//@ExtendWith(MockitoExtension.class)
//@MockitoSettings(strictness = Strictness.LENIENT)
//public class ExportOrderTest {
//    @Mock
//    private ConsignmentRepository consignmentRepository;
//
//    @Mock
//    private WarehouseRepository inventoryRepository;
//
//    @Mock
//    private ProductRepository productRepository;
//
//    @Mock
//    private ExportOrderRepository exportOrderRepository;
//
//    @Mock
//    private ImportOrderRepository orderRepository;
//
//    @Mock
//    private OrderTypeRepository orderTypeRepository;
//
//    @Mock
//    private UserRepository userRepository;
//
//    @Mock
//    private ManufacturerRepository manufactorRepository;
//
//    @Mock
//    private OrderDetailRepository orderDetailRepository;
//
//    @Mock
//    Constant constant;
//
//    @Spy
//    @InjectMocks
//    private ExportGoodsServiceImpl exportGoodsService;
//
//    @Autowired
//    private MockMvc mockMvc;
//    @Autowired
//    private ObjectMapper objectMapper;
//
//    @BeforeAll
//    static void setUpBeforeClass() throws Exception {
//    }
//
//    @Test
//    void testCreateImportOrderNormal() throws Exception {
//        String billReferenceNumber = "Ex001";
//        LocalDateTime createdDate = LocalDateTime.now();
//        String description = "aaa";
//        Long userId = 1L;
//        Long consignmentId = 1L;
//        double unitPrice = 20.00;
//        int quantity = 10;
//
//        ConsignmentExport consignmentExport = new ConsignmentExport(consignmentId, quantity,unitPrice);
//        List<ConsignmentExport> consignmentRequest = new ArrayList<>();
//        consignmentRequest.add(consignmentExport);
//        ExportGoodsRequest exportGoodsRequest = new ExportGoodsRequest(billReferenceNumber, createdDate, description, userId, consignmentRequest);
//
//        OrderType orderType = new OrderType(1L,"EXPORT","Xuất hàng");
//        User user = new User(userId,"Trongdd3","123456",true, LocalDate.now(),"trong@gmail.com","1234567890","0901345833","Duong Duc Trong");
//        Manufacturer manufacturer  = new Manufacturer(1L,"vu Hung","kho1@gmail","0901323");
//        Warehouse warehouse = new Warehouse(1L,"Kho 1");
//        Consignment consignment = new Consignment(consignmentId,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity);
//        OrderDetail orderDetail = new OrderDetail(5L,unitPrice,quantity);
//        Order order = new Order();
//        Product product = new Product();
//        when(orderTypeRepository.getById(any())).thenReturn(orderType);
//        when(userRepository.getById(any())).thenReturn(user);
//        when(manufactorRepository.getById(any())).thenReturn(manufacturer);
//        when(inventoryRepository.getById(any())).thenReturn(warehouse);
//        when(orderRepository.save(any())).thenReturn(order);
//        when(productRepository.getById(any())).thenReturn(product);
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        when(orderDetailRepository.save(any())).thenReturn(orderDetail);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Tạo phiếu xuất kho thành công !");
//        expected.setStatus(Constant.SUCCESS);
//        //actual
//        ResponseEntity<?> responseEntity = exportGoodsService.save(exportGoodsRequest);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call exportGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.BAD_REQUEST);
//        when(exportGoodsService.save(exportGoodsRequest)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
//    @Test
//    void testCreateImportOrderNormal1() throws Exception {
//        String billReferenceNumber = "Imp001";
//        LocalDateTime createdDate = LocalDateTime.now();
//        String description = "aaa";
//        Long userId = 1L;
//        Long consignmentId = 1L;
//        double unitPrice = 20.00;
//        int quantity = 10;
//
//        ConsignmentExport consignmentExport = new ConsignmentExport(consignmentId, quantity,unitPrice);
//        List<ConsignmentExport> consignmentRequest = new ArrayList<>();
//        consignmentRequest.add(consignmentExport);
//        ExportGoodsRequest exportGoodsRequest = new ExportGoodsRequest(billReferenceNumber, createdDate, description, userId, consignmentRequest);
//
//        OrderType orderType = new OrderType(1L,"EXPORT","Xuất hàng");
//        User user = new User(userId,"Trongdd3","123456",true, LocalDate.now(),"trong@gmail.com","1234567890","0901345833","Duong Duc Trong");
//        Manufacturer manufacturer  = new Manufacturer(1L,"vu Hung","kho1@gmail","0901323");
//        Warehouse warehouse = new Warehouse(1L,"Kho 1");
//        Consignment consignment = new Consignment(consignmentId,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity);
//        OrderDetail orderDetail = new OrderDetail(5L,unitPrice,quantity);
//        Order order = new Order();
//        Product product = new Product();
//        when(orderTypeRepository.getById(any())).thenReturn(orderType);
//        when(userRepository.getById(any())).thenReturn(user);
//        when(manufactorRepository.getById(any())).thenReturn(manufacturer);
//        when(inventoryRepository.getById(any())).thenReturn(warehouse);
//        when(orderRepository.save(any())).thenReturn(order);
//        when(productRepository.getById(any())).thenReturn(product);
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        when(orderDetailRepository.save(any())).thenReturn(orderDetail);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Tạo phiếu xuất kho thành công !");
//        expected.setStatus(Constant.SUCCESS);
//        //actual
//        ResponseEntity<?> responseEntity = exportGoodsService.save(exportGoodsRequest);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call exportGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.OK);
//        when(exportGoodsService.save(exportGoodsRequest)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
//
//    @Test
//    void testCreateImportOrderBoundary1() throws Exception {
//        String billReferenceNumber = "new";
//        LocalDateTime createdDate = LocalDateTime.now();
//        String description = "Nhap hang";
//        Long userId = 1L;
//        Long manufactorId = 1L;
//        Long wareHouseId = 6L;
//
//        Long consignmentId = 1L;
//        Long productId = 5L;
//        LocalDateTime expirationDate = LocalDateTime.now();
//        LocalDateTime importDate = LocalDateTime.now();
//        double unitPrice = 20;
//        int quantity = 2147483647;
//
//        ConsignmentExport consignmentExport = new ConsignmentExport(consignmentId, quantity,unitPrice);
//        List<ConsignmentExport> consignmentRequest = new ArrayList<>();
//        consignmentRequest.add(consignmentExport);
//        ExportGoodsRequest exportGoodsRequest = new ExportGoodsRequest(billReferenceNumber, createdDate, description, userId, consignmentRequest);
//
//        OrderType orderType = new OrderType(1L,"IMPORT","Nhập hàng");
//        User user = new User(userId,"Trongdd3","123456",true, LocalDate.now(),"trong@gmail.com","1234567890","0901345833","Duong Duc Trong");
//        Manufacturer manufacturer  = new Manufacturer(1L,"vu Hung","kho1@gmail","0901323");
//        Warehouse warehouse = new Warehouse(1L,"Kho 1");
//
//        Consignment consignment = new Consignment(consignmentId,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity);
//        OrderDetail orderDetail = new OrderDetail(5L,unitPrice,quantity);
//        Order order = new Order();
//        Product product = new Product();
//        when(orderTypeRepository.getById(any())).thenReturn(orderType);
//        when(userRepository.getById(any())).thenReturn(user);
//        when(manufactorRepository.getById(any())).thenReturn(manufacturer);
//        when(inventoryRepository.getById(any())).thenReturn(warehouse);
//        when(orderRepository.save(any())).thenReturn(order);
//        when(productRepository.getById(any())).thenReturn(product);
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        when(orderDetailRepository.save(any())).thenReturn(orderDetail);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Tạo phiếu xuất kho thành công !");
//        expected.setStatus(Constant.SUCCESS);
//        //actual
//        ResponseEntity<?> responseEntity = exportGoodsService.save(exportGoodsRequest);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call exportGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.BAD_REQUEST);
//        when(exportGoodsService.save(exportGoodsRequest)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
//
//
//    @Test
//    void testCreateImportOrderInputNull() throws Exception {
//        String billReferenceNumber = null;
//        LocalDateTime createdDate =null;
//        String description = null;
//        Long userId = null;
//        Long manufactorId = null;
//        Long wareHouseId = null;
//
//        Long consignmentId = null;
//        Long productId = null;
//        LocalDateTime expirationDate =null;
//        LocalDateTime importDate = null;
//        double unitPrice =10;
//        int quantity = 10;
//
//        ConsignmentExport consignmentExport = new ConsignmentExport(consignmentId, quantity,unitPrice);
//        List<ConsignmentExport> consignmentRequest = new ArrayList<>();
//        consignmentRequest.add(consignmentExport);
//        ExportGoodsRequest exportGoodsRequest = new ExportGoodsRequest(billReferenceNumber, createdDate, description, userId, consignmentRequest);
//
//        OrderType orderType = new OrderType(1L,"IMPORT","Nhập hàng");
//        User user = new User(userId,"Trongdd3","123456",true, LocalDate.now(),"trong@gmail.com","1234567890","0901345833","Duong Duc Trong");
//        Manufacturer manufacturer  = new Manufacturer(1L,"vu Hung","kho1@gmail","0901323");
//        Warehouse warehouse = new Warehouse(1L,"Kho 1");
//
//        Consignment consignment = new Consignment(consignmentId,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity);
//        OrderDetail orderDetail = new OrderDetail(5L,unitPrice,quantity);
//        Order order = new Order();
//        Product product = new Product();
//        when(orderTypeRepository.getById(any())).thenReturn(orderType);
//        when(userRepository.getById(any())).thenReturn(user);
//        when(manufactorRepository.getById(any())).thenReturn(manufacturer);
//        when(inventoryRepository.getById(any())).thenReturn(warehouse);
//        when(orderRepository.save(any())).thenReturn(order);
//        when(productRepository.getById(any())).thenReturn(product);
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        when(orderDetailRepository.save(any())).thenReturn(orderDetail);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Tạo phiếu xuất kho thất bại !");
//        expected.setStatus(Constant.FAIL);
//        //actual
//        ResponseEntity<?> responseEntity = exportGoodsService.save(null);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call exportGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.BAD_REQUEST);
//        when(exportGoodsService.save(exportGoodsRequest)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
//    //update
//    @Test
//    void testUpdateImportOrderNormal() throws Exception {
//        Long orderId = 1L;
//        String billReferenceNumber = "aa";
//        String description = "nhap hang";
//        Long userId = 1L;
//
//        Long consignmentId = 1L;
//        double unitPrice = 20;
//        int quantity = 10;
//        ConsignmentExport consignmentExport = new ConsignmentExport(consignmentId, quantity,unitPrice);
//        List<ConsignmentExport> consignmentRequest = new ArrayList<>();
//        consignmentRequest.add(consignmentExport);
//        UpdateExportOrder updateExportOrder = new UpdateExportOrder(orderId, billReferenceNumber, description, userId, consignmentRequest);
//
//        OrderType orderType = new OrderType(1L,"IMPORT","Nhập hàng");
//        User user = new User(userId,"Trongdd3","123456",true, LocalDate.now(),"trong@gmail.com","1234567890","0901345833","Duong Duc Trong");
//        Manufacturer manufacturer  = new Manufacturer(1L,"vu Hung","kho1@gmail","0901323");
//        Warehouse warehouse = new Warehouse();
//        OrderDetail orderDetail = new OrderDetail(5L,unitPrice,quantity);
//        Consignment consignment = new Consignment(consignmentId,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity);
//        Order order = new Order();
//        Product product = new Product();
//        when(orderRepository.findById(any())).thenReturn(Optional.of(order));
//        when(consignmentRepository.getById(any())).thenReturn(consignment);
//        when(orderDetailRepository.findByOrder_IdAndConsignment_Id(any(),any())).thenReturn(orderDetail);
//        when(productRepository.getById(any())).thenReturn(product);
//        when(inventoryRepository.getById(any())).thenReturn(warehouse);
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        when(orderTypeRepository.getById(any())).thenReturn(orderType);
//        when(userRepository.getById(any())).thenReturn(user);
//        when(manufactorRepository.getById(any())).thenReturn(manufacturer);
//        when(orderRepository.save(any())).thenReturn(order);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Cập nhật phiếu xuất kho thành công !");
//        expected.setStatus(Constant.SUCCESS);
//        //actual
//        ResponseEntity<?> responseEntity = exportGoodsService.update(updateExportOrder);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call exportGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.OK);
//        when(exportGoodsService.update(updateExportOrder)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
//
//    @Test
//    void testUpdateImportOrderIdNull() throws Exception {
//        Long orderId = 1L;
//        String billReferenceNumber = "";
//        String description = "";
//        Long userId = 1L;
//
//        Long consignmentId = 1L;
//        double unitPrice = 20;
//        int quantity = 10;
//        ConsignmentExport consignmentExport = new ConsignmentExport(consignmentId, quantity,unitPrice);
//        List<ConsignmentExport> consignmentRequest = new ArrayList<>();
//        consignmentRequest.add(consignmentExport);
//        UpdateExportOrder updateExportOrder = new UpdateExportOrder(orderId, billReferenceNumber, description, userId, consignmentRequest);
//
//        OrderType orderType = new OrderType(1L,"IMPORT","Nhập hàng");
//        User user = new User(userId,"Trongdd3","123456",true, LocalDate.now(),"trong@gmail.com","1234567890","0901345833","Duong Duc Trong");
//        Manufacturer manufacturer  = new Manufacturer(1L,"vu Hung","kho1@gmail","0901323");
//        Warehouse warehouse = new Warehouse();
//        OrderDetail orderDetail = new OrderDetail(5L,unitPrice,quantity);
//        Consignment consignment = new Consignment(consignmentId,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity);
//        Order order = new Order();
//        Product product = new Product();
//        when(orderRepository.findById(any())).thenReturn(Optional.of(order));
//        when(consignmentRepository.getById(any())).thenReturn(consignment);
//        when(orderDetailRepository.findByOrder_IdAndConsignment_Id(any(),any())).thenReturn(orderDetail);
//        when(productRepository.getById(any())).thenReturn(product);
//        when(inventoryRepository.getById(any())).thenReturn(warehouse);
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        when(orderTypeRepository.getById(any())).thenReturn(orderType);
//        when(userRepository.getById(any())).thenReturn(user);
//        when(manufactorRepository.getById(any())).thenReturn(manufacturer);
//        when(orderRepository.save(any())).thenReturn(order);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Cập nhật phiếu xuất kho thành công !");
//        expected.setStatus(Constant.SUCCESS);
//        //actual
//        ResponseEntity<?> responseEntity = exportGoodsService.update(updateExportOrder);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call exportGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.OK);
//        when(exportGoodsService.update(updateExportOrder)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
//
//  //  Confirm
//    @Test
//    void testConfirmOrderNormal() throws Exception {
//        Long orderId = 1L;
//        Long confirmUserId = 1L;
//
//        Long consignmentId =1L;
//        double unitPrice = 20;
//        int quantity = 10;
//        Status status = new Status();
//        Order order = new Order();
//        order.setStatus(status);
//        Product product = new Product(1L,"Gach xay","Gach001","10","10",10,"vang","image1","hang dep",20.00);
//        Consignment consignment = new Consignment(1L,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity,product);
//        OrderDetail orderDetail = new OrderDetail(1L,unitPrice,quantity,order,consignment);
//        List<OrderDetail> orderDetails = new ArrayList<>();
//        orderDetails.add(orderDetail);
//
//        Map<String,Object> consignmentMap = new HashMap<>();
//        consignmentMap.put("quantity",100);
//        consignmentMap.put("consignmentId",new BigInteger(String.valueOf(1)));
//        List<Map<String,Object>> consignmentList = new ArrayList<>();
//        consignmentList.add(consignmentMap);
//        when(exportOrderRepository.getListConsignmentByOrderId(any()) ).thenReturn(consignmentList);
//        when(orderRepository.getById(any())).thenReturn(order);
//        when( consignmentRepository.getById(any())).thenReturn(consignment);
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Số lượng hàng trong kho không đủ để xuất kho !");
//        expected.setStatus(409);
//        //actual
//
//        ResponseEntity<?> responseEntity = exportGoodsService.confirm(orderId,confirmUserId);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call importGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.BAD_REQUEST);
//        when(exportGoodsService.confirm(orderId,confirmUserId)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
//    @Test
//    void testConfirmOrderNormal1() throws Exception {
//        Long orderId = 1L;
//        Long confirmUserId = 1L;
//        double unitPrice = 20;
//        int quantity = 10;
//        Status status = new Status();
//        Order order1 = new Order();
//
//        status.setId(Constant.COMPLETED);
//        User user = new User();
//        Manufacturer manufacturer = new Manufacturer();
//        OrderType orderType = new OrderType();
//        Product product = new Product(1L,"Gach xay","Gach001","10","10",10,"vang","image1","hang dep",20.00);
//        Consignment consignment = new Consignment(1L,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity,product);
//        OrderDetail orderDetail = new OrderDetail(1L,unitPrice,quantity,order1,consignment);
//        Set<OrderDetail> orderDetails1 = new HashSet<>();
//        orderDetails1.add(orderDetail);
//        List<OrderDetail> orderDetails = new ArrayList<>();
//        orderDetails.add(orderDetail);
//        Order order = new Order(orderId,LocalDateTime.now(),LocalDateTime.now(),LocalDateTime.now(),
//                1L,"nhap hang","hang tot",true,true,orderDetails1,user,manufacturer,orderType,status);
//
//        when(orderRepository.getById(any())).thenReturn(order1);
//        when(orderDetailRepository.findOrderDetailByOrderId(any())).thenReturn(orderDetails);
//        when(consignmentRepository.findConsignmentById(any())).thenReturn(consignment);
//        when(consignmentRepository.getById(any())).thenReturn(consignment);
//        when(productRepository.findProductById(any())).thenReturn(product);
//        when(consignmentRepository.countQuantityWithConsignmentNotImport(any())).thenReturn(consignment);
//        when(consignmentRepository.countQuantity(any())).thenReturn(quantity);
//
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        when(productRepository.save(any())).thenReturn(product);
//        when(orderRepository.save(any())).thenReturn(order);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Xác nhận phiếu xuất kho thành công !");
//        expected.setStatus(Constant.SUCCESS);
//        //actual
//
//        ResponseEntity<?> responseEntity = exportGoodsService.confirm(orderId,confirmUserId);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call importGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.BAD_REQUEST);
//        when(exportGoodsService.confirm(orderId,confirmUserId)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
//   //test cancel Order
//
//    @Test
//    void testCancelImportOrder() throws Exception {
//        Long orderId = 1L;
//        Order order = new Order();
//        when(exportOrderRepository.findById(any())).thenReturn(Optional.of(order));
//        when(orderRepository.save(any())).thenReturn(order);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        ResponseEntity<?> responseEntity = exportGoodsService.cancel(orderId);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call exportGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.BAD_REQUEST);
//        when(exportGoodsService.cancel(orderId)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//
//    }
//    @Test
//    void testCancelImportOrderAbnormal() throws Exception {
//        Long orderId = 1L;
//        Order order = new Order();
//        when(exportOrderRepository.findById(any())).thenReturn(Optional.of(order));
//        when(orderRepository.save(any())).thenReturn(order);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        ResponseEntity<?> responseEntity = exportGoodsService.cancel(orderId);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call exportGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.BAD_REQUEST);
//        when(exportGoodsService.cancel(orderId)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//
//    }
//
//    @Test
//    void testCancelImportOrderBoudary() throws Exception {
//        Long orderId = 2L;
//        Order order = new Order();
//        when(exportOrderRepository.findById(any())).thenReturn(Optional.of(order));
//        when(orderRepository.save(any())).thenReturn(order);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        ResponseEntity<?> responseEntity = exportGoodsService.cancel(orderId);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call exportGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.BAD_REQUEST);
//        when(exportGoodsService.cancel(orderId)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//
//    }
//
//    @Test
//    void testgetListExportOrder() throws Exception {
//        Long userId =1L;
//        String billReferenceNumber = "nhap";
//        String startDate1 = "01-01-2022";
//        String endDate1 = "01-10-2022";
//        Date startDate =new SimpleDateFormat("dd-MM-yyyy").parse(startDate1);
//        Date endDate= new SimpleDateFormat("dd-MM-yyyy").parse(endDate1);
//        Integer status = 1;
//        Integer pageIndex = 1;
//        Integer pageSize = 10;
//        Map<String, Object> map = new HashMap<>();
//        map.put("orderId",2L);
//
//        List<Map<String, Object>> orderList = new ArrayList<>();
//        orderList.add(map);
//        when(exportOrderRepository.getListExportBySearchData(any(),any(),any(),any(),any(),any())).thenReturn(orderList);
//
//        //expected
//        ServiceResult<Map<String, Object>> expected = new ServiceResult<>();
//        expected.setMessage("success");
//        expected.setStatus(HttpStatus.OK);
//        //actual
//        ServiceResult<Map<String, Object>> actual = exportGoodsService.getListExportOrder(userId,billReferenceNumber,startDate,endDate,status,pageIndex,pageSize);
//        //call getListImportGoods
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//
//    }
//    @Test
//    void testgetListExportOrder1() throws Exception {
//        Long userId =1L;
//        String billReferenceNumber = "nhap";
//        String startDate1 = "01-01-2022";
//        String endDate1 = "01-10-2022";
//        Date startDate =new SimpleDateFormat("dd-MM-yyyy").parse(startDate1);
//        Date endDate= new SimpleDateFormat("dd-MM-yyyy").parse(endDate1);
//        Integer status = 1;
//        Integer pageIndex = 1;
//        Integer pageSize = 10;
//        Map<String, Object> map = new HashMap<>();
//        map.put("orderId",2L);
//
////        List<Map<String, Object>> orderList = null;
////        orderList.add(map);
//        when(exportOrderRepository.getListExportBySearchData(any(),any(),any(),any(),any(),any()))
//                .thenThrow(new IllegalArgumentException());
//
//        //expected
//        ServiceResult<Map<String, Object>> expected = new ServiceResult<>();
//        expected.setMessage("fail");
//        expected.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
//        //actual
//        ServiceResult<Map<String, Object>> actual = exportGoodsService.getListExportOrder(userId,billReferenceNumber,startDate,endDate,status,pageIndex,pageSize);
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//
//    }
//
//    @Test
//    void testGetListProductByImportDate() throws Exception {
//        Map<String,Object> output = new HashMap<>();
//        List<Map<String,Object>> list = new ArrayList<>();
//        list.add(output);
//        when(exportOrderRepository.getListProductByImportDate()).thenReturn(list);
//
//        //expected
//        ServiceResult<Map<String, Object>> expected = new ServiceResult<>();
//        expected.setMessage("success");
//        expected.setStatus1(200);
//        //actual
//        Map<String, Object> actual = exportGoodsService.getListProductByImportDate();
//        //call getListImportGoods
//
//        assertEquals(expected.getMessage(), actual.get("message"));
//        assertEquals(expected.getStatus1(), actual.get("status"));
//
//    }
//    @Test
//    void testGetListProductByImportDateAbnomal() throws Exception {
//        Map<String,Object> output = new HashMap<>();
//        List<Map<String,Object>> list = new ArrayList<>();
//        when(exportOrderRepository.getListProductByImportDate()).thenThrow(new NullPointerException());
//
//        //expected
//        ServiceResult<Map<String, Object>> expected = new ServiceResult<>();
//        expected.setMessage("fail");
//        expected.setStatus(HttpStatus.BAD_REQUEST);
//        //actual
//        Map<String, Object> actual = exportGoodsService.getListProductByImportDate();
//        //call getListImportGoods
//
//        assertEquals(expected.getMessage(), actual.get("message"));
//        assertEquals(expected.getStatus(), actual.get("status"));
//
//    }
//    @Test
//    void testGetProductInstock() throws Exception {
//        List<Map<String,Object>> list = new ArrayList<>();
//        Long productId1 = 1L;
//        Integer pageIndex = 1;
//        Integer pageSize = 10;
//        Map<String,Object> output = new HashMap<>();
//        String productName = "Gauche";
//        List<Map<String,Object>> addressWarehouse = new ArrayList<>();
//        BigDecimal quantity = new BigDecimal(1);
//        BigInteger productId = new BigInteger(String.valueOf(1L));
//        output.put("productId",productId);
//        output.put("productCode",1);
//        output.put("productName",productName);
//        output.put("unitMeasure","10");
//        output.put("unitPrice",10.00);
//        output.put("quantity",quantity);
//        output.put("totalPrice",10.00);
//        output.put("numberOfWrapUnitMeasure",10);
//        output.put("wrapUnitMeasure","Hop");
//        output.put("warehouseName","Kho 1");
//        output.put("quantityInstock",10);
//        list.add(output);
//        when(exportOrderRepository.getProductInstock(any(),any())).thenReturn(list);
//        when(exportOrderRepository.getListAddressWarehouseByListId(any())).thenReturn(addressWarehouse);
//        //expected
//        ServiceResult<Map<String, Object>> expected = new ServiceResult<>();
//        expected.setMessage("success");
//        expected.setStatus1(200);
//        //actual
//        Map<String, Object> actual = exportGoodsService.getProductInstock(productId1,pageIndex,pageSize);
//
//        assertEquals(expected.getStatus1(), 200);
//
//    }
//
//    @Test
//    void testGetDetailExportOrder1() throws Exception {
//        Long orderId =1L;
//        Map<String, Object> map = new HashMap<>();
//        map.put("orderId",2L);
//        when(orderRepository.getOrderDetail(any())).thenReturn(map);
//
//        //expected
//        ServiceResult<Map<String, Object>> expected = new ServiceResult<>();
//        expected.setMessage("success");
//        expected.setStatus(HttpStatus.OK);
//        //actual
//        ServiceResult<Map<String, Object>> actual = exportGoodsService.getExportOrderDetail(orderId);
//        //call getListImportGoods
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//
//    }
//
//    @Test
//    void testGetlistProductOfExportGoods() throws Exception {
//        Long orderId =1L;
//        Integer pageIndex = 1;
//        Integer pageSize = 10;
//        Map<String,Object> output = new HashMap<>();
//        Map<String,Object> output1 = new HashMap<>();
//        List<Map<String,Object>> list = new ArrayList<>();
//        BigDecimal quantity = new BigDecimal(1);
//        BigInteger productId = new BigInteger(String.valueOf(1L));
//        output.put("productId",productId);
//        output.put("productCode","1");
//        output.put("productName","12");
//        output.put("unitMeasure","10");
//        output.put("unitPrice",10.00);
//        output.put("quantity",quantity);
//        output.put("totalPrice",10.00);
//        output.put("numberOfWrapUnitMeasure",10);
//        output.put("wrapUnitMeasure","Hop");
//        list.add(output);
//        List<Map<String,Object>> listConsignment = new ArrayList<>();
//        output1.put("consignmentId",1);
//        output1.put("warehouseId",1);
//        output1.put("warehouseName","Kho 1");
//        output1.put("quantity",1);
//        output1.put("quantityInstock",10);
//        listConsignment.add(output1);
//        when(exportOrderRepository.getDetailExportGoods2(any(),any())).thenReturn(list);
//        when(exportOrderRepository.getListConsignment(any())).thenReturn(listConsignment);
//        when(exportOrderRepository.getListAddressWarehouseByListId(any())).thenReturn(list);
//
//        //expected
//        ServiceResult<Map<String, Object>> expected = new ServiceResult<>();
//        expected.setMessage("success");
//        expected.setStatus(HttpStatus.OK);
//        //actual
//        ServiceResult<Map<String, Object>> actual = exportGoodsService.getlistProductOfExportGoods(orderId,pageIndex,pageSize);
//        //call getListImportGoods
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//
//    }
//
//
//    @Test
//    void testGetlistProductOfExportGoodsAbnormal() throws Exception {
//        Long orderId =1L;
//        Integer pageIndex = 1;
//        Integer pageSize = 10;
//        Map<String,Object> output = new HashMap<>();
//        Map<String,Object> output1 = new HashMap<>();
//        List<Map<String,Object>> list = new ArrayList<>();
//        BigDecimal quantity = new BigDecimal(1);
//        BigInteger productId = new BigInteger(String.valueOf(1L));
//        output.put("productId",productId);
//        output.put("productCode","1");
//        output.put("productName","12");
//        output.put("unitMeasure","10");
//        output.put("unitPrice",10.00);
//        output.put("quantity",quantity);
//        output.put("totalPrice",10.00);
//        output.put("numberOfWrapUnitMeasure",10);
//        output.put("wrapUnitMeasure","Hop");
//        list.add(output);
//        List<Map<String,Object>> listConsignment = null;
//        when(exportOrderRepository.getDetailExportGoods2(any(),any())).thenReturn(list);
//        when(exportOrderRepository.getListConsignment(any())).thenReturn(listConsignment);
//        when(exportOrderRepository.getListAddressWarehouseByListId(any())).thenReturn(list);
//
//        //expected
//        ServiceResult<Map<String, Object>> expected = new ServiceResult<>();
//        expected.setMessage("fail");
//        expected.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
//        //actual
//        ServiceResult<Map<String, Object>> actual = exportGoodsService.getlistProductOfExportGoods(orderId,pageIndex,pageSize);
//        //call getListImportGoods
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//
//    }
//
//}
