//package com.example.capstone_g25_be.service;
//
//import com.example.capstone_g25_be.entities.*;
//import com.example.capstone_g25_be.model.request.ConsignmentRequest;
//import com.example.capstone_g25_be.model.request.ImportGoodsRequest;
//import com.example.capstone_g25_be.model.request.UpdateImportGoodsRequest;
//import com.example.capstone_g25_be.model.response.MessageResponse;
//import com.example.capstone_g25_be.repository.*;
//import com.example.capstone_g25_be.services.ImportGoodsService;
//import com.example.capstone_g25_be.services.impl.ImportGoodsServiceImpl;
//import com.example.capstone_g25_be.utils.Constant;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Spy;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.mockito.junit.jupiter.MockitoSettings;
//import org.mockito.quality.Strictness;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.data.domain.Pageable;
//import org.springframework.data.domain.Sort;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.web.servlet.MockMvc;
//
//import java.text.SimpleDateFormat;
//import java.time.LocalDate;
//import java.time.LocalDateTime;
//import java.util.*;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.*;
//
//@ExtendWith(MockitoExtension.class)
//@MockitoSettings(strictness = Strictness.LENIENT)
//public class ImportGoodsServiceTest {
//    @Mock
//    private ConsignmentRepository consignmentRepository;
//
//    @Mock
//    Constant constant;
//    @Mock
//    private WarehouseRepository inventoryRepository;
//
//    @Mock
//    private ProductRepository productRepository;
//
//    @Mock
//    private ImportOrderRepository orderRepository;
//
//    @Mock
//    private OrderTypeRepository orderTypeRepository;
//
//    @Mock
//    private UserRepository userRepository;
//
//    @Mock
//    private ManufacturerRepository manufactorRepository;
//
//    @Mock
//    private OrderDetailRepository orderDetailRepository;
//
//    @Spy
//    @InjectMocks
//    private ImportGoodsServiceImpl importGoodsService;
//
//    @Autowired
//    private MockMvc mockMvc;
//    @Autowired
//    private ObjectMapper objectMapper;
//
//    @BeforeAll
//    static void setUpBeforeClass() throws Exception {
//    }
//
//    @Test
//    void testCreateImportOrderNormal() throws Exception {
//        String billReferenceNumber = "Imp001";
//        LocalDateTime createdDate = LocalDateTime.now();
//        String description = "aaa";
//        Long userId = 1L;
//        Long manufactorId = 1L;
//        Long wareHouseId = 6L;
//
//        Long consignmentId = 1L;
//        Long productId = 5L;
//        LocalDateTime expirationDate = LocalDateTime.now();
//        LocalDateTime importDate = LocalDateTime.now();
//        double unitPrice = 20.00;
//        int quantity = 10;
//
//        ConsignmentRequest consignmentRequest = new ConsignmentRequest(consignmentId, productId, expirationDate, importDate, unitPrice, quantity);
//        List<ConsignmentRequest> consignmentRequestList = new ArrayList<>();
//        consignmentRequestList.add(consignmentRequest);
//
//        ImportGoodsRequest importGoodsRequest = new ImportGoodsRequest(billReferenceNumber, createdDate, description, userId, manufactorId, wareHouseId, consignmentRequestList);
//
//        OrderType orderType = new OrderType(1L,"IMPORT","Nhập hàng");
//        User user = new User(userId,"Trongdd3","123456",true, LocalDate.now(),"trong@gmail.com","1234567890","0901345833","Duong Duc Trong");
//        Manufacturer manufacturer  = new Manufacturer(1L,"vu Hung","kho1@gmail","0901323");
//        Warehouse warehouse = new Warehouse(1L,"Kho 1");
//        Consignment consignment = new Consignment(consignmentId,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity);
//        OrderDetail orderDetail = new OrderDetail(5L,unitPrice,quantity);
//        Order order = new Order();
//        Product product = new Product();
//        when(orderTypeRepository.getById(any())).thenReturn(orderType);
//        when(userRepository.getById(any())).thenReturn(user);
//        when(manufactorRepository.getById(any())).thenReturn(manufacturer);
//        when(inventoryRepository.getById(any())).thenReturn(warehouse);
//        when(orderRepository.save(any())).thenReturn(order);
//        when(productRepository.getById(any())).thenReturn(product);
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        when(orderDetailRepository.save(any())).thenReturn(orderDetail);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Tạo phiếu nhập kho thành công !");
//        expected.setStatus(Constant.SUCCESS);
//        //actual
//        ResponseEntity<?> responseEntity = importGoodsService.save(importGoodsRequest);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call importGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.OK);
//        when(importGoodsService.save(importGoodsRequest)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
//    @Test
//    void testCreateImportOrderNormal1() throws Exception {
//        String billReferenceNumber = "Imp001";
//        LocalDateTime createdDate = LocalDateTime.now();
//        String description = "";
//        Long userId = 1L;
//        Long manufactorId = 1L;
//        Long wareHouseId = 6L;
//
//        Long consignmentId = 1L;
//        Long productId = 5L;
//        LocalDateTime expirationDate = LocalDateTime.now();
//        LocalDateTime importDate = LocalDateTime.now();
//        double unitPrice = 20.00;
//        int quantity = 10;
//
//        ConsignmentRequest consignmentRequest = new ConsignmentRequest(consignmentId, productId, expirationDate, importDate, unitPrice, quantity);
//        List<ConsignmentRequest> consignmentRequestList = new ArrayList<>();
//        consignmentRequestList.add(consignmentRequest);
//
//        ImportGoodsRequest importGoodsRequest = new ImportGoodsRequest(billReferenceNumber, createdDate, description, userId, manufactorId, wareHouseId, consignmentRequestList);
//
//        OrderType orderType = new OrderType(1L,"IMPORT","Nhập hàng");
//        User user = new User(userId,"Trongdd3","123456",true, LocalDate.now(),"trong@gmail.com","1234567890","0901345833","Duong Duc Trong");
//        Manufacturer manufacturer  = new Manufacturer(1L,"vu Hung","kho1@gmail","0901323");
//        Warehouse warehouse = new Warehouse(1L,"Kho 1");
//        Consignment consignment = new Consignment(consignmentId,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity);
//        OrderDetail orderDetail = new OrderDetail(5L,unitPrice,quantity);
//        Order order = new Order();
//        Product product = new Product();
//        when(orderTypeRepository.getById(any())).thenReturn(orderType);
//        when(userRepository.getById(any())).thenReturn(user);
//        when(manufactorRepository.getById(any())).thenReturn(manufacturer);
//        when(inventoryRepository.getById(any())).thenReturn(warehouse);
//        when(orderRepository.save(any())).thenReturn(order);
//        when(productRepository.getById(any())).thenReturn(product);
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        when(orderDetailRepository.save(any())).thenReturn(orderDetail);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Tạo phiếu nhập kho thành công !");
//        expected.setStatus(Constant.SUCCESS);
//        //actual
//        ResponseEntity<?> responseEntity = importGoodsService.save(importGoodsRequest);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call importGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.OK);
//        when(importGoodsService.save(importGoodsRequest)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
//
//    @Test
//    void testCreateImportOrderAbnormal() throws Exception {
//        String billReferenceNumber = "";
//        LocalDateTime createdDate = LocalDateTime.now();
//        String description = "abc";
//        Long userId = 1L;
//        Long manufactorId = 1L;
//        Long wareHouseId = 1L;
//
//        Long consignmentId = 1L;
//        Long productId = 1L;
//        LocalDateTime expirationDate = LocalDateTime.now();
//        LocalDateTime importDate = LocalDateTime.now();
//        double unitPrice = 20;
//        int quantity = 0;
//
//        ConsignmentRequest consignmentRequest = new ConsignmentRequest(consignmentId, productId, expirationDate, importDate, unitPrice, quantity);
//        List<ConsignmentRequest> consignmentRequestList = new ArrayList<>();
//        consignmentRequestList.add(consignmentRequest);
//
//        ImportGoodsRequest importGoodsRequest = new ImportGoodsRequest(billReferenceNumber, createdDate, description, userId, manufactorId, wareHouseId, consignmentRequestList);
//
//        OrderType orderType = new OrderType(1L,"IMPORT","Nhập hàng");
//        User user = new User(userId,"Trongdd3","123456",true, LocalDate.now(),"trong@gmail.com","1234567890","0901345833","Duong Duc Trong");
//        Manufacturer manufacturer  = new Manufacturer(1L,"vu Hung","kho1@gmail","0901323");
//        Warehouse warehouse = new Warehouse(1L,"Kho 1");
//
//        Consignment consignment = new Consignment(consignmentId,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity);
//        OrderDetail orderDetail = new OrderDetail(5L,unitPrice,quantity);
//        Order order = new Order();
//        Product product = new Product();
//        when(orderTypeRepository.getById(any())).thenReturn(orderType);
//        when(userRepository.getById(any())).thenReturn(user);
//        when(manufactorRepository.getById(any())).thenReturn(manufacturer);
//        when(inventoryRepository.getById(any())).thenReturn(warehouse);
//        when(orderRepository.save(any())).thenReturn(order);
//        when(productRepository.getById(any())).thenReturn(product);
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        when(orderDetailRepository.save(any())).thenReturn(orderDetail);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Lỗi số lượng nhập vào !");
//        expected.setStatus(Constant.FAIL);
//        //actual
//        ResponseEntity<?> responseEntity = importGoodsService.save(importGoodsRequest);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call importGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.BAD_REQUEST);
//        when(importGoodsService.save(importGoodsRequest)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
//
//    @Test
//    void testCreateImportOrderBoundary1() throws Exception {
//        String billReferenceNumber = "new";
//        LocalDateTime createdDate = LocalDateTime.now();
//        String description = "Nhap hang";
//        Long userId = 1L;
//        Long manufactorId = 1L;
//        Long wareHouseId = 6L;
//
//        Long consignmentId = 1L;
//        Long productId = 5L;
//        LocalDateTime expirationDate = LocalDateTime.now();
//        LocalDateTime importDate = LocalDateTime.now();
//        double unitPrice = 20;
//        int quantity = 2147483647;
//
//        ConsignmentRequest consignmentRequest = new ConsignmentRequest(consignmentId, productId, expirationDate, importDate, unitPrice, quantity);
//        List<ConsignmentRequest> consignmentRequestList = new ArrayList<>();
//        consignmentRequestList.add(consignmentRequest);
//
//        ImportGoodsRequest importGoodsRequest = new ImportGoodsRequest(billReferenceNumber, createdDate, description, userId, manufactorId, wareHouseId, consignmentRequestList);
//
//        OrderType orderType = new OrderType(1L,"IMPORT","Nhập hàng");
//        User user = new User(userId,"Trongdd3","123456",true, LocalDate.now(),"trong@gmail.com","1234567890","0901345833","Duong Duc Trong");
//        Manufacturer manufacturer  = new Manufacturer(1L,"vu Hung","kho1@gmail","0901323");
//        Warehouse warehouse = new Warehouse(1L,"Kho 1");
//
//        Consignment consignment = new Consignment(consignmentId,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity);
//        OrderDetail orderDetail = new OrderDetail(5L,unitPrice,quantity);
//        Order order = new Order();
//        Product product = new Product();
//        when(orderTypeRepository.getById(any())).thenReturn(orderType);
//        when(userRepository.getById(any())).thenReturn(user);
//        when(manufactorRepository.getById(any())).thenReturn(manufacturer);
//        when(inventoryRepository.getById(any())).thenReturn(warehouse);
//        when(orderRepository.save(any())).thenReturn(order);
//        when(productRepository.getById(any())).thenReturn(product);
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        when(orderDetailRepository.save(any())).thenReturn(orderDetail);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Tạo phiếu nhập kho thành công !");
//        expected.setStatus(Constant.SUCCESS);
//        //actual
//        ResponseEntity<?> responseEntity = importGoodsService.save(importGoodsRequest);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call importGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.OK);
//        when(importGoodsService.save(importGoodsRequest)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
//
//    @Test
//    void testCreateImportOrderBoundary2() throws Exception {
//        String billReferenceNumber = "new";
//        LocalDateTime createdDate = LocalDateTime.now();
//        String description = "Nhap hang";
//        Long userId = 1L;
//        Long manufactorId = 1L;
//        Long wareHouseId = 6L;
//
//        Long consignmentId = 1L;
//        Long productId = 5L;
//        LocalDateTime expirationDate = LocalDateTime.now();
//        LocalDateTime importDate = LocalDateTime.now();
//        double unitPrice = 20;
//        int quantity = -10;
//
//        ConsignmentRequest consignmentRequest = new ConsignmentRequest(consignmentId, productId, expirationDate, importDate, unitPrice, quantity);
//        List<ConsignmentRequest> consignmentRequestList = new ArrayList<>();
//        consignmentRequestList.add(consignmentRequest);
//
//        ImportGoodsRequest importGoodsRequest = new ImportGoodsRequest(billReferenceNumber, createdDate, description, userId, manufactorId, wareHouseId, consignmentRequestList);
//
//        OrderType orderType = new OrderType(1L,"IMPORT","Nhập hàng");
//        User user = new User(userId,"Trongdd3","123456",true, LocalDate.now(),"trong@gmail.com","1234567890","0901345833","Duong Duc Trong");
//        Manufacturer manufacturer  = new Manufacturer(1L,"vu Hung","kho1@gmail","0901323");
//        Warehouse warehouse = new Warehouse(1L,"Kho 1");
//
//        Consignment consignment = new Consignment(consignmentId,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity);
//        OrderDetail orderDetail = new OrderDetail(5L,unitPrice,quantity);
//        Order order = new Order();
//        Product product = new Product();
//        when(orderTypeRepository.getById(any())).thenReturn(orderType);
//        when(userRepository.getById(any())).thenReturn(user);
//        when(manufactorRepository.getById(any())).thenReturn(manufacturer);
//        when(inventoryRepository.getById(any())).thenReturn(warehouse);
//        when(orderRepository.save(any())).thenReturn(order);
//        when(productRepository.getById(any())).thenReturn(product);
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        when(orderDetailRepository.save(any())).thenReturn(orderDetail);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Lỗi số lượng nhập vào !");
//        expected.setStatus(Constant.FAIL);
//        //actual
//        ResponseEntity<?> responseEntity = importGoodsService.save(importGoodsRequest);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call importGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.BAD_REQUEST);
//        when(importGoodsService.save(importGoodsRequest)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
//
//    @Test
//    void testCreateImportOrderInputNull() throws Exception {
//        String billReferenceNumber = null;
//        LocalDateTime createdDate =null;
//        String description = null;
//        Long userId = null;
//        Long manufactorId = null;
//        Long wareHouseId = null;
//
//        Long consignmentId = null;
//        Long productId = null;
//        LocalDateTime expirationDate =null;
//        LocalDateTime importDate = null;
//        double unitPrice =10;
//        int quantity = 10;
//
//        ConsignmentRequest consignmentRequest = new ConsignmentRequest(consignmentId, productId, expirationDate, importDate, unitPrice, quantity);
//        List<ConsignmentRequest> consignmentRequestList = new ArrayList<>();
//        consignmentRequestList.add(consignmentRequest);
//
//        ImportGoodsRequest importGoodsRequest = new ImportGoodsRequest(billReferenceNumber, createdDate, description, userId, manufactorId, wareHouseId, consignmentRequestList);
//
//        OrderType orderType = new OrderType(1L,"IMPORT","Nhập hàng");
//        User user = new User(userId,"Trongdd3","123456",true, LocalDate.now(),"trong@gmail.com","1234567890","0901345833","Duong Duc Trong");
//        Manufacturer manufacturer  = new Manufacturer(1L,"vu Hung","kho1@gmail","0901323");
//        Warehouse warehouse = new Warehouse(1L,"Kho 1");
//
//        Consignment consignment = new Consignment(consignmentId,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity);
//        OrderDetail orderDetail = new OrderDetail(5L,unitPrice,quantity);
//        Order order = new Order();
//        Product product = new Product();
//        when(orderTypeRepository.getById(any())).thenReturn(orderType);
//        when(userRepository.getById(any())).thenReturn(user);
//        when(manufactorRepository.getById(any())).thenReturn(manufacturer);
//        when(inventoryRepository.getById(any())).thenReturn(warehouse);
//        when(orderRepository.save(any())).thenReturn(order);
//        when(productRepository.getById(any())).thenReturn(product);
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        when(orderDetailRepository.save(any())).thenReturn(orderDetail);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Tạo phiếu nhập kho thất bại !");
//        expected.setStatus(Constant.FAIL);
//        //actual
//        ResponseEntity<?> responseEntity = importGoodsService.save(null);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call importGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.BAD_REQUEST);
//        when(importGoodsService.save(importGoodsRequest)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
//    //update
//
//    @Test
//    void testUpdateImportOrderNormal0() throws Exception {
//        Long orderId = 1L;
//        String billReferenceNumber = "aa";
//        LocalDateTime createdDate = LocalDateTime.now();
//        LocalDateTime confirmDate= LocalDateTime.now();
//        String description = "nhap hang";
//        Long userId = 1L;
//        Long manufactorId= 1L;
//        Long wareHouseId= 5L;
//
//        Long consignmentId = 1L;
//        Long productId = 5L;
//        LocalDateTime expirationDate = null;
//        LocalDateTime importDate = LocalDateTime.now();
//        double unitPrice = 20;
//        int quantity = 10;
//        ConsignmentRequest consignmentRequest = new ConsignmentRequest(consignmentId, productId, expirationDate, importDate, unitPrice, quantity);
//
//        List<ConsignmentRequest> consignmentRequestList = new ArrayList<>();
//        consignmentRequestList.add(consignmentRequest);
//        UpdateImportGoodsRequest updateImportGoodsRequest = new UpdateImportGoodsRequest(orderId,billReferenceNumber,createdDate,confirmDate
//                ,description,userId,manufactorId,wareHouseId,consignmentRequestList);
//
//        OrderType orderType = new OrderType(1L,"IMPORT","Nhập hàng");
//        User user = new User(userId,"Trongdd3","123456",true, LocalDate.now(),"trong@gmail.com","1234567890","0901345833","Duong Duc Trong");
//        Manufacturer manufacturer  = new Manufacturer(1L,"vu Hung","kho1@gmail","0901323");
//        Warehouse warehouse = new Warehouse();
//
//        Consignment consignment = new Consignment(consignmentId,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity);
//        OrderDetail orderDetail = new OrderDetail(5L,unitPrice,quantity);
//        Order order = new Order();
//        Product product = new Product();
//        when(orderRepository.getById(any())).thenReturn(order);
//        when(consignmentRepository.getById(any())).thenReturn(consignment);
//        when(productRepository.getById(any())).thenReturn(product);
//        when(inventoryRepository.getById(any())).thenReturn(warehouse);
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        when(orderTypeRepository.getById(any())).thenReturn(orderType);
//        when(userRepository.getById(any())).thenReturn(user);
//        when(manufactorRepository.getById(any())).thenReturn(manufacturer);
//        when(orderRepository.save(any())).thenReturn(order);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Cập nhật phiếu nhập kho thành công !");
//        expected.setStatus(Constant.SUCCESS);
//        //actual
//        ResponseEntity<?> responseEntity = importGoodsService.update(updateImportGoodsRequest);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call importGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.OK);
//        when(importGoodsService.update(updateImportGoodsRequest)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
//    @Test
//    void testUpdateImportOrderNormal() throws Exception {
//        Long orderId = 1L;
//        String billReferenceNumber = "aa";
//        LocalDateTime createdDate = LocalDateTime.now();
//        LocalDateTime confirmDate= LocalDateTime.now();
//        String description = "nhap hang";
//        Long userId = 1L;
//        Long manufactorId= 1L;
//        Long wareHouseId= 5L;
//
//        Long consignmentId = 1L;
//        Long productId = 5L;
//        LocalDateTime expirationDate = LocalDateTime.parse("2023-01-25T21:34:55");
//        LocalDateTime importDate = LocalDateTime.now();
//        double unitPrice = 20;
//        int quantity = 10;
//        ConsignmentRequest consignmentRequest = new ConsignmentRequest(consignmentId, productId, expirationDate, importDate, unitPrice, quantity);
//
//        List<ConsignmentRequest> consignmentRequestList = new ArrayList<>();
//        consignmentRequestList.add(consignmentRequest);
//        UpdateImportGoodsRequest updateImportGoodsRequest = new UpdateImportGoodsRequest(orderId,billReferenceNumber,createdDate,confirmDate
//                ,description,userId,manufactorId,wareHouseId,consignmentRequestList);
//
//        OrderType orderType = new OrderType(1L,"IMPORT","Nhập hàng");
//        User user = new User(userId,"Trongdd3","123456",true, LocalDate.now(),"trong@gmail.com","1234567890","0901345833","Duong Duc Trong");
//        Manufacturer manufacturer  = new Manufacturer(1L,"vu Hung","kho1@gmail","0901323");
//        Warehouse warehouse = new Warehouse();
//
//        Consignment consignment = new Consignment(consignmentId,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity);
//        OrderDetail orderDetail = new OrderDetail(5L,unitPrice,quantity);
//        Order order = new Order();
//        Product product = new Product();
//        when(orderRepository.getById(any())).thenReturn(order);
//        when(consignmentRepository.getById(any())).thenReturn(consignment);
//        when(productRepository.getById(any())).thenReturn(product);
//        when(inventoryRepository.getById(any())).thenReturn(warehouse);
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        when(orderTypeRepository.getById(any())).thenReturn(orderType);
//        when(userRepository.getById(any())).thenReturn(user);
//        when(manufactorRepository.getById(any())).thenReturn(manufacturer);
//        when(orderRepository.save(any())).thenReturn(order);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Cập nhật phiếu nhập kho thành công !");
//        expected.setStatus(Constant.SUCCESS);
//        //actual
//        ResponseEntity<?> responseEntity = importGoodsService.update(updateImportGoodsRequest);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call importGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.OK);
//        when(importGoodsService.update(updateImportGoodsRequest)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
//    @Test
//    void testUpdateImportOrderAbnomal() throws Exception {
//        Long orderId = 1L;
//        String billReferenceNumber = "aa";
//        LocalDateTime createdDate = LocalDateTime.now();
//        LocalDateTime confirmDate= LocalDateTime.now();
//        String description = "nhap hang";
//        Long userId = 1L;
//        Long manufactorId= 1L;
//        Long wareHouseId= 5L;
//
//        Long consignmentId = 1L;
//        Long productId = 5L;
//        LocalDateTime expirationDate = LocalDateTime.parse("2023-01-25T21:34:55");
//        LocalDateTime importDate = LocalDateTime.now();
//        double unitPrice = 20;
//        int quantity = 10;
//        ConsignmentRequest consignmentRequest = new ConsignmentRequest(consignmentId, productId, expirationDate, importDate, unitPrice, quantity);
//
//        List<ConsignmentRequest> consignmentRequestList = new ArrayList<>();
//        consignmentRequestList.add(consignmentRequest);
//        UpdateImportGoodsRequest updateImportGoodsRequest = new UpdateImportGoodsRequest(orderId,billReferenceNumber,createdDate,confirmDate
//                ,description,userId,manufactorId,wareHouseId,null);
//
//        OrderType orderType = new OrderType(1L,"IMPORT","Nhập hàng");
//        User user = new User(userId,"Trongdd3","123456",true, LocalDate.now(),"trong@gmail.com","1234567890","0901345833","Duong Duc Trong");
//        Manufacturer manufacturer  = new Manufacturer(1L,"vu Hung","kho1@gmail","0901323");
//        Warehouse warehouse = new Warehouse();
//
//        Consignment consignment = new Consignment(consignmentId,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity);
//        OrderDetail orderDetail = new OrderDetail(5L,unitPrice,quantity);
//        Order order = new Order();
//        Product product = new Product();
//        when(orderRepository.getById(any())).thenReturn(order);
//        when(consignmentRepository.getById(any())).thenReturn(consignment);
//        when(productRepository.getById(any())).thenReturn(product);
//        when(inventoryRepository.getById(any())).thenReturn(warehouse);
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        when(orderTypeRepository.getById(any())).thenReturn(orderType);
//        when(userRepository.getById(any())).thenReturn(user);
//        when(manufactorRepository.getById(any())).thenReturn(manufacturer);
//        when(orderRepository.save(any())).thenReturn(order);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Cập nhật phiếu nhập kho thất bại !");
//        expected.setStatus(Constant.FAIL);
//        //actual
//        ResponseEntity<?> responseEntity = importGoodsService.update(updateImportGoodsRequest);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call importGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.OK);
//        when(importGoodsService.update(updateImportGoodsRequest)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
//
//    @Test
//    void testUpdateImportOrderIdNull() throws Exception {
//        Long orderId = 1L;
//        String billReferenceNumber = "aa";
//        LocalDateTime createdDate = LocalDateTime.now();
//        LocalDateTime confirmDate= LocalDateTime.now();
//        String description = "nhap hang";
//        Long userId = 1L;
//        Long manufactorId= 1L;
//        Long wareHouseId= null;
//
//        Long consignmentId = 1L;
//        Long productId = 5L;
//        LocalDateTime expirationDate = LocalDateTime.now();
//        LocalDateTime importDate = LocalDateTime.now();
//        double unitPrice = 20;
//        int quantity = 10;
//        ConsignmentRequest consignmentRequest = new ConsignmentRequest(consignmentId, productId, expirationDate, importDate, unitPrice, quantity);
//
//        List<ConsignmentRequest> consignmentRequestList = new ArrayList<>();
//        consignmentRequestList.add(consignmentRequest);
//        UpdateImportGoodsRequest updateImportGoodsRequest = new UpdateImportGoodsRequest(orderId,billReferenceNumber,createdDate,confirmDate
//                ,description,userId,manufactorId,wareHouseId,consignmentRequestList);
//
//        OrderType orderType = new OrderType(1L,"IMPORT","Nhập hàng");
//        User user = new User(userId,"Trongdd3","123456",true, LocalDate.now(),"trong@gmail.com","1234567890","0901345833","Duong Duc Trong");
//        Manufacturer manufacturer  = new Manufacturer(1L,"vu Hung","kho1@gmail","0901323");
//        Warehouse warehouse = new Warehouse();
//
//        Consignment consignment = new Consignment(consignmentId,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity);
//        OrderDetail orderDetail = new OrderDetail(5L,unitPrice,quantity);
//        Order order = new Order();
//        Product product = new Product();
//        when(orderRepository.findById(any())).thenReturn(java.util.Optional.of(order));
//        when(consignmentRepository.getById(any())).thenReturn(consignment);
//        when(productRepository.getById(any())).thenReturn(product);
//        when(inventoryRepository.getById(any())).thenReturn(warehouse);
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        when(orderTypeRepository.getById(any())).thenReturn(orderType);
//        when(userRepository.getById(any())).thenReturn(user);
//        when(manufactorRepository.getById(any())).thenReturn(manufacturer);
//        when(orderRepository.save(any())).thenReturn(order);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Nhà kho không thể để trống !");
//        expected.setStatus(Constant.FAIL);
//        //actual
//        ResponseEntity<?> responseEntity = importGoodsService.update(updateImportGoodsRequest);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call importGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.OK);
//        when(importGoodsService.update(updateImportGoodsRequest)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
//    @Test
//    void testUpdateImportOrderExpirationDateBeforeNow() throws Exception {
//        Long orderId = 1L;
//        String billReferenceNumber = "aa";
//        LocalDateTime createdDate = LocalDateTime.now();
//        LocalDateTime confirmDate= LocalDateTime.now();
//        String description = "nhap hang";
//        Long userId = 1L;
//        Long manufactorId= 1L;
//        Long wareHouseId= 1L;
//
//        Long consignmentId = 1L;
//        Long productId = 5L;
//        LocalDateTime expirationDate = LocalDateTime.parse("2021-01-25T21:34:55");
//        LocalDateTime importDate = LocalDateTime.now();
//        double unitPrice = 20;
//        int quantity = 10;
//        ConsignmentRequest consignmentRequest = new ConsignmentRequest(consignmentId, productId, expirationDate, importDate, unitPrice, quantity);
//
//        List<ConsignmentRequest> consignmentRequestList = new ArrayList<>();
//        consignmentRequestList.add(consignmentRequest);
//        UpdateImportGoodsRequest updateImportGoodsRequest = new UpdateImportGoodsRequest(orderId,billReferenceNumber,createdDate,confirmDate
//                ,description,userId,manufactorId,wareHouseId,consignmentRequestList);
//
//        OrderType orderType = new OrderType(1L,"IMPORT","Nhập hàng");
//        User user = new User(userId,"Trongdd3","123456",true, LocalDate.now(),"trong@gmail.com","1234567890","0901345833","Duong Duc Trong");
//        Manufacturer manufacturer  = new Manufacturer(1L,"vu Hung","kho1@gmail","0901323");
//        Warehouse warehouse = new Warehouse();
//
//        Consignment consignment = new Consignment(consignmentId,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity);
//        OrderDetail orderDetail = new OrderDetail(5L,unitPrice,quantity);
//        Order order = new Order(1L);
//        Product product = new Product();
//        when(orderRepository.getById(any())).thenReturn(order);
//        when(consignmentRepository.getById(any())).thenReturn(consignment);
//        when(productRepository.getById(any())).thenReturn(product);
//        when(inventoryRepository.getById(any())).thenReturn(warehouse);
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        when(orderTypeRepository.getById(any())).thenReturn(orderType);
//        when(userRepository.getById(any())).thenReturn(user);
//        when(manufactorRepository.getById(any())).thenReturn(manufacturer);
//        when(orderRepository.save(any())).thenReturn(order);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Hạn lưu kho phải lớn hơn ngày hiện tại !");
//        expected.setStatus(Constant.FAIL);
//        //actual
//        ResponseEntity<?> responseEntity = importGoodsService.update(updateImportGoodsRequest);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call importGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.OK);
//        when(importGoodsService.update(updateImportGoodsRequest)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
//    @Test
//    void testUpdateImportOrderExpirationDateAfterNow() throws Exception {
//        Long orderId = 1L;
//        String billReferenceNumber = "aa";
//        LocalDateTime createdDate = LocalDateTime.now();
//        LocalDateTime confirmDate= LocalDateTime.now();
//        String description = "nhap hang";
//        Long userId = 1L;
//        Long manufactorId= 1L;
//        Long wareHouseId= 5L;
//
//        Long consignmentId = 1L;
//        Long productId = 5L;
//        LocalDateTime expirationDate = LocalDateTime.parse("2023-01-25T21:34:55");
//        LocalDateTime importDate = LocalDateTime.now();
//        double unitPrice = 20;
//        int quantity = 10;
//        ConsignmentRequest consignmentRequest = new ConsignmentRequest(consignmentId, productId, expirationDate, importDate, unitPrice, quantity);
//
//        List<ConsignmentRequest> consignmentRequestList = new ArrayList<>();
//        consignmentRequestList.add(consignmentRequest);
//        UpdateImportGoodsRequest updateImportGoodsRequest = new UpdateImportGoodsRequest(orderId,billReferenceNumber,createdDate,confirmDate
//                ,description,userId,manufactorId,wareHouseId,consignmentRequestList);
//
//        OrderType orderType = new OrderType(1L,"IMPORT","Nhập hàng");
//        User user = new User(userId,"Trongdd3","123456",true, LocalDate.now(),"trong@gmail.com","1234567890","0901345833","Duong Duc Trong");
//        Manufacturer manufacturer  = new Manufacturer(1L,"vu Hung","kho1@gmail","0901323");
//        Warehouse warehouse = new Warehouse();
//
//        Consignment consignment = new Consignment(consignmentId,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity);
//        OrderDetail orderDetail = new OrderDetail(5L,unitPrice,quantity);
//        Order order = new Order();
//        Product product = new Product();
//        when(orderRepository.getById(any())).thenReturn(order);
//        when(consignmentRepository.getById(any())).thenReturn(consignment);
//        when(productRepository.getById(any())).thenReturn(product);
//        when(inventoryRepository.getById(any())).thenReturn(warehouse);
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        when(orderTypeRepository.getById(any())).thenReturn(orderType);
//        when(userRepository.getById(any())).thenReturn(user);
//        when(manufactorRepository.getById(any())).thenReturn(manufacturer);
//        when(orderRepository.save(any())).thenReturn(order);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Cập nhật phiếu nhập kho thành công !");
//        expected.setStatus(Constant.SUCCESS);
//        //actual
//        ResponseEntity<?> responseEntity = importGoodsService.update(updateImportGoodsRequest);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call importGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.OK);
//        when(importGoodsService.update(updateImportGoodsRequest)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
//
//    //Confirm
//    @Test
//    void testConfirmOrderNormal() throws Exception {
//        Long orderId = 1L;
//        Long confirmUserId = 1L;
//
//        Long consignmentId = 1L;
//        double unitPrice = 20;
//        int quantity = 10;
//        Order order = new Order();
//        Product product = new Product(1L,"Gach xay","Gach001","10","10",10,"vang","image1","hang dep",20.00);
//        Consignment consignment = new Consignment(consignmentId,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity,product);
//        OrderDetail orderDetail = new OrderDetail(5L,unitPrice,quantity,order,consignment);
//        List<OrderDetail> orderDetails = new ArrayList<>();
//        orderDetails.add(orderDetail);
//
//        when(orderRepository.getById(any())).thenReturn(order);
//        when(orderDetailRepository.findOrderDetailByOrderId(any())).thenReturn(orderDetails);
//        when(consignmentRepository.findConsignmentById(any())).thenReturn(consignment);
//
//        when(productRepository.findProductById(any())).thenReturn(product);
//        when(consignmentRepository.countQuantityWithConsignmentNotImport(any())).thenReturn(consignment);
//        when(consignmentRepository.countQuantity(any())).thenReturn(quantity);
//
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        when(productRepository.save(any())).thenReturn(product);
//        when(orderRepository.save(any())).thenReturn(order);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Xác nhận đơn hàng thành công !");
//        expected.setStatus(Constant.SUCCESS);
//        //actual
//        ResponseEntity<?> responseEntity = importGoodsService.confirm(orderId,confirmUserId);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call importGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.OK);
//        when(importGoodsService.confirm(orderId,confirmUserId)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
////    @Test
////    void testConfirmOrderAbNormal1() throws Exception {
////        Long orderId = Long.valueOf("-1");
////        Long confirmUserId = Long.valueOf("-1");
////
////        Long consignmentId = 1L;
////        double unitPrice = -1;
////        int quantity = -1;
////        Status status = new Status();
////        Order order = new Order();
////        order.setId(null);
////        order.setConfirmDate(null);
////        order.setConfirmBy(null);
////        order.setUser(null);
////        status.setId(Constant.COMPLETED);
////        Product product = new Product(null,"Gach xay","Gach001","10","10",10,"vang","image1","hang dep",20.00);
////        Consignment consignment = new Consignment(null,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity,product);
////        OrderDetail orderDetail = new OrderDetail(null,unitPrice,quantity,null,consignment);
////        List<OrderDetail> orderDetails = new ArrayList<>();
////        orderDetails.add(orderDetail);
////
////        when(orderRepository.getById(any())).thenReturn(order);
////        when(orderDetailRepository.findOrderDetailByOrderId(any())).thenReturn(orderDetails);
////        when(consignmentRepository.findConsignmentById(any())).thenReturn(consignment);
////
////        when(productRepository.findProductById(any())).thenReturn(product);
////        when(consignmentRepository.countQuantityWithConsignmentNotImport(any())).thenReturn(consignment);
////        when(consignmentRepository.countQuantity(any())).thenReturn(quantity);
////
////        when(consignmentRepository.save(any())).thenReturn(consignment);
////        when(productRepository.save(any())).thenReturn(product);
////        when(orderRepository.save(any())).thenReturn(order);
////        //expected
////        MessageResponse expected = new MessageResponse();
////        expected.setMessage("Xác nhận đơn hàng thất bại !");
////        expected.setStatus(Constant.FAIL);
////        //actual
////        ResponseEntity<?> responseEntity = importGoodsService.confirm(orderId,confirmUserId);
////        MessageResponse actual = (MessageResponse) responseEntity.getBody();
////
////        //call importGoodsService
////        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.OK);
////        when(importGoodsService.confirm(orderId,confirmUserId)).thenReturn(ResponseEntity);
////
////        assertEquals(expected.getMessage(), actual.getMessage());
////        assertEquals(expected.getStatus(), actual.getStatus());
////    }
//    @Test
//    void testConfirmOrderNotHaveConfirmUser() throws Exception {
//        Long orderId = 1L;
//        Long confirmUserId = null;
//
//        Long consignmentId = 1L;
//        double unitPrice = 20;
//        int quantity = 10;
//        Order order = new Order();
//        Product product = new Product(1L,"Gach xay","Gach001","10","10",10,"vang","image1","hang dep",20.00);
//        Consignment consignment = new Consignment(consignmentId,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity,product);
//        OrderDetail orderDetail = new OrderDetail(5L,unitPrice,quantity,order,consignment);
//        List<OrderDetail> orderDetails = new ArrayList<>();
//        orderDetails.add(orderDetail);
//
//        when(orderRepository.getById(any())).thenReturn(order);
//        when(orderDetailRepository.findOrderDetailByOrderId(any())).thenReturn(orderDetails);
//        when(consignmentRepository.findConsignmentById(any())).thenReturn(consignment);
//
//        when(productRepository.findProductById(any())).thenReturn(product);
//        when(consignmentRepository.countQuantityWithConsignmentNotImport(any())).thenReturn(consignment);
//        when(consignmentRepository.countQuantity(any())).thenReturn(quantity);
//
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        when(productRepository.save(any())).thenReturn(product);
//        when(orderRepository.save(any())).thenReturn(order);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Người xác nhận không thể bỏ trống !");
//        expected.setStatus(Constant.FAIL);
//        //actual
//        ResponseEntity<?> responseEntity = importGoodsService.confirm(orderId,confirmUserId);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call importGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.BAD_REQUEST);
//        when(importGoodsService.confirm(orderId,confirmUserId)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
//    @Test
//    void testConfirmOrderOrderIdNull() throws Exception {
//        Long orderId = null;
//        Long confirmUserId = 1L;
//
//        Long consignmentId = 1L;
//        double unitPrice = 20;
//        int quantity = 10;
//        Order order = new Order();
//        Product product = new Product(1L,"Gach xay","Gach001","10","10",10,"vang","image1","hang dep",20.00);
//        Consignment consignment = new Consignment(consignmentId,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity,product);
//        OrderDetail orderDetail = new OrderDetail(5L,unitPrice,quantity,order,consignment);
//        List<OrderDetail> orderDetails = new ArrayList<>();
//        orderDetails.add(orderDetail);
//
//        when(orderRepository.getById(any())).thenReturn(order);
//        when(orderDetailRepository.findOrderDetailByOrderId(any())).thenReturn(orderDetails);
//        when(consignmentRepository.findConsignmentById(any())).thenReturn(consignment);
//
//        when(productRepository.findProductById(any())).thenReturn(product);
//        when(consignmentRepository.countQuantityWithConsignmentNotImport(any())).thenReturn(consignment);
//        when(consignmentRepository.countQuantity(any())).thenReturn(quantity);
//
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        when(productRepository.save(any())).thenReturn(product);
//        when(orderRepository.save(any())).thenReturn(order);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Đơn hàng không tồn tại !");
//        expected.setStatus(Constant.FAIL);
//        //actual
//        ResponseEntity<?> responseEntity = importGoodsService.confirm(orderId,confirmUserId);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call importGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.BAD_REQUEST);
//        when(importGoodsService.confirm(orderId,confirmUserId)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
//    @Test
//    void testConfirmOrderconfirmUserIdNlank() throws Exception {
//        Long orderId = null;
//        Long confirmUserId = 1L;
//
//        Long consignmentId = 1L;
//        double unitPrice = 20;
//        int quantity = 10;
//        Order order = new Order();
//        Product product = new Product(1L,"Gach xay","Gach001","10","10",10,"vang","image1","hang dep",20.00);
//        Consignment consignment = new Consignment(consignmentId,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity,product);
//        OrderDetail orderDetail = new OrderDetail(5L,unitPrice,quantity,order,consignment);
//        List<OrderDetail> orderDetails = new ArrayList<>();
//        orderDetails.add(orderDetail);
//
//        when(orderRepository.getById(any())).thenReturn(order);
//        when(orderDetailRepository.findOrderDetailByOrderId(any())).thenReturn(orderDetails);
//        when(consignmentRepository.findConsignmentById(any())).thenReturn(consignment);
//
//        when(productRepository.findProductById(any())).thenReturn(product);
//        when(consignmentRepository.countQuantityWithConsignmentNotImport(any())).thenReturn(consignment);
//        when(consignmentRepository.countQuantity(any())).thenReturn(quantity);
//
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        when(productRepository.save(any())).thenReturn(product);
//        when(orderRepository.save(any())).thenReturn(order);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Đơn hàng không tồn tại !");
//        expected.setStatus(Constant.FAIL);
//        //actual
//        ResponseEntity<?> responseEntity = importGoodsService.confirm(orderId,confirmUserId);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call importGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.BAD_REQUEST);
//        when(importGoodsService.confirm(orderId,confirmUserId)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
//
//    @Test
//    void testConfirmOrderconfirmUserIdNlank1() throws Exception {
//        Long orderId = -1L;
//        Long confirmUserId = 1L;
//
//        Long consignmentId =1L;
//        double unitPrice = 20;
//        int quantity = 10;
//        Order order = new Order();
//        Product product = new Product(null,"Gach xay","Gach001","10","10",10,"vang","image1","hang dep",20.00);
//        Consignment consignment = new Consignment(null,LocalDateTime.now(),LocalDateTime.now(),unitPrice,quantity,product);
//        OrderDetail orderDetail = new OrderDetail(null,unitPrice,quantity,order,consignment);
//        List<OrderDetail> orderDetails = new ArrayList<>();
//        orderDetails.add(orderDetail);
//
//        when(orderRepository.getById(any())).thenReturn(order);
//        when(orderDetailRepository.findOrderDetailByOrderId(any())).thenReturn(orderDetails);
//        when(consignmentRepository.findConsignmentById(any())).thenReturn(consignment);
//
//        when(productRepository.findProductById(any())).thenReturn(product);
//        when(consignmentRepository.countQuantityWithConsignmentNotImport(any())).thenReturn(consignment);
//        when(consignmentRepository.countQuantity(any())).thenReturn(quantity);
//
//        when(consignmentRepository.save(any())).thenReturn(consignment);
//        when(productRepository.save(any())).thenReturn(product);
//        when(orderRepository.save(any())).thenReturn(order);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        expected.setMessage("Xác nhận đơn hàng thành công !");
//        expected.setStatus(Constant.SUCCESS);
//        //actual
//        ResponseEntity<?> responseEntity = importGoodsService.confirm(orderId,confirmUserId);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call importGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.OK);
//        when(importGoodsService.confirm(orderId,confirmUserId)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//    }
//
//    //test cancel Order
//    @Test
//    void testCancelImportOrder() throws Exception {
//        Long orderId = 1L;
//        Order order = new Order();
//        when(orderRepository.findById(any())).thenReturn(Optional.of(order));
//        when(orderRepository.save(any())).thenReturn(order);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        ResponseEntity<?> responseEntity = importGoodsService.cancel(orderId);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call importGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.BAD_REQUEST);
//        when(importGoodsService.cancel(orderId)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//
//    }
//    @Test
//    void testCancelImportOrderAbnormal() throws Exception {
//        Long orderId = -1L;
//        Order order = new Order();
//        when(orderRepository.findById(any())).thenReturn(Optional.of(order));
//        when(orderRepository.save(any())).thenReturn(order);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        ResponseEntity<?> responseEntity = importGoodsService.cancel(orderId);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call importGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.BAD_REQUEST);
//        when(importGoodsService.cancel(orderId)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//
//    }
//
//    @Test
//    void testCancelImportOrderBoudary() throws Exception {
//        Long orderId = 0L;
//        Order order = new Order();
//        when(orderRepository.findById(any())).thenReturn(Optional.of(order));
//        when(orderRepository.save(any())).thenReturn(order);
//        //expected
//        MessageResponse expected = new MessageResponse();
//        ResponseEntity<?> responseEntity = importGoodsService.cancel(orderId);
//        MessageResponse actual = (MessageResponse) responseEntity.getBody();
//
//        //call importGoodsService
//        ResponseEntity ResponseEntity = new ResponseEntity(expected, HttpStatus.BAD_REQUEST);
//        when(importGoodsService.cancel(orderId)).thenReturn(ResponseEntity);
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//
//    }
//
//    // test get List
//    @Test
//    void testgetListImportOrder() throws Exception {
//        Long userId =1L;
//        String billReferenceNumber = "nhap";
//        String startDate1 = "01-01-2022";
//        String endDate1 = "01-10-2022";
//        Date startDate =new SimpleDateFormat("dd-MM-yyyy").parse(startDate1);
//        Date endDate= new SimpleDateFormat("dd-MM-yyyy").parse(endDate1);
//        Integer status = 1;
//        Integer pageIndex = 1;
//        Integer pageSize = 10;
//        Map<String, Object> map = new HashMap<>();
//        map.put("orderId",2L);
//
//        List<Map<String, Object>> orderList = new ArrayList<>();
//        orderList.add(map);
//        when(orderRepository.getListImportGoodsBySearchData(any(),any(),any(),any(),any(),any())).thenReturn(orderList);
//
//        //expected
//        ServiceResult<Map<String, Object>> expected = new ServiceResult<>();
//        expected.setMessage("success");
//        expected.setStatus(HttpStatus.OK);
//        //actual
//        ServiceResult<Map<String, Object>> actual = importGoodsService.getListImportGoods(userId,billReferenceNumber,startDate,endDate,status,pageIndex,pageSize);
//        //call getListImportGoods
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//
//    }
//
//    @Test
//    void testgetListImportOrder1() throws Exception {
//        Long userId =0L;
//        String billReferenceNumber = "";
//        String startDate1 = "01-01-2022";
//        String endDate1 = "01-01-2022";
//        Date startDate =new SimpleDateFormat("dd-MM-yyyy").parse(startDate1);
//        Date endDate= new SimpleDateFormat("dd-MM-yyyy").parse(endDate1);
//        Integer status = 0;
//        Integer pageIndex = 1;
//        Integer pageSize = 10;
//        Map<String, Object> map = new HashMap<>();
//        map.put("orderId",2L);
//
//        List<Map<String, Object>> orderList = new ArrayList<>();
//        orderList.add(map);
//        when(orderRepository.getListImportGoodsBySearchData(any(),any(),any(),any(),any(),any())).thenReturn(orderList);
//
//        //expected
//        ServiceResult<Map<String, Object>> expected = new ServiceResult<>();
//        expected.setMessage("success");
//        expected.setStatus(HttpStatus.OK);
//        //actual
//        ServiceResult<Map<String, Object>> actual = importGoodsService.getListImportGoods(userId,billReferenceNumber,startDate,endDate,status,pageIndex,pageSize);
//        //call getListImportGoods
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//
//    }
//    @Test
//    void testgetListImportOrder2() throws Exception {
//        Long userId =null;
//        String billReferenceNumber = null;
//        String startDate1 = "01-01-2022";
//        String endDate1 = "01-01-2022";
//        Date startDate =new SimpleDateFormat("dd-MM-yyyy").parse(startDate1);
//        Date endDate= new SimpleDateFormat("dd-MM-yyyy").parse(endDate1);
//        Integer status = null;
//        Integer pageIndex = 1;
//        Integer pageSize = 10;
//        Map<String, Object> map = new HashMap<>();
//        map.put("orderId",2L);
//
//        List<Map<String, Object>> orderList = new ArrayList<>();
//        orderList.add(map);
//        when(orderRepository.getListImportGoodsBySearchData(any(),any(),any(),any(),any(),any())).thenReturn(orderList);
//
//        //expected
//        ServiceResult<Map<String, Object>> expected = new ServiceResult<>();
//        expected.setMessage("success");
//        expected.setStatus(HttpStatus.OK);
//        //actual
//        ServiceResult<Map<String, Object>> actual = importGoodsService.getListImportGoods(userId,billReferenceNumber,startDate,endDate,status,pageIndex,pageSize);
//        //call getListImportGoods
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//
//    }
//
//    @Test
//    void testGetDetailImportOrder1() throws Exception {
//        Long orderId =1L;
//        Map<String, Object> map = new HashMap<>();
//        map.put("orderId",2L);
//        when(orderRepository.getOrderDetail(any())).thenReturn(map);
//
//        //expected
//        ServiceResult<Map<String, Object>> expected = new ServiceResult<>();
//        expected.setMessage("success");
//        expected.setStatus(HttpStatus.OK);
//        //actual
//        ServiceResult<Map<String, Object>> actual = importGoodsService.getInforDetailImportGoods(orderId);
//        //call getListImportGoods
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//
//    }
//
//    @Test
//    void testGetlistProductOfImportGoods() throws Exception {
//        Long orderId =-1L;
//        Integer pageIndex = 1;
//        Integer pageSize = 10;
//        Map<String, Object> map = new HashMap<>();
//        map.put("orderId",2L);
//
//        List<Map<String, Object>> orderList = new ArrayList<>();
//        orderList.add(map);
//        when( orderRepository.getDetailImportGoods2(any(),any())).thenReturn(orderList);
//
//        //expected
//        ServiceResult<Map<String, Object>> expected = new ServiceResult<>();
//        expected.setMessage("success");
//        expected.setStatus(HttpStatus.OK);
//        //actual
//        ServiceResult<Map<String, Object>> actual = importGoodsService.getlistProductOfImportGoods(orderId,pageIndex,pageSize);
//        //call getListImportGoods
//
//        assertEquals(expected.getMessage(), actual.getMessage());
//        assertEquals(expected.getStatus(), actual.getStatus());
//
//    }
//
//}
